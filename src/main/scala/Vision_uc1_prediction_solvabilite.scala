import org.apache.spark.sql.functions._
import org.apache.spark.sql._
import org.apache.spark.ml.tuning.{ParamGridBuilder, TrainValidationSplit}
import org.apache.spark.ml.{Pipeline, PipelineModel, PipelineStage}
import org.apache.spark.ml.feature.{IndexToString, OneHotEncoderEstimator, StandardScaler, StringIndexer, VectorAssembler}
import org.apache.spark.ml.classification.{GBTClassifier, LogisticRegression, MultilayerPerceptronClassifier, RandomForestClassifier}
import org.apache.spark.ml.evaluation.{BinaryClassificationEvaluator, MulticlassClassificationEvaluator}
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}
import org.apache.spark.mllib.evaluation.MulticlassMetrics


object Vision_uc1_prediction_solvabilite {


  def main(args: Array[String]): Unit = {
    // création de spark session
    val spark = SparkSession.builder().appName("Vision_uc1_prediction_solvabilite").master("yarn").enableHiveSupport().getOrCreate()

    // chargement des données de datalake RCU dans les deux dataframes df_rcu et df_rcu_socio et persist dans la RAM
    // date_jour est le dérnier jour du mois précédent
    val df_rcu = spark.sql(" SELECT master_id, MSISDN FROM  (    SELECT master_id , MSISDN , ROW_NUMBER() OVER (PARTITION BY MSISDN ORDER BY date_creation_mid DESC) AS row_num  FROM   ( SELECT master_id , CASE WHEN LENGTH(TRIM(phone_num)) = 8 THEN CASE WHEN LIBELLE_TYPESERVICE = 'FIXE' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'WIMAX' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'MOBILE' THEN CASE WHEN SUBSTR(TRIM(phone_num),2,1) IN ('0','1','2','3') THEN CONCAT('01',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('4','5','6') THEN CONCAT('05',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('7','8','9') THEN CONCAT('07',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'AVISO' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'ORANGE MONEY' THEN CASE WHEN SUBSTR(TRIM(phone_num),2,1) IN ('0','1','2','3') THEN CONCAT('01',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('4','5','6') THEN CONCAT('05',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('7','8','9') THEN CONCAT('07',TRIM(phone_num)) ELSE TRIM(phone_num) END ELSE TRIM(phone_num) END ELSE TRIM(phone_num) END AS MSISDN  , date_creation_mid FROM vision_uc1_datalake.rcu where date_jour =                        (SELECT MAX(date_jour) FROM vision_uc1_datalake.rcu)                      ) first_req   ) second_req  WHERE row_num = 1 ")

    // charger la table de mapping pour les categories clients
    val df_categ_mapping = spark.sql("SELECT distinct trim(categorie_client) as categorie_client,mapping as segment FROM vision_uc1_datalake.file_categories_clients")

    // chargement des données de la datalake rateplan et technologie
    // date_jour est le dérnier jour du mois précédent
    val df_univers = spark.sql("SELECT DISTINCT TRIM(RATEPLAN) as rateplan, TRIM(UNIVERS) as univers from vision_uc1_datalake.file_mapping_mob_broadb_b2b_b2c where date_jour =               (SELECT MAX(date_jour) FROM vision_uc1_datalake.file_mapping_mob_broadb_b2b_b2c)            ")


    //*********************************************************************************************************************************************

    // chargement des données parc
    // date_jour est le dérnier jour du mois précédent
    val df_parc_fixe = spark.sql   (" SELECT msisdn_parc,NUMERO_CLIENT  FROM ( SELECT TRIM(numero_appel) as msisdn_parc, TRIM(NUMERO_CLIENT) as NUMERO_CLIENT, ROW_NUMBER() OVER (PARTITION BY TRIM(numero_appel) ORDER BY DATE_MS DESC) AS row_num   FROM vision_uc1_datalake.gaia_parc_fixe             WHERE date_jour =                (SELECT MAX(date_jour) FROM vision_uc1_datalake.gaia_parc_fixe)          ) first_req WHERE row_num=1 ")

    val df_parc_fixe_master_id = df_parc_fixe.join(df_rcu, df_parc_fixe("msisdn_parc")===df_rcu("MSISDN"),"LEFT")
      .select(df_rcu("master_id"),df_parc_fixe("NUMERO_CLIENT"))

    val df_gaia_nouvelles_factures_fixe = spark.sql("SELECT ncli, SUM(MONTANT_TOTAL_FACTURE) as MONTANT_TOTAL_FACTURE, univers, categorie,date_jour FROM vision_uc1_datalake.gaia_nouvelles_factures_fixe WHERE date_jour=date_format(current_date,'yyyy-MM-01') GROUP BY ncli,univers, categorie, date_jour ")

    // ajout du master_id
    val df_gaia_master_id = df_gaia_nouvelles_factures_fixe
      .join(df_parc_fixe_master_id, df_gaia_nouvelles_factures_fixe("ncli") === df_parc_fixe_master_id("NUMERO_CLIENT"),"left" )
      .select(df_parc_fixe_master_id("master_id"),  df_gaia_nouvelles_factures_fixe("MONTANT_TOTAL_FACTURE"), df_gaia_nouvelles_factures_fixe("univers"), df_gaia_nouvelles_factures_fixe("categorie"),df_gaia_nouvelles_factures_fixe("DATE_JOUR") )
      .distinct()

    // ajout de segment
    val df_gaia_segment = df_gaia_master_id
      .join(df_categ_mapping, df_gaia_master_id("categorie") === df_categ_mapping("categorie_client"),"LEFT" )
      .select(df_gaia_master_id("master_id"),df_gaia_master_id("MONTANT_TOTAL_FACTURE"),df_gaia_master_id("univers"),df_categ_mapping("segment"),df_gaia_master_id("DATE_JOUR"))

    //*********************************************************************************************************************************************

    // chargement des données parc
    // date_jour est le dérnier jour du mois précédent
    val df_parc_mobile = spark.sql(" SELECT msisdn_parc, compte_client FROM ( SELECT DISTINCT TRIM(msisdn)       as msisdn_parc , TRIM(compte_client) as compte_client , ROW_NUMBER() OVER (PARTITION BY TRIM(msisdn) ORDER BY DATE_ACTIVATION DESC) AS row_num  FROM vision_uc1_datalake.bscs_parc_mobile  WHERE date_jour =                     (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_mobile)                  and type_offre='POSTPAID'                   ) first_req WHERE row_num=1  ")
    // date_jour est le dérnier jour du mois précédent
    val df_parc_adsl = spark.sql(" SELECT msisdn_parc, custcode      FROM ( SELECT DISTINCT TRIM(numero_adsl)  as msisdn_parc , TRIM(custcode) as custcode           , ROW_NUMBER() OVER (PARTITION BY TRIM(numero_adsl) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_adsl WHERE date_jour =            (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_adsl)                   ) first_req WHERE row_num=1 ")
    // date_jour est le dérnier jour du mois précédent
    val df_parc_ftth = spark.sql(" SELECT msisdn_parc, custcode      FROM ( SELECT DISTINCT TRIM(nd)           as msisdn_parc , TRIM(custcode) as custcode           , ROW_NUMBER() OVER (PARTITION BY TRIM(nd) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_ftth  WHERE date_jour =                    (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_ftth)                   AND  date_resiliation is NULL ) first_req WHERE row_num=1 ")
    // date_jour est le dérnier jour du mois précédent
    val df_parc_tdd = spark.sql(" SELECT msisdn_parc, custcode      FROM ( SELECT DISTINCT TRIM(nd)           as msisdn_parc , TRIM(custcode) as custcode           , ROW_NUMBER() OVER (PARTITION BY TRIM(nd) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_tdd  WHERE date_jour =                      (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_tdd)                  ) first_req WHERE row_num=1 ")
    // date_jour est le dérnier jour du mois précédent
    val df_parc_fdd = spark.sql(" SELECT msisdn_parc, custcode      FROM ( SELECT DISTINCT TRIM(nd)           as msisdn_parc , TRIM(custcode) as custcode           , ROW_NUMBER() OVER (PARTITION BY TRIM(nd) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_fdd   WHERE date_jour =                     (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_fdd)                   ) first_req WHERE row_num=1 ")

    // calcul de couple master_id, custcode entre parc et rcu
    val df_union_parc_msisdn_custcode = df_parc_mobile.withColumnRenamed("compte_client","custcode")
      .union(df_parc_adsl)
      .union(df_parc_ftth)
      .union(df_parc_fdd)
      .union(df_parc_tdd)
      .distinct()
      .dropDuplicates("msisdn_parc")

    // couple master_id / custcode distinct
    val df_union_parc_custcode_master_id = df_union_parc_msisdn_custcode
      .join(df_rcu, df_union_parc_msisdn_custcode("msisdn_parc")===df_rcu("msisdn") ,"LEFT")
      .select(df_rcu("master_id"), df_union_parc_msisdn_custcode("custcode"))
      .distinct()
      .dropDuplicates("custcode")

    val df_dwocit_nouvelles_factures_broadband = spark.sql("SELECT COMPLE_CLIENT, SUM(MONTANT_TOTAL_FACTURE) as MONTANT_TOTAL_FACTURE, rateplan, categorie, date_jour FROM vision_uc1_datalake.dwocit_nouvelles_factures_broadband WHERE date_jour=date_format(current_date,'yyyy-MM-01') GROUP BY COMPLE_CLIENT,rateplan, categorie, date_jour")

    // ajout du master_id
    val df_dwocit_master_id = df_dwocit_nouvelles_factures_broadband
      .join(df_union_parc_custcode_master_id, df_dwocit_nouvelles_factures_broadband("COMPLE_CLIENT") === df_union_parc_custcode_master_id("custcode"),"left" )
      .select(df_union_parc_custcode_master_id("master_id"),  df_dwocit_nouvelles_factures_broadband("MONTANT_TOTAL_FACTURE"), df_dwocit_nouvelles_factures_broadband("rateplan"), df_dwocit_nouvelles_factures_broadband("categorie"),df_dwocit_nouvelles_factures_broadband("DATE_JOUR") )
      .distinct()

    // ajout de segment et univers
    val df_dwocit_segment_univers = df_dwocit_master_id
      .join(df_categ_mapping, df_dwocit_master_id("categorie") === df_categ_mapping("categorie_client") , "LEFT" )
      .join(df_univers, df_dwocit_master_id("rateplan") === df_univers("rateplan") , "LEFT" )
      .select(df_dwocit_master_id("master_id"),df_dwocit_master_id("MONTANT_TOTAL_FACTURE"),df_univers("univers"),df_categ_mapping("segment"),df_dwocit_master_id("date_jour"))


    //**********************************************************************************************************************************************

    val df_new_factures = df_gaia_segment.union(df_dwocit_segment_univers).na.drop(Seq("master_id"))
      .groupBy("master_id","segment","date_jour")
      .agg(sum("MONTANT_TOTAL_FACTURE").as("MONTANT_TOTAL_FACTURE"))
      .cache()

    //**********************************************************************************************************************************************

    // importer les données de facturation (factures payées et factures non payées) du derniere années
    val df_factures_payees =   spark.sql("SELECT master_id as master_id_a,  date_emission_facture ,    date_payement_facture      , montant_total_facture ,         montant_total_paye ,         mode_payement , CASE WHEN montant_total_facture=montant_total_paye THEN datediff(date_payement_facture,date_emission_facture) ELSE null END AS delai_edit_paim , CASE WHEN montant_total_facture=montant_total_paye THEN 'payee' ELSE 'impayee' END AS statut_facture , univers  , categorie_client  , numero_facture FROM vision_uc1_datamart.vision_dm_facturation       where master_id IS NOT NULL                        AND DATE_EMISSION_FACTURE BETWEEN            date_sub(current_date,366) AND date_sub(current_date,1)                        ")
    val df_factures_impayees = spark.sql("SELECT master_id as master_id_a, date_emission_facture , null as date_payement_facture , montant_total_facture , null as montant_total_paye , null as mode_payement ,                null    AS    delai_edit_paim                                                                                                   , null     AS       statut_facture                                                                     , univers  , categorie_client  , numero_facture FROM vision_uc1_datamart.vision_dm_factures_impayees where master_id IS NOT NULL                        AND date_emission_facture BETWEEN            date_sub(current_date,366) AND date_sub(current_date,1)                        ")

    // dataframe contenant toutes les factures de la derniere annee pour chaque client
    val df_fact = df_factures_payees.union(df_factures_impayees)

    // Calculer les delais de paiement des factures ==> (master_id,delai_moyen_entre_edition_paiement_facture)
    val df_fact_pay = df_fact.groupBy(col("master_id_a").as("master_id_a_a")).agg(round(avg("delai_edit_paim")).cast("int").as("delai_moyen_entre_edition_paiement_facture"))

    // Date et Statut de la derniere facture pour chaque client ==> (master_id, date_edition_derniere_facture, statut_derniere_facture)
    val df_dern_fact = df_fact.groupBy(col("master_id_a").as("master_id_a_b")).agg(max("date_emission_facture").as("date_edition_derniere_facture"), last("statut_facture").as("statut_derniere_facture"))

    // Calculer le Nombre de factures non payees pour chaque client ==> (master_id,nb_factures_non_payees)
    val df_fact_imp = df_fact.filter(col("statut_facture") === "impayee").groupBy(col("master_id_a").as("master_id_a_c")).count().withColumnRenamed("count","nb_factures_non_payees").withColumn("nb_factures_non_payees",col("nb_factures_non_payees").cast("int"))


    //Retourner l'anciennete en terme de nombre de jours
    // Master_id unique , date_aquisation minimale ,  anciennete en terme de jours sur la dérnière partition
    //val df_offre_anc = spark.sql("SELECT master_id, date_aquisation , datediff(current_date , date_aquisation) as anciennete  FROM ( SELECT master_id, date_aquisation, ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY date_aquisation )  AS row_num  FROM vision_uc1_datamart.vision_dm_offre where master_id IS NOT NULL AND date_jour = date_sub(current_date,1))  firest_req   WHERE  row_num = 1")
    //==> ici j'ai constaté qu'il y a plusieurs master_id qui sont NULL
    //la derniere partition qui contient des master_id not null est  2022-03-14
    //donc pour remplir la datamart, j'ai changé la date manuellement ( 2022-03-14 a la place de j-1 ==> date_sub(current_date,1) )
    val df_offre_anc = spark.sql("SELECT master_id as master_id_b , datediff(current_date , date_aquisation) as anciennete  FROM ( SELECT master_id, date_aquisation, ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY date_aquisation )  AS row_num  FROM vision_uc1_datamart.vision_dm_offre where master_id IS NOT NULL                       AND date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre)                       )  firest_req   WHERE  row_num = 1")
      .join(df_new_factures , col("master_id_b")===col("master_id"),"INNER")
      .select("master_id_b","anciennete")

    // master_id unique , profilConvergence ==> mono_univers / multi_univers sur la dérnière partition
    val df_offre_converg = spark.sql("SELECT DISTINCT master_id as master_id_c, univers FROM  vision_uc1_datamart.vision_dm_offre where master_id IS NOT NULL                       AND date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre)                       ")
      .groupBy("master_id_c").agg(count("*").alias("cnt"))
      .withColumn("profil_de_convergence" , when(col("cnt") === 1,"mono_univers").otherwise("multi_univers")).drop("cnt")
      .join(df_new_factures , col("master_id_c")===col("master_id"),"INNER")
      .select("master_id_c","profil_de_convergence")

    //nombre de jours suspendu pour chaque client
    val df_offre_susp = spark.sql("SELECT master_id as master_id_d, count(*) as nb_suspensions FROM vision_uc1_datamart.vision_dm_offre where master_id is not null and statut_technique = 'Suspendu'                       AND date_jour BETWEEN              date_sub(current_date,366) AND date_sub(current_date,1)                      group by master_id")
      .join(df_new_factures , col("master_id_d")===col("master_id"),"INNER")
      .select("master_id_d","nb_suspensions")

    //calculer l usage total de voix, sms, data pour chaque client
    val df_usage_tot = spark.sql("SELECT master_id as master_id_e, SUM(NVL(usage_voix_sortant,0)) as usage_voix_sortant , SUM(NVL(usage_sms_sortant,0)) as usage_sms_sortant, SUM(NVL(usage_data,0)) as usage_data FROM vision_uc1_datamart.vision_dm_usage where master_id IS NOT NULL                       AND date_jour BETWEEN              date_sub(current_date,366) AND date_sub(current_date,1)                      GROUP BY master_id ")
      .join(df_new_factures , col("master_id_e")===col("master_id"),"INNER")
      .select("master_id_e","usage_voix_sortant","usage_sms_sortant","usage_data")

    //calculer le montant moyen de rechargement pour chaque client
    // master_id unique , moyen de montant_total_rechargement par client
    val df_rech_moy = spark.sql("SELECT master_id as master_id_f, AVG(montant_total_rechargement) as moyenne_rechargement FROM vision_uc1_datamart.vision_dm_rechargement where master_id IS NOT NULL                       AND date_jour BETWEEN              date_sub(current_date,366) AND date_sub(current_date,1)                       GROUP BY master_id")
      .join(df_new_factures , col("master_id_f")===col("master_id"),"INNER")
      .select("master_id_f","moyenne_rechargement")

    //calculer le nombre de reclamations pour chaque client
    // master_id unique , count des réclamation par client
    val df_rec_nbr = spark.sql("SELECT master_id as master_id_h, count(*) as nbr_reclamations from vision_uc1_datamart.vision_dm_exp_client_reclamation where master_id IS NOT NULL                       AND date_jour BETWEEN          date_sub(current_date,366) AND date_sub(current_date,1)                       GROUP BY master_id ")
      .join(df_new_factures , col("master_id_h")===col("master_id"),"INNER")
      .select("master_id_h","nbr_reclamations")

    //**********************************************************************************************************************************************

    val df_new_factures_prediction =  df_new_factures
      .join(df_fact_pay,df_new_factures("master_id") === df_fact_pay("master_id_a_a"), "left")
      .join(df_dern_fact, df_new_factures("master_id") === df_dern_fact("master_id_a_b"), "left")
      .join(df_fact_imp, df_new_factures("master_id") === df_fact_imp("master_id_a_c"), "left")
      .join(df_offre_anc, df_new_factures("master_id") === df_offre_anc("master_id_b"), "left")
      .join(df_offre_converg, df_new_factures("master_id") === df_offre_converg("master_id_c"), "left")
      .join(df_offre_susp, df_new_factures("master_id") === df_offre_susp("master_id_d"), "left")
      .join(df_usage_tot, df_new_factures("master_id") === df_usage_tot("master_id_e"), "left")
      .join(df_rech_moy, df_new_factures("master_id") === df_rech_moy("master_id_f"), "left")
      .join(df_rec_nbr, df_new_factures("master_id") === df_rec_nbr("master_id_h"), "left")
      .select(df_new_factures("master_id"),
        df_fact_pay("delai_moyen_entre_edition_paiement_facture"),
        df_new_factures("MONTANT_TOTAL_FACTURE"),
        df_dern_fact("statut_derniere_facture"),
        df_fact_imp("nb_factures_non_payees"),
        df_new_factures("segment"),
        df_offre_anc("anciennete")
        ,df_offre_converg("profil_de_convergence")
        ,df_offre_susp("nb_suspensions"),
        df_usage_tot("usage_voix_sortant"),
        df_usage_tot("usage_sms_sortant"),
        df_usage_tot("usage_data"),
        df_rech_moy("moyenne_rechargement"),
        df_rec_nbr("nbr_reclamations"),
        df_new_factures("date_jour"))
      .na.fill(0)
      .na.fill("-")
      .cache()

    //**********************************************************************************************************************************************


    // data preprocessing
    def transform_data(data: DataFrame): DataFrame = {
      // string indexer pour les colonnes categoriques
      val fact_pay_indexer = new StringIndexer().setInputCol("statut_derniere_facture").setOutputCol("statut_derniere_facture_index")
      val seg_client_indexer = new StringIndexer().setInputCol("segment").setOutputCol("segment_index")
      val conv_indexer = new StringIndexer().setInputCol("profil_de_convergence").setOutputCol("profil_de_convergence_index")
      // One hot encoding pour les 'categorical features'
      val encoder = new OneHotEncoderEstimator().setInputCols(Array("statut_derniere_facture_index", "segment_index", "profil_de_convergence_index")).setOutputCols(Array("statut_derniere_facture_encoded", "segment_encoded", "profil_de_convergence_encoded"))
      // vector assembling pour les 'numerical features' (le modele ML dans Spark accepte un seul attribut : vecteur , contenant toutes les colonnes a implementer)
      val num_assembler = new VectorAssembler().setInputCols(Array("delai_moyen_entre_edition_paiement_facture", "MONTANT_TOTAL_FACTURE", "nb_factures_non_payees", "anciennete", "nb_suspensions", "usage_voix_sortant", "usage_sms_sortant", "usage_data", "moyenne_rechargement", "nbr_reclamations")).setOutputCol("num_assembled")
      // appliquer le 'feature scaling' pour les donnees numeriques (pour avoir le meme range pour les 'nemrical features')
      val num_scaler = new StandardScaler().setInputCol("num_assembled").setOutputCol("scaled_num").setWithStd(true)
      // vector assembling pour toutes les colonnes
      val assembler = new VectorAssembler().setInputCols(Array("scaled_num", "statut_derniere_facture_encoded", "segment_encoded", "profil_de_convergence_encoded")).setOutputCol("features")
      // preparer le pipeline a suivre pour le feature engineering (indexing, encoding, scaling, assembling)
      val pipeline = new Pipeline().setStages(Array(fact_pay_indexer, seg_client_indexer, conv_indexer, encoder, num_assembler, num_scaler, assembler))
      // appliquer le pipeline sur notre data
      val df_processed = pipeline.fit(data).transform(data)
      df_processed
    }


    val df_new_factures_prediction_features = transform_data(df_new_factures_prediction)

    val sameModel = PipelineModel.load("/user/vision_cpt/model_ds")

    val rf_predictions = sameModel.transform(df_new_factures_prediction_features)

    rf_predictions.select("master_id","delai_moyen_entre_edition_paiement_facture","MONTANT_TOTAL_FACTURE","statut_derniere_facture","nb_factures_non_payees","segment","anciennete","profil_de_convergence","nb_suspensions","usage_voix_sortant","usage_sms_sortant","usage_data","moyenne_rechargement","nbr_reclamations","predicted_label","date_jour")
      .withColumnRenamed("predicted_label","solvable")
      .coalesce(1).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_prediction_solvabilite")

  }

  }
