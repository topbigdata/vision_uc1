import org.apache.spark.sql.functions._
import org.apache.spark.sql._
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.feature.{IndexToString, OneHotEncoderEstimator, StandardScaler, StringIndexer, VectorAssembler}
import org.apache.spark.ml.classification.RandomForestClassifier

// coorection solvabilité : enlever les deux colonnes segment_client et delai_moyen_entre_edition_paiement_facture


object Vision_uc1_modele_solvabilite {

  def main(args: Array[String]): Unit = {
    // création de spark session
    val spark = SparkSession.builder().appName("Vision_uc1_modele_solvabilite").master("yarn").enableHiveSupport().getOrCreate()

    // data preprocessing
    def transform_data(data: DataFrame): DataFrame = {
      // string indexer pour les colonnes categoriques
      val fact_pay_indexer = new StringIndexer().setInputCol("statut_derniere_facture").setOutputCol("statut_derniere_facture_index")
      val seg_client_indexer = new StringIndexer().setInputCol("segment_client").setOutputCol("segment_client_index")
      val conv_indexer = new StringIndexer().setInputCol("profil_de_convergence").setOutputCol("profil_de_convergence_index")
      // One hot encoding pour les 'categorical features'
      val encoder = new OneHotEncoderEstimator().setInputCols(Array("statut_derniere_facture_index", "segment_client_index", "profil_de_convergence_index")).setOutputCols(Array("statut_derniere_facture_encoded", "segment_client_encoded", "profil_de_convergence_encoded"))
      // vector assembling pour les 'numerical features' (le modele ML dans Spark accepte un seul attribut : vecteur , contenant toutes les colonnes a implementer)
      val num_assembler = new VectorAssembler().setInputCols(Array("delai_moyen_entre_edition_paiement_facture", "montant_moyen_factures", "nb_factures_non_payees", "anciennete", "nb_suspensions", "usage_voix_sortant", "usage_sms_sortant", "usage_data", "moyenne_rechargement", "nbr_reclamations")).setOutputCol("num_assembled")
      // appliquer le 'feature scaling' pour les donnees numeriques (pour avoir le meme range pour les 'nemrical features')
      val num_scaler = new StandardScaler().setInputCol("num_assembled").setOutputCol("scaled_num").setWithStd(true)
      // vector assembling pour toutes les colonnes
      val assembler = new VectorAssembler().setInputCols(Array("scaled_num", "statut_derniere_facture_encoded", "segment_client_encoded", "profil_de_convergence_encoded")).setOutputCol("features")
      // preparer le pipeline a suivre pour le feature engineering (indexing, encoding, scaling, assembling)
      val pipeline = new Pipeline().setStages(Array(fact_pay_indexer, seg_client_indexer, conv_indexer, encoder, num_assembler, num_scaler, assembler))
      // appliquer le pipeline sur notre data
      val df_processed = pipeline.fit(data).transform(data)
      df_processed
    }


    // lire la datamart solvabilité
    val dm_solvabilite = spark.sql("select * from vision_uc1_datamart.vision_dm_solvabilite")

    // split data 80% train 20% test
    val Array(training, test) = transform_data(dm_solvabilite).randomSplit(Array(0.8, 0.2), seed = 10)
    // undersampling pour avoir le nombre de oui == non
    // compter le nombre de oui
    val nb = training.filter(col("Solvable") === "Oui").count()
    // undersampling pour avoir nbr oui == nbr non (pour la partie training)
    // dans notre cas, undersampling est preferable par rapport au oversampling pour ne pas influencer le modele
    val train_non = training.filter(col("Solvable") === "Non").orderBy(rand()).limit(nb.toInt)
    val train_oui = training.filter(col("Solvable") === "Oui")
    // train contient finalement des donnees avec labels equilibrés
    val train = train_non.union(train_oui).orderBy(rand())



    //nous allons travailler avec random forest dans notre cas
    //RF
    // indexer le label (solvable, non solvable)
    val labelIndexer = new StringIndexer().setInputCol("solvable").setOutputCol("label").fit(train)
    // le choix des parametres est fait en utilisant GridSearch+CrossValidation (operation lourde dure jsq 13 heures)
    // on peut utiliser les parametres par defaut du modele, et si on n'obtient pas la precision attendue on fait recours aux GridSearch+CrossValidation
    // val rf = new RandomForestClassifier().setLabelCol("label").setFeaturesCol("features").setMaxBins(32).setMaxDepth(10).setNumTrees(50).setMaxMemoryInMB(256).setMinInfoGain(0.0).setMinInstancesPerNode(1).setSeed(207336481).setCheckpointInterval(10).setImpurity("gini").setSubsamplingRate(1.0)
    val rf = new RandomForestClassifier().setLabelCol("label").setFeaturesCol("features").setMaxDepth(10).setNumTrees(50)
    // re-convertir le label obtenu en label d'origine (solvable, non solvable) et les inserer dans la colonne prediction
    val labelConverter = new IndexToString().setInputCol("prediction").setOutputCol("predicted_label").setLabels(labelIndexer.labels)
    val rf_pipeline = new Pipeline().setStages(Array(labelIndexer, rf, labelConverter))
    val rf_model = rf_pipeline.fit(train)
    val rf_predictions = rf_model.transform(test)


    // preparer la confusion metrix pour calculer les 'evaluation metrics' (accuracy, f1, recall, precision)
    // tp : true-positive ==> solvable, predit solvable
    // tn : true-negative ==> non solvable, predit non solvable
    // fp : false-positive ==> solvable, predit non solvable
    // fn : false-negative ==> non solvable, predit solvable
    //rf_predictions.show(false)
    val tp = rf_predictions.filter(col("Solvable") === "Oui" && col("predicted_label") === "Oui").count()
    val tn = rf_predictions.filter(col("Solvable") === "Non" && col("predicted_label") === "Non").count()
    val fp = rf_predictions.filter(col("Solvable") === "Non" && col("predicted_label") === "Oui").count()
    val fn = rf_predictions.filter(col("Solvable") === "Oui" && col("predicted_label") === "Non").count()

    val accuracy = (tp+tn).toDouble / (tp+fp+fn+tn).toDouble
    val precision = tp.toDouble / (tp+fp).toDouble
    val recall = tp.toDouble / (tp+fn).toDouble
    val f1_score = (2*recall * precision).toDouble / (recall + precision).toDouble

    println ("ACCURACY  ==> ", accuracy )
    println ("PRECISION ==> ", precision )
    println ("RECALL    ==> ", recall )
    println ("F1_SCORE  ==> ", f1_score )

    // sauvgarder le modele dans hdfs
    rf_model.write.overwrite().save("/user/ahmed.mnif/model_ds")


  }

}
