import org.apache.spark.sql.SparkSession

object Vision_uc1_datamart_factures_impayees_rattrapage {

  def main(args: Array[String]): Unit = {

    // création de spark session
    val spark = SparkSession.builder().appName("Vision_uc1_datamart_factures_impayees_rattrapage").master("yarn").enableHiveSupport().getOrCreate()
    // création de hive warehouse connection session
    //val hive = com.hortonworks.hwc.HiveWarehouseSession.session(spark).build()

    // chargement des données de datalake RCU dans les deux dataframes df_rcu et df_rcu_socio et persist dans la RAM
    // date_jour est le dérnier jour du mois précédent
    val df_rcu = spark.sql(" SELECT master_id, MSISDN FROM  (    SELECT master_id , MSISDN , ROW_NUMBER() OVER (PARTITION BY MSISDN ORDER BY date_creation_mid DESC) AS row_num  FROM   ( SELECT master_id , CASE WHEN LENGTH(TRIM(phone_num)) = 8 THEN CASE WHEN LIBELLE_TYPESERVICE = 'FIXE' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'WIMAX' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'MOBILE' THEN CASE WHEN SUBSTR(TRIM(phone_num),2,1) IN ('0','1','2','3') THEN CONCAT('01',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('4','5','6') THEN CONCAT('05',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('7','8','9') THEN CONCAT('07',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'AVISO' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'ORANGE MONEY' THEN CASE WHEN SUBSTR(TRIM(phone_num),2,1) IN ('0','1','2','3') THEN CONCAT('01',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('4','5','6') THEN CONCAT('05',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('7','8','9') THEN CONCAT('07',TRIM(phone_num)) ELSE TRIM(phone_num) END ELSE TRIM(phone_num) END ELSE TRIM(phone_num) END AS MSISDN  , date_creation_mid FROM vision_uc1_datalake.rcu where date_jour =                        (SELECT MAX(date_jour) FROM vision_uc1_datalake.rcu WHERE date_jour BETWEEN '"+args(0)+"' AND last_day ( '"+args(0)+"' ))                      ) first_req   ) second_req  WHERE row_num = 1 ")
      .persist()

    // chargement des données parc
    // date_jour est le dérnier jour du mois précédent
    val df_parc_mobile = spark.sql(" SELECT msisdn_parc, compte_client FROM ( SELECT DISTINCT TRIM(msisdn)       as msisdn_parc , TRIM(compte_client) as compte_client , ROW_NUMBER() OVER (PARTITION BY TRIM(msisdn) ORDER BY DATE_ACTIVATION DESC) AS row_num  FROM vision_uc1_datalake.bscs_parc_mobile  WHERE date_jour =                     (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_mobile WHERE date_jour BETWEEN '"+args(0)+"' AND last_day ('"+args(0)+"'))                  and type_offre='POSTPAID'                   ) first_req WHERE row_num=1  ")
    // date_jour est le dérnier jour du mois précédent
    val df_parc_adsl = spark.sql(" SELECT msisdn_parc, custcode      FROM ( SELECT DISTINCT TRIM(numero_adsl)  as msisdn_parc , TRIM(custcode) as custcode           , ROW_NUMBER() OVER (PARTITION BY TRIM(numero_adsl) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_adsl WHERE date_jour =            (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_adsl WHERE date_jour BETWEEN '"+args(0)+"' AND last_day ('"+args(0)+"'))                   ) first_req WHERE row_num=1 ")
    // date_jour est le dérnier jour du mois précédent
    val df_parc_ftth = spark.sql(" SELECT msisdn_parc, custcode      FROM ( SELECT DISTINCT TRIM(nd)           as msisdn_parc , TRIM(custcode) as custcode           , ROW_NUMBER() OVER (PARTITION BY TRIM(nd) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_ftth  WHERE date_jour =                    (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_ftth WHERE date_jour BETWEEN '"+args(0)+"' AND last_day ('"+args(0)+"'))                   AND  date_resiliation is NULL ) first_req WHERE row_num=1 ")
    // date_jour est le dérnier jour du mois précédent
    val df_parc_tdd = spark.sql(" SELECT msisdn_parc, custcode      FROM ( SELECT DISTINCT TRIM(nd)           as msisdn_parc , TRIM(custcode) as custcode           , ROW_NUMBER() OVER (PARTITION BY TRIM(nd) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_tdd  WHERE date_jour =                      (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_tdd WHERE date_jour BETWEEN '"+args(0)+"' AND last_day ('"+args(0)+"'))                  ) first_req WHERE row_num=1 ")
    // date_jour est le dérnier jour du mois précédent
    val df_parc_fdd = spark.sql(" SELECT msisdn_parc, custcode      FROM ( SELECT DISTINCT TRIM(nd)           as msisdn_parc , TRIM(custcode) as custcode           , ROW_NUMBER() OVER (PARTITION BY TRIM(nd) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_fdd   WHERE date_jour =                     (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_fdd WHERE date_jour BETWEEN '"+args(0)+"' AND last_day ('"+args(0)+"'))                   ) first_req WHERE row_num=1 ")



    // calcul de couple master_id, custcode entre parc et rcu
    val df_union_parc_msisdn_custcode = df_parc_mobile.withColumnRenamed("compte_client","custcode")
      .union(df_parc_adsl)
      .union(df_parc_ftth)
      .union(df_parc_fdd)
      .union(df_parc_tdd)
      .distinct()
      .dropDuplicates("msisdn_parc")

    // couple master_id / custcode distinct
    val df_union_parc_custcode_master_id = df_union_parc_msisdn_custcode
      .join(df_rcu, df_union_parc_msisdn_custcode("msisdn_parc")===df_rcu("msisdn") ,"LEFT")
      .select(df_rcu("master_id"), df_union_parc_msisdn_custcode("custcode"))
      .distinct()
      .dropDuplicates("custcode")



    // chargement des données bscs_factures_impayees du premier jour du mois précédent
    val df_bscs_broadband_facture_impaye = spark.sql("select NUMERO_FACTURE, sum(NVL(Montant_facture,0)) as MONTANT_TOTAL_FACTURE , date_emission AS DATE_EMISSION_FACTURE, periode, date_echeance, UNIVERS, categorie as categorie_client, TRIM(customer_code) as customer_code,  DATE_JOUR from vision_uc1_datalake.bscs_factures_impayees WHERE date_jour =          '"+args(0)+"'           group by NUMERO_FACTURE, date_emission, periode , date_echeance, UNIVERS, categorie, customer_code, DATE_JOUR")

    // ajout du master_id
    val df_bscs_broadband_facture_master_id = df_bscs_broadband_facture_impaye
      .join(df_union_parc_custcode_master_id, df_bscs_broadband_facture_impaye("customer_code") === df_union_parc_custcode_master_id("custcode"),"left" )
      .select(df_union_parc_custcode_master_id("master_id"),  df_bscs_broadband_facture_impaye("NUMERO_FACTURE"), df_bscs_broadband_facture_impaye("MONTANT_TOTAL_FACTURE"), df_bscs_broadband_facture_impaye("DATE_EMISSION_FACTURE"), df_bscs_broadband_facture_impaye("periode") ,df_bscs_broadband_facture_impaye("date_echeance"),df_bscs_broadband_facture_impaye("UNIVERS"),df_bscs_broadband_facture_impaye("categorie_client"),df_bscs_broadband_facture_impaye("DATE_JOUR") )
      .distinct()

    // insertion dans la datamart vision_dm_factures_impayees
    //df_vision_dm_facturation.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_facturation").save()
    df_bscs_broadband_facture_master_id.write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_factures_impayees")
    //df_vision_dm_facturation.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_facturation")




  }

}
