import org.apache.spark.sql.SparkSession

object Vision_datalake {

  def main(args: Array[String]): Unit = {

    // création de spark session
    val spark = SparkSession.builder().appName("Vision_datalake").master("yarn").enableHiveSupport().getOrCreate()
    // création de hive warehouse connection session
    // val hive = com.hortonworks.hwc.HiveWarehouseSession.session(spark).build()
    //args(0) ==> /warehouse/tablespace/external/hive/vision_uc1_datalake.db/${datalake}_external/
    val df=spark.read.orc(args(0))
    //args(1) ==> vision_uc1_datalake.${datalake}
    //df.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable(args(1))
    df.coalesce(10).write.format("orc").mode("overwrite").insertInto(args(1))

    //hive.close()
    spark.stop()
    spark.close()


  }


}
