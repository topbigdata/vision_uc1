// ce job va etre lancer chaque 2 de mois courant pour insérer les informations sur la payement des factures de mois dérnier
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.feature.{IndexToString, OneHotEncoderEstimator, StandardScaler, StringIndexer, VectorAssembler}
import org.apache.spark.ml.classification.RandomForestClassifier


object Vision_uc1_datamart_solvabilite {

  def main(args: Array[String]): Unit = {

    // création de spark session
    val spark = SparkSession.builder().appName("Vision_uc1_datamart_solvabilite").master("yarn").enableHiveSupport().getOrCreate()

    // importer les données de facturation (factures payées et factures non payées) du derniere mois
    val df_factures_payees =   spark.sql("SELECT master_id,  date_emission_facture ,    date_payement_facture      , montant_total_facture ,         montant_total_paye ,         mode_payement , CASE WHEN montant_total_facture=montant_total_paye THEN datediff(date_payement_facture,date_emission_facture) ELSE null END AS delai_edit_paim , CASE WHEN montant_total_facture=montant_total_paye THEN 'payee' ELSE 'impayee' END AS statut_facture , univers  , categorie_client  , numero_facture FROM vision_uc1_datamart.vision_dm_facturation       where master_id IS NOT NULL                        AND DATE_EMISSION_FACTURE BETWEEN            date_format(date_sub(current_date,25),'yyyy-01-01') AND date_format(date_sub(current_date,25),'yyyy-12-31')                        ")
    val df_factures_impayees = spark.sql("SELECT master_id , date_emission_facture , null as date_payement_facture , montant_total_facture , null as montant_total_paye , null as mode_payement ,                null    AS    delai_edit_paim                                                                                                   , null     AS       statut_facture                                                                     , univers  , categorie_client  , numero_facture FROM vision_uc1_datamart.vision_dm_factures_impayees where master_id IS NOT NULL                        AND date_emission_facture BETWEEN            date_format(date_sub(current_date,25),'yyyy-01-01') AND date_format(date_sub(current_date,25),'yyyy-12-31')                        ")

    // dataframe contenant toutes les factures de la derniere annee pour chaque client
    val df_fact = df_factures_payees.union(df_factures_impayees)

    // charger la table de mapping pour les categories clients
    val df_categ_mapping = spark.sql("SELECT distinct trim(categorie_client) as seg_client,mapping as segment_client FROM vision_uc1_datalake.file_categories_clients")

    // appliquer le mapping sur la dataframe facturation, et ajouter la colonne label (solvable ou pas)
    // si le client est dans le délai permis on le considère comme solvable
    val df_facturation = df_fact
      .join(df_categ_mapping, df_fact("categorie_client") === df_categ_mapping("seg_client"), "LEFT")
      .drop("seg_client")
      .withColumn("Solv",
        when(col("date_payement_facture").isNull && col("segment_client") === "B2C" && datediff(current_date(),col("date_emission_facture")) <= 25, "Oui").
          when(col("segment_client") === "B2C" && col("delai_edit_paim") <= 25, "Oui").
          when(col("date_payement_facture").isNull && col("segment_client") === "B2B" && datediff(current_date(),col("date_emission_facture")) <= 65,"Oui").
          when(col("segment_client") === "B2B" && col("delai_edit_paim") <= 65,"Oui").
          otherwise(lit("Non"))).dropDuplicates()



    // Travaux sur la colonne du label, grouper par master_id et idetidfier la solvabilite pour chacun ==> (master_id,Solvable)
    //regle appliquée : si le client etait une fois non solvable, il sera marqué comme non solvable
    val df_fact_solv = df_facturation.groupBy("master_id").agg(  collect_list("Solv").as("Solv")  ).withColumn("Solvable", when(array_contains(col("Solv"),"Non"), lit("Non")).otherwise(lit("Oui"))).select("master_id","Solvable")

    // Calculer les delais de paiement des factures ==> (master_id,delai_moyen_entre_edition_paiement_facture)
    val df_fact_pay = df_facturation.groupBy("master_id").agg(round(avg("delai_edit_paim")).cast("int").as("delai_moyen_entre_edition_paiement_facture"))

    // Dernier canal utilisé pour le paiement ==> (master_id,canaux_paiement_facture)
    val df_fact_canal = df_facturation.groupBy("master_id","mode_payement").agg(count("*").alias("cnt")).orderBy(desc("cnt")) .orderBy("master_id") .dropDuplicates("master_id").select("master_id","mode_payement")

    // Montant moyen des factures pour chaque client ==> (master_id, montant_moyen_factures)
    val df_mt_fact = df_facturation.groupBy("master_id").agg(round(avg("montant_total_facture")).cast("bigint").as("montant_moyen_factures"))

    // Date et Statut de la derniere facture pour chaque client ==> (master_id, date_edition_derniere_facture, statut_derniere_facture)
    val df_dern_fact = df_facturation.groupBy("master_id").agg(max("date_emission_facture").as("date_edition_derniere_facture"), last("statut_facture").as("statut_derniere_facture"))

    // Calculer le Nombre de factures non payees pour chaque client ==> (master_id,nb_factures_non_payees)
    val df_fact_imp = df_facturation.filter(col("statut_facture") === "impayee").groupBy("master_id").count().withColumnRenamed("count","nb_factures_non_payees").withColumn("nb_factures_non_payees",col("nb_factures_non_payees").cast("int"))



    //calculer l usage total de voix, sms, data pour chaque client
    val df_usage_tot = spark.sql("SELECT master_id, SUM(NVL(usage_voix_sortant,0)) as usage_voix_sortant , SUM(NVL(usage_sms_sortant,0)) as usage_sms_sortant, SUM(NVL(usage_data,0)) as usage_data FROM vision_uc1_datamart.vision_dm_usage where master_id IS NOT NULL                       AND date_jour BETWEEN              date_format(date_sub(current_date,25),'yyyy-01-01') AND date_format(date_sub(current_date,25),'yyyy-12-31')                       GROUP BY master_id ")

    //calculer le montant moyen de rechargement pour chaque client
    // master_id unique , moyen de montant_total_rechargement par client
    val df_rech_moy = spark.sql("SELECT master_id, AVG(montant_total_rechargement) as moyenne_rechargement FROM vision_uc1_datamart.vision_dm_rechargement where master_id IS NOT NULL                       AND date_jour BETWEEN            date_format(date_sub(current_date,25),'yyyy-01-01') AND date_format(date_sub(current_date,25),'yyyy-12-31')                       GROUP BY master_id")

    //calculer le nombre de reclamations pour chaque client
    // master_id unique , count des réclamation par client
    val df_rec_nbr = spark.sql("SELECT master_id, count(*) as nbr_reclamations from vision_uc1_datamart.vision_dm_exp_client_reclamation where master_id IS NOT NULL                       AND date_jour BETWEEN          date_format(date_sub(current_date,25),'yyyy-01-01') AND date_format(date_sub(current_date,25),'yyyy-12-31')                       GROUP BY master_id ")

    // master_id unique , dérnier motif order by date_jour
    val df_rec_motif = spark.sql("SELECT master_id, motif FROM ( SELECT master_id, motif , ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY date_jour DESC )  AS row_num from vision_uc1_datamart.vision_dm_exp_client_reclamation where master_id IS NOT NULL                       AND date_jour BETWEEN                date_format(date_sub(current_date,25),'yyyy-01-01') AND date_format(date_sub(current_date,25),'yyyy-12-31')                      )  firest_req   WHERE  row_num = 1")

    //nombre de jours suspendu pour chaque client
    val df_offre_susp = spark.sql("SELECT master_id, count(*) as nb_suspensions FROM vision_uc1_datamart.vision_dm_offre where master_id is not null and statut_technique = 'Suspendu'                       AND date_jour BETWEEN              date_format(date_sub(current_date,25),'yyyy-01-01') AND date_format(date_sub(current_date,25),'yyyy-12-31')                     group by master_id")

    //Retourner l'anciennete en terme de nombre de jours
    // Master_id unique , date_aquisation minimale ,  anciennete en terme de jours sur la dérnière partition
    //val df_offre_anc = spark.sql("SELECT master_id, date_aquisation , datediff(current_date , date_aquisation) as anciennete  FROM ( SELECT master_id, date_aquisation, ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY date_aquisation )  AS row_num  FROM vision_uc1_datamart.vision_dm_offre where master_id IS NOT NULL AND date_jour = date_sub(current_date,1))  firest_req   WHERE  row_num = 1")
    //==> ici j'ai constaté qu'il y a plusieurs master_id qui sont NULL
    //la derniere partition qui contient des master_id not null est  2022-03-14
    //donc pour remplir la datamart, j'ai changé la date manuellement ( 2022-03-14 a la place de j-1 ==> date_sub(current_date,1) )
    val df_offre_anc = spark.sql("SELECT master_id , datediff(current_date , date_aquisation) as anciennete  FROM ( SELECT master_id, date_aquisation, ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY date_aquisation )  AS row_num  FROM vision_uc1_datamart.vision_dm_offre where master_id IS NOT NULL                       AND date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre where date_jour<=last_day(date_sub(current_date,25)) )                       )  firest_req   WHERE  row_num = 1")

    // master_id unique , dérnier segment_client (a discuter, on est pas encore mis d'accord sur la regle)
    //val df_new_seg = spark.sql("SELECT master_id, mapping as segment_client FROM vision_uc1_datamart.vision_dm_offre offre, vision_uc1_datalake.file_categories_clients mapp where master_id IS NOT NULL AND offre.date_jour = date_sub(current_date,1) AND offre.categorie_client = trim(mapp.categorie_client)")
    val df_new_seg = spark.sql("SELECT master_id , segment_client FROM (SELECT master_id, mapping as segment_client , ROW_NUMBER() OVER (PARTITION BY offre.master_id ORDER BY offre.date_aquisation DESC ) AS row_num FROM vision_uc1_datamart.vision_dm_offre offre, vision_uc1_datalake.file_categories_clients mapp where master_id IS NOT NULL                       AND offre.date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre where date_jour<=last_day(date_sub(current_date,25)) )                       AND offre.categorie_client = trim(mapp.categorie_client)   ) firest_req   WHERE  row_num = 1")

    // master_id unique , profilConvergence ==> mono_univers / multi_univers sur la dérnière partition
    val df_offre_converg = spark.sql("SELECT DISTINCT master_id, univers FROM  vision_uc1_datamart.vision_dm_offre where master_id IS NOT NULL                       AND date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre where date_jour<=last_day(date_sub(current_date,25)) )                       ").
      groupBy("master_id").agg(count("*").alias("cnt")).
      withColumn("profil_de_convergence" , when(col("cnt") === 1,"mono_univers").otherwise("multi_univers")).drop("cnt")

    // Jointure entre toutes le df
    val df_solvabilite = df_fact_pay
      .join(df_fact_canal, Seq("master_id"), "left").
      join(df_mt_fact, Seq("master_id"), "left").
      join(df_dern_fact, Seq("master_id"), "left").
      join(df_fact_imp, Seq("master_id"), "left").
      join(df_fact_solv, Seq("master_id"), "left").
      join(df_new_seg, Seq("master_id"), "left").
      join(df_offre_anc, Seq("master_id"), "left").
      join(df_offre_converg, Seq("master_id"), "left").
      join(df_offre_susp, Seq("master_id"), "left").
      join(df_usage_tot, Seq("master_id"), "left").
      join(df_rech_moy, Seq("master_id"), "left").
      join(df_rec_nbr, Seq("master_id"), "left").na.fill(0).
      join(df_rec_motif, Seq("master_id"), "left").na.fill("-")
      .withColumn("date_jour",date_format(date_sub(current_date(),25),"yyyy-01-01"))

    // insertion dans la datamart vision_dm_solvabilite
    //df_offre_final.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_offre").save()
    df_solvabilite.coalesce(1).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_solvabilite")
    //df_offre_final.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_offre")




    // calcul et création du modèle

    val dm_solvabilite_model = spark.sql("select * from vision_uc1_datamart.vision_dm_solvabilite where date_jour=date_format(date_sub(current_date(),25),'yyyy-01-01')  ")

    // data preprocessing
    def transform_data(data: DataFrame): DataFrame = {
      // string indexer pour les colonnes categoriques
      val fact_pay_indexer = new StringIndexer().setInputCol("statut_derniere_facture").setOutputCol("statut_derniere_facture_index")
      val seg_client_indexer = new StringIndexer().setInputCol("segment_client").setOutputCol("segment_client_index")
      val conv_indexer = new StringIndexer().setInputCol("profil_de_convergence").setOutputCol("profil_de_convergence_index")
      // One hot encoding pour les 'categorical features'
      val encoder = new OneHotEncoderEstimator().setInputCols(Array("statut_derniere_facture_index", "segment_client_index", "profil_de_convergence_index")).setOutputCols(Array("statut_derniere_facture_encoded", "segment_client_encoded", "profil_de_convergence_encoded"))
      // vector assembling pour les 'numerical features' (le modele ML dans Spark accepte un seul attribut : vecteur , contenant toutes les colonnes a implementer)
      val num_assembler = new VectorAssembler().setInputCols(Array("delai_moyen_entre_edition_paiement_facture", "montant_moyen_factures", "nb_factures_non_payees", "anciennete", "nb_suspensions", "usage_voix_sortant", "usage_sms_sortant", "usage_data", "moyenne_rechargement", "nbr_reclamations")).setOutputCol("num_assembled")
      // appliquer le 'feature scaling' pour les donnees numeriques (pour avoir le meme range pour les 'nemrical features')
      val num_scaler = new StandardScaler().setInputCol("num_assembled").setOutputCol("scaled_num").setWithStd(true)
      // vector assembling pour toutes les colonnes
      val assembler = new VectorAssembler().setInputCols(Array("scaled_num", "statut_derniere_facture_encoded", "segment_client_encoded", "profil_de_convergence_encoded")).setOutputCol("features")
      // preparer le pipeline a suivre pour le feature engineering (indexing, encoding, scaling, assembling)
      val pipeline = new Pipeline().setStages(Array(fact_pay_indexer, seg_client_indexer, conv_indexer, encoder, num_assembler, num_scaler, assembler))
      // appliquer le pipeline sur notre data
      val df_processed = pipeline.fit(data).transform(data)
      df_processed
    }



    val df_solvabilite_transformed = transform_data(dm_solvabilite_model)
    // split data 80% train 20% test
   // val Array(training, test) = transform_data(dm_solvabilite).randomSplit(Array(0.8, 0.2), seed = 10)
    // undersampling pour avoir le nombre de oui == non
    // compter le nombre de oui
    val nb = df_solvabilite_transformed.filter(col("Solvable") === "Oui").count()
    // undersampling pour avoir nbr oui == nbr non (pour la partie training)
    // dans notre cas, undersampling est preferable par rapport au oversampling pour ne pas influencer le modele
    val train_non = df_solvabilite_transformed.filter(col("Solvable") === "Non").orderBy(rand()).limit(nb.toInt)
    val train_oui = df_solvabilite_transformed.filter(col("Solvable") === "Oui")
    // train contient finalement des donnees avec labels equilibrés
    val train = train_non.union(train_oui).orderBy(rand())



    //nous allons travailler avec random forest dans notre cas
    //RF
    // indexer le label (solvable, non solvable)
    val labelIndexer = new StringIndexer().setInputCol("solvable").setOutputCol("label").fit(train)
    // le choix des parametres est fait en utilisant GridSearch+CrossValidation (operation lourde dure jsq 13 heures)
    // on peut utiliser les parametres par defaut du modele, et si on n'obtient pas la precision attendue on fait recours aux GridSearch+CrossValidation
    // val rf = new RandomForestClassifier().setLabelCol("label").setFeaturesCol("features").setMaxBins(32).setMaxDepth(10).setNumTrees(50).setMaxMemoryInMB(256).setMinInfoGain(0.0).setMinInstancesPerNode(1).setSeed(207336481).setCheckpointInterval(10).setImpurity("gini").setSubsamplingRate(1.0)
    val rf = new RandomForestClassifier().setLabelCol("label").setFeaturesCol("features").setMaxDepth(10).setNumTrees(50)
    // re-convertir le label obtenu en label d'origine (solvable, non solvable) et les inserer dans la colonne prediction
    val labelConverter = new IndexToString().setInputCol("prediction").setOutputCol("predicted_label").setLabels(labelIndexer.labels)
    val rf_pipeline = new Pipeline().setStages(Array(labelIndexer, rf, labelConverter))
    val rf_model = rf_pipeline.fit(train)
    //val rf_predictions = rf_model.transform(test)


    // sauvgarder le modele dans hdfs
    rf_model.write.overwrite().save("/user/vision_cpt/model_ds")




  }
}
