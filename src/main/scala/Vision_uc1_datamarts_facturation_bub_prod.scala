import org.apache.spark.sql.SparkSession

object Vision_uc1_datamarts_facturation_bub_prod {

  def main(args: Array[String]): Unit = {

    // création de spark session
    val spark = SparkSession.builder().appName("Vision_uc1_datamarts_facturation_bub_prod").master("yarn").enableHiveSupport().getOrCreate()
    // création de hive warehouse connection session
    //val hive = com.hortonworks.hwc.HiveWarehouseSession.session(spark).build()

    // chargement des données de datalake RCU dans les deux dataframes df_rcu et df_rcu_socio et persist dans la RAM
    // date_jour est le dérnier jour du mois précédent
    val df_rcu = spark.sql (" SELECT master_id, MSISDN FROM  (    SELECT master_id , MSISDN , ROW_NUMBER() OVER (PARTITION BY MSISDN ORDER BY date_creation_mid DESC) AS row_num  FROM   ( SELECT master_id , CASE WHEN LENGTH(TRIM(phone_num)) = 8 THEN CASE WHEN LIBELLE_TYPESERVICE = 'FIXE' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'WIMAX' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'MOBILE' THEN CASE WHEN SUBSTR(TRIM(phone_num),2,1) IN ('0','1','2','3') THEN CONCAT('01',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('4','5','6') THEN CONCAT('05',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('7','8','9') THEN CONCAT('07',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'AVISO' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'ORANGE MONEY' THEN CASE WHEN SUBSTR(TRIM(phone_num),2,1) IN ('0','1','2','3') THEN CONCAT('01',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('4','5','6') THEN CONCAT('05',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('7','8','9') THEN CONCAT('07',TRIM(phone_num)) ELSE TRIM(phone_num) END ELSE TRIM(phone_num) END ELSE TRIM(phone_num) END AS MSISDN  , date_creation_mid FROM vision_uc1_datalake.rcu where date_jour =              (SELECT MAX(date_jour) FROM vision_uc1_datalake.rcu WHERE date_jour BETWEEN date_format ( date_sub(current_date,20),'yyyy-MM-01' ) AND last_day ( date_sub(current_date,20) ))                   ) first_req   ) second_req  WHERE row_num = 1 " )
      .persist()

    // chargement des données parc
    // date_jour est le dérnier jour du mois précédent
    val df_parc_mobile = spark.sql (" SELECT msisdn_parc, compte_client FROM ( SELECT DISTINCT TRIM(msisdn)       as msisdn_parc , TRIM(compte_client) as compte_client , ROW_NUMBER() OVER (PARTITION BY TRIM(msisdn) ORDER BY DATE_ACTIVATION DESC) AS row_num  FROM vision_uc1_datalake.bscs_parc_mobile  WHERE date_jour =                     (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_mobile WHERE date_jour BETWEEN date_format ( date_sub(current_date,20),'yyyy-MM-01' ) AND last_day ( date_sub(current_date,20) ))                          ) first_req WHERE row_num=1  ")
    // date_jour est le dérnier jour du mois précédent
    val df_parc_adsl = spark.sql   (" SELECT msisdn_parc, custcode      FROM ( SELECT DISTINCT TRIM(numero_adsl)  as msisdn_parc , TRIM(custcode) as custcode           , ROW_NUMBER() OVER (PARTITION BY TRIM(numero_adsl) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_adsl WHERE date_jour =          (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_adsl WHERE date_jour BETWEEN date_format ( date_sub(current_date,20),'yyyy-MM-01' ) AND last_day ( date_sub(current_date,20) ))                 ) first_req WHERE row_num=1 ")
    // date_jour est le dérnier jour du mois précédent
    val df_parc_ftth = spark.sql   (" SELECT msisdn_parc, custcode      FROM ( SELECT DISTINCT TRIM(nd)           as msisdn_parc , TRIM(custcode) as custcode           , ROW_NUMBER() OVER (PARTITION BY TRIM(nd) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_ftth  WHERE date_jour =                  (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_ftth WHERE date_jour BETWEEN date_format ( date_sub(current_date,20),'yyyy-MM-01' ) AND last_day ( date_sub(current_date,20) ))            AND  date_resiliation is NULL ) first_req WHERE row_num=1 ")
    // date_jour est le dérnier jour du mois précédent
    val df_parc_tdd = spark.sql    (" SELECT msisdn_parc, custcode      FROM ( SELECT DISTINCT TRIM(nd)           as msisdn_parc , TRIM(custcode) as custcode           , ROW_NUMBER() OVER (PARTITION BY TRIM(nd) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_tdd  WHERE date_jour =                   (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_tdd WHERE date_jour BETWEEN date_format ( date_sub(current_date,20),'yyyy-MM-01' ) AND last_day ( date_sub(current_date,20) ))                 ) first_req WHERE row_num=1 ")
    // date_jour est le dérnier jour du mois précédent
    val df_parc_fdd = spark.sql    (" SELECT msisdn_parc, custcode      FROM ( SELECT DISTINCT TRIM(nd)           as msisdn_parc , TRIM(custcode) as custcode           , ROW_NUMBER() OVER (PARTITION BY TRIM(nd) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_fdd   WHERE date_jour =                  (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_fdd WHERE date_jour BETWEEN date_format ( date_sub(current_date,20),'yyyy-MM-01' ) AND last_day ( date_sub(current_date,20) ))                   ) first_req WHERE row_num=1 ")




    //********************************************* Ajout de master_id à chaque PARC

    // ajout du master_id au parc mobile
    val df_parc_mobile_master_id = df_parc_mobile
      .join(df_rcu, df_parc_mobile("msisdn_parc")===df_rcu("msisdn") ,"LEFT")
      .select(df_rcu("master_id"), df_parc_mobile("compte_client")).distinct().dropDuplicates("compte_client")

    // ajout du master_id au parc adsl
    val df_parc_adsl_master_id = df_parc_adsl
      .join(df_rcu, df_parc_adsl("msisdn_parc")===df_rcu("msisdn") ,"LEFT")
      .select(df_rcu("master_id"), df_parc_adsl("custcode")).distinct().dropDuplicates("custcode")

    // ajout du master_id au parc ftth
    val df_parc_ftth_master_id = df_parc_ftth
      .join(df_rcu, df_parc_ftth("msisdn_parc")===df_rcu("msisdn") ,"LEFT")
      .select(df_rcu("master_id"), df_parc_ftth("custcode")).distinct().dropDuplicates("custcode")

    // ajout du master_id au parc tdd
    val df_parc_tdd_master_id = df_parc_tdd
      .join(df_rcu, df_parc_tdd("msisdn_parc")===df_rcu("msisdn") ,"LEFT")
      .select(df_rcu("master_id"), df_parc_tdd("custcode")).distinct().dropDuplicates("custcode")

    // ajout du master_id au parc fdd
    val df_parc_fdd_master_id = df_parc_fdd
      .join(df_rcu, df_parc_fdd("msisdn_parc")===df_rcu("msisdn") ,"LEFT")
      .select(df_rcu("master_id"), df_parc_fdd("custcode")).distinct().dropDuplicates("custcode")



    //********************************************* Calcul des données des facturation ajout de technologie

    // chargement des données de la datalake des facture mobile postpaid et braodband
    // date_jour est le premier du mois courant qui désigne les facture de mois dérnier
    val df_factures = spark.sql("SELECT TRIM(comple_client) as custcode, TRIM(rateplan) as rateplan, SUM(amount_ttc) as amount_ttc, date_jour FROM vision_uc1_datalake.dwocit_facturation_bub_prod WHERE date_jour =                date_format(current_date,'yyyy-MM-01')                   GROUP BY comple_client, TRIM(rateplan), date_jour ")

    // chargement des données de la datalake rateplan et technologie
    // date_jour est le dérnier jour du mois précédent
    val df_technologie = spark.sql("SELECT DISTINCT TRIM(RATEPLAN) as rateplan ,TRIM(TECHNOLOGIE) as technologie, TRIM(UNIVERS) as univers from vision_uc1_datalake.file_mapping_mob_broadb_b2b_b2c where date_jour =               (SELECT MAX(date_jour) FROM vision_uc1_datalake.file_mapping_mob_broadb_b2b_b2c WHERE date_jour BETWEEN date_format ( date_sub(current_date,20),'yyyy-MM-01' ) AND last_day ( date_sub(current_date,20) )      )            ")

    // ajout de la colonne technologie
    val df_factures_technologie = df_factures
      .join(df_technologie, df_factures("rateplan")===df_technologie("rateplan"),"LEFT")
      .select(df_factures("custcode"),df_factures("amount_ttc"),df_factures("rateplan"), df_technologie("technologie"), df_technologie("univers"),df_factures("date_jour") )



    //********************************************* calcul de chaque technologie

    //************************ PARC MOBILE
    val df_factures_mobile = df_factures_technologie
      .filter(df_factures_technologie("technologie") isin  ("Mobile_POSTPAID","Mobile_HYBRID","Mobile_POSTPAID","Mobile_HYBRID POSTPAID"))

    val df_factures_mobile_master_id = df_factures_mobile
      .join(df_parc_mobile_master_id, df_factures_mobile("custcode") === df_parc_mobile_master_id("compte_client"),"LEFT")
      .select(df_parc_mobile_master_id("master_id"), df_factures_mobile("custcode"),df_factures_mobile("amount_ttc"),df_factures_mobile("rateplan"),df_factures_mobile("technologie"),df_factures_mobile("univers"),df_factures_mobile("date_jour") )

    df_factures_mobile_master_id.write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_ca_factures_mobile")

    //************************ PARC ADSL
    val df_factures_adsl = df_factures_technologie
      .filter(df_factures_technologie("technologie") === "ADSL")

    val df_factures_adsl_master_id = df_factures_adsl
      .join(df_parc_adsl_master_id, df_factures_adsl("custcode") === df_parc_adsl_master_id("custcode"),"LEFT")
      .select(df_parc_adsl_master_id("master_id"), df_factures_adsl("custcode"),df_factures_adsl("amount_ttc"),df_factures_adsl("rateplan"),df_factures_adsl("technologie"),df_factures_adsl("univers"),df_factures_adsl("date_jour") )

    df_factures_adsl_master_id.write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_ca_factures_adsl")

    //************************ PARC FTTH
    val df_factures_ftth = df_factures_technologie
      .filter(df_factures_technologie("technologie") === "FTTH")

    val df_factures_ftth_master_id = df_factures_ftth
      .join(df_parc_ftth_master_id, df_factures_ftth("custcode") === df_parc_ftth_master_id("custcode"),"LEFT")
      .select(df_parc_ftth_master_id("master_id"), df_factures_ftth("custcode"),df_factures_ftth("amount_ttc"),df_factures_ftth("rateplan"),df_factures_ftth("technologie"),df_factures_ftth("univers"),df_factures_ftth("date_jour") )

    df_factures_ftth_master_id.write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_ca_factures_ftth")

    //************************ PARC TDD
    val df_factures_tdd = df_factures_technologie
      .filter(df_factures_technologie("technologie") === "TDD")

    val df_factures_tdd_master_id = df_factures_tdd
      .join(df_parc_tdd_master_id, df_factures_tdd("custcode") === df_parc_tdd_master_id("custcode"),"LEFT")
      .select(df_parc_tdd_master_id("master_id"), df_factures_tdd("custcode"),df_factures_tdd("amount_ttc"),df_factures_tdd("rateplan"),df_factures_tdd("technologie"),df_factures_tdd("univers"),df_factures_tdd("date_jour") )

    df_factures_tdd_master_id.write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_ca_factures_tdd")

    //************************ PARC FDD
    val df_factures_fdd = df_factures_technologie
      .filter(df_factures_technologie("technologie") === "FDD")

    val df_factures_fdd_master_id = df_factures_fdd
      .join(df_parc_fdd_master_id, df_factures_fdd("custcode") === df_parc_fdd_master_id("custcode"),"LEFT")
      .select(df_parc_fdd_master_id("master_id"), df_factures_fdd("custcode"),df_factures_fdd("amount_ttc"),df_factures_fdd("rateplan"),df_factures_fdd("technologie"),df_factures_fdd("univers"),df_factures_fdd("date_jour") )

    df_factures_fdd_master_id.write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_ca_factures_fdd")



    spark.stop()
    spark.close()



  }

}
