import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import com.mongodb.spark._

object Vision_uc1_datamarts_vue360 {

  def main(args: Array[String]): Unit = {

    // création de spark session
    val spark = SparkSession.builder().appName("Vision_uc1_datamarts_vue360").master("yarn").enableHiveSupport().getOrCreate()
    // création de hive warehouse connection session
   // val hive = com.hortonworks.hwc.HiveWarehouseSession.session(spark).build()






    if (args(0) == "current_month") {


      // table 1 ****************************************************************************************************************** vision_dm_client_physique

      // lecture des données de la datamart vision_uc1_datamart.vision_dm_socio_pro_geolocalisation
      //master_id unique , nom_prenom , date_naissance , profession , ville ,quartier  , commune, email, date_jour
      val df_dm_socio_pro_geolocalisation = spark.sql(" SELECT master_id||'_'||date_format(date_jour,'yyyy-MM') as _id , master_id , nom_prenom , date_naissance , profession , ville ,quartier  , commune, email, date_jour FROM  (select  master_id , nom_prenom , date_naissance , profession , ville_idbase as ville ,quartier_idbase as quartier  , commune_idbase as commune, email_parc as email , ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY date_identification_idbase DESC) AS row_num , date_sub(current_date(),1) as date_jour FROM vision_uc1_datamart.vision_dm_socio_pro_geolocalisation   WHERE                           date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_socio_pro_geolocalisation )                AND master_id is not null               ) first_req WHERE row_num=1 ")

      // lecture des données de la datamart vision_uc1_datamart.vision_dm_offre
      val df_dm_offre_nb_statut = spark.sql(" SELECT master_id as master_id_offre, univers||':'|| COUNT(*) AS univers_nb  FROM vision_uc1_datamart.vision_dm_offre WHERE              date_jour =          ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre )                 AND master_id is not null      GROUP BY master_id, univers  ")
        .groupBy("master_id_offre")
        .agg(collect_set("univers_nb").as("univers_nb"))
        .withColumn("univers_nb", concat_ws("#", col("univers_nb")))

      // lecture des données de la datamart vision_uc1_datamart.vision_dm_usage master_id unique
      val df_dm_usage_nb_univers = spark.sql(" SELECT master_id as master_id_usage, univers||':'|| SUM(NVL(usage_voix_sortant,0)) AS usage_voix_sortant, univers||':'|| SUM(NVL(usage_sms_sortant,0)) AS usage_sms_sortant , univers||':'|| SUM(NVL(usage_data,0)) AS usage_data FROM vision_uc1_datamart.vision_dm_usage WHERE date_jour                             between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)                      AND master_id is not null       group by master_id, univers ")
        .groupBy("master_id_usage")
        .agg(collect_set("usage_voix_sortant").as("usage_voix_sortant"), collect_set("usage_sms_sortant").as("usage_sms_sortant"), collect_set("usage_data").as("usage_data"))
        .withColumn("usage_voix_sortant", concat_ws("#", col("usage_voix_sortant")))
        .withColumn("usage_sms_sortant", concat_ws("#", col("usage_sms_sortant")))
        .withColumn("usage_data", concat_ws("#", col("usage_data")))

      // lecture des données de la datamart vision_uc1_datamart.vision_dm_orange_money master_id unique
      val df_dm_orange_money_ca = spark.sql(" SELECT master_id as master_id_om, 'ca_om_' || service_de_base || ':' || SUM(NVL(chiffre_affaire,0)) AS ca_om  FROM vision_uc1_datamart.vision_dm_orange_money WHERE       date_jour                  between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)                     and master_id is not null       GROUP BY master_id, service_de_base  ")
        .groupBy("master_id_om")
        .agg(collect_set("ca_om").as("ca_om"))
        .withColumn("ca_om", concat_ws("#", col("ca_om")))


      // lecture des données des datamarts de chiffre d'affaire master_id unique
      val df_ca_fixe_postpaid = spark.sql(" select master_id as master_id_ca    , 'FIXE' as univers  ,    SUM(NVL(mnt_solde_fact_fixe,0)) as ca_global FROM vision_uc1_datamart.vision_dm_ca_fixe_postpaid WHERE          date_jour               between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)              and master_id is not null   group by master_id ")
      val df_dm_ca_mobile_prepaid = spark.sql(" select master_id as master_id_ca, 'MOBILE' as univers,    SUM(NVL(montant_appel,0)) + SUM(NVL(montant_sms,0)) + SUM(NVL(montant_achat_pass,0)) + SUM(NVL(montant_sva,0)) + SUM(NVL(montant_rembourse,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_prepaid WHERE     date_jour              between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)              and master_id is not null  group by master_id ")
      val df_dm_ca_mobile_hybrid = spark.sql(" select master_id as master_id_ca , 'MOBILE' as univers,    SUM(NVL(montant_appel,0)) + SUM(NVL(montant_sms,0)) + SUM(NVL(montant_achat_pass,0)) + SUM(NVL(montant_sva,0)) + SUM(NVL(montant_rembourse,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_hybrid WHERE     date_jour              between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)             and master_id is not null   group by master_id")
      val df_dm_ca_mobile_postpaid = spark.sql("select master_id as master_id_ca, 'MOBILE' as univers,    SUM(NVL(montant_achat_pass,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_postpaid WHERE     date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)  and master_id is not null   group by master_id")
      val df_dm_ca_adsl = spark.sql(" select master_id as master_id_ca         , 'ADSL' as univers,      SUM(NVL(ca_rechargement_ods,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_adsl WHERE     date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)  and master_id is not null  group by master_id ")
      val df_dm_ca_ftth = spark.sql(" select master_id as master_id_ca         , 'FTTH' as univers,      SUM(NVL(ca_base_contrat,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_ftth WHERE     date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)  and master_id is not null  group by master_id ")
      val df_dm_ca_tdd = spark.sql(" select master_id as master_id_ca          , 'TDD' as univers,       SUM(NVL(ca_e_recharge,0)) + SUM(NVL(rechargemet_om,0)) + SUM(NVL(rechargemet_om,0)) + SUM(NVL(ca_base_contrat,0)) + SUM(NVL(ca_rechargement_ods,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_tdd WHERE     date_jour              between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)             and master_id is not null  group by master_id   ")
      val df_dm_ca_fdd = spark.sql(" select master_id as master_id_ca          , 'FDD' as univers,       SUM(NVL(ca_e_recharge,0)) + SUM(NVL(rechargemet_om,0)) + SUM(NVL(ca_rechargement_ods,0)) as ca_global FROM vision_uc1_datamart.vision_dm_ca_fdd WHERE     date_jour                between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)            and master_id is not null group by master_id  ")
      // le premier jour du mois prochain
      val df_dm_ca_factures_mobile = spark.sql(" select master_id as master_id_ca , 'MOBILE' as univers, SUM(NVL(amount_ttc,0)) AS ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_mobile  WHERE       date_jour=date_format(add_months(current_date,1),'yyyy-MM-01')           and master_id is not null       group by master_id ")
      val df_dm_ca_factures_adsl = spark.sql(" select master_id as master_id_ca    , 'ADSL' as univers,   SUM(NVL(amount_ttc,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_adsl  WHERE       date_jour=date_format(add_months(current_date,1),'yyyy-MM-01')           and master_id is not null       group by master_id ")
      val df_dm_ca_factures_ftth = spark.sql(" select master_id as master_id_ca    , 'FTTH' as univers,   SUM(NVL(amount_ttc,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_ftth  WHERE       date_jour=date_format(add_months(current_date,1),'yyyy-MM-01')           and master_id is not null       group by master_id ")
      val df_dm_ca_factures_tdd = spark.sql(" select master_id as master_id_ca     , 'TDD' as univers,    SUM(NVL(amount_ttc,0))   as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_tdd  WHERE       date_jour=date_format(add_months(current_date,1),'yyyy-MM-01')           and master_id is not null       group by master_id ")
      val df_dm_ca_factures_fdd = spark.sql(" select master_id as master_id_ca     , 'FDD' as univers,    SUM(NVL(amount_ttc,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_fdd   WHERE       date_jour=date_format(add_months(current_date,1),'yyyy-MM-01')           and master_id is not null       group by master_id ")

      val df_ca_mobile = df_dm_ca_mobile_prepaid.union(df_dm_ca_mobile_hybrid).union(df_dm_ca_mobile_postpaid).union(df_dm_ca_factures_mobile).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_adsl = df_dm_ca_adsl.union(df_dm_ca_factures_adsl).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_ftth = df_dm_ca_ftth.union(df_dm_ca_factures_ftth).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_tdd = df_dm_ca_tdd.union(df_dm_ca_factures_tdd).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_fdd = df_dm_ca_fdd.union(df_dm_ca_factures_fdd).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))

      val ca_global = df_ca_fixe_postpaid.union(df_ca_mobile).union(df_ca_adsl).union(df_ca_ftth).union(df_ca_tdd).union(df_ca_fdd)
        .withColumn("ca_global", concat_ws(":", col("univers"), col("ca_global")))
        .select("master_id_ca", "ca_global")
        .groupBy("master_id_ca")
        .agg(collect_set("ca_global").as("ca_global"))
        .withColumn("ca_global", concat_ws("#", col("ca_global")))


      //création de la dataframe finale
      val join1 = df_dm_socio_pro_geolocalisation
        .join(df_dm_offre_nb_statut, df_dm_socio_pro_geolocalisation("master_id") === df_dm_offre_nb_statut("master_id_offre"), "LEFT")
        .join(df_dm_usage_nb_univers, df_dm_socio_pro_geolocalisation("master_id") === df_dm_usage_nb_univers("master_id_usage"), "LEFT")
        .join(df_dm_orange_money_ca, df_dm_socio_pro_geolocalisation("master_id") === df_dm_orange_money_ca("master_id_om"), "LEFT")
        .join(ca_global, df_dm_socio_pro_geolocalisation("master_id") === ca_global("master_id_ca"), "LEFT")
        .select(
          df_dm_socio_pro_geolocalisation("_id"),
          df_dm_socio_pro_geolocalisation("master_id"),
          df_dm_socio_pro_geolocalisation("date_jour"),
          df_dm_socio_pro_geolocalisation("nom_prenom"),
          df_dm_socio_pro_geolocalisation("date_naissance"),
          df_dm_socio_pro_geolocalisation("profession"),
          df_dm_socio_pro_geolocalisation("ville"),
          df_dm_socio_pro_geolocalisation("quartier"),
          df_dm_socio_pro_geolocalisation("commune"),
          df_dm_socio_pro_geolocalisation("email"),
          df_dm_offre_nb_statut("univers_nb"),
          df_dm_usage_nb_univers("usage_voix_sortant"),
          df_dm_usage_nb_univers("usage_sms_sortant"),
          df_dm_usage_nb_univers("usage_data"),
          df_dm_orange_money_ca("ca_om"),
          ca_global("ca_global"))

      //join1.write.format("org.apache.phoenix.spark").mode("overwrite").option("table", "VISION_DM_CLIENT_PHYSIQUE").option("zkUrl", "10.240.36.86,10.240.36.87,10.240.36.90:2181").save()


      MongoSpark.save(join1.write.option("spark.mongodb.output.uri", "mongodb://OciAppVision:3zNgDFE5kuQp9bEs@10.242.30.114/ociVision.vision_dm_client_physique?authSource=admin").mode("append"))



      // table 2 ****************************************************************** vision_dm_msisdn

      //master_id, msisdn, univers, statut_technique, statut_business, date_jour
      val df_dm_offre_msisdn = spark.sql(" SELECT master_id||'_'||msisdn||'_'||univers||'_'||date_format(date_jour,'yyyy-MM') as _id, master_id, msisdn, univers, statut_technique, statut_business, date_sub(current_date(),1) as date_jour FROM vision_uc1_datamart.vision_dm_offre WHERE                   date_jour =  ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre )              and master_id is not null            GROUP BY  master_id, msisdn, univers, statut_technique, statut_business, date_jour  ")

      //usage_voix_sortant , usage_sms_sortant
      val df_dm_usage_voix_sms_nb_types = spark.sql(" SELECT msisdn as msisdn_a, type||':'|| SUM(NVL(usage_voix_sortant,0)) AS usage_voix_sortant, type||':'|| SUM(NVL(usage_sms_sortant,0)) AS usage_sms_sortant FROM vision_uc1_datamart.vision_dm_usage WHERE                date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)                 group by msisdn, type ")
        .groupBy("msisdn_a")
        .agg(collect_set("usage_voix_sortant").as("usage_voix_sortant"), collect_set("usage_sms_sortant").as("usage_sms_sortant"))
        .withColumn("usage_voix_sortant", concat_ws("#", col("usage_voix_sortant")))
        .withColumn("usage_sms_sortant", concat_ws("#", col("usage_sms_sortant")))

      //usage_data
      val df_dm_usage_data_nb_types = spark.sql("SELECT msisdn_s, type||':'|| SUM(NVL(usage_data,0)) AS usage_data FROM ( SELECT msisdn as msisdn_s, case when type='offnet' then 'national' when type='onnet' then 'national' when type='international' then 'national' else type end AS type, usage_data FROM vision_uc1_datamart.vision_dm_usage WHERE              date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)                ) req_1         group by msisdn_s, type ")
        .groupBy("msisdn_s")
        .agg(collect_set("usage_data").as("usage_data"))
        .withColumn("usage_data", concat_ws("#", col("usage_data")))

      //montant_achat_pass
      val df_dm_conso_achats_bundles_type_pass = spark.sql(" SELECT msisdn as msisdn_b, TRIM(type_de_pass) || ':' || SUM(NVL(montant_achat_pass,0)) AS montant_achat_pass  FROM vision_uc1_datamart.vision_dm_conso_achats_bundles WHERE               date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)               GROUP BY msisdn, type_de_pass  ")
        .groupBy("msisdn_b")
        .agg(collect_set("montant_achat_pass").as("montant_achat_pass"))
        .withColumn("montant_achat_pass", concat_ws("#", col("montant_achat_pass")))

      //montant_rembourse_sos
      val df_dm_conso_credit_sos_service = spark.sql(" SELECT msisdn as msisdn_c, TRIM(service) || ':' || SUM(NVL(montant_rembourse,0)) AS montant_rembourse_sos  FROM vision_uc1_datamart.vision_dm_conso_credit_sos WHERE               date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)              GROUP BY msisdn, service  ")
        .groupBy("msisdn_c")
        .agg(collect_set("montant_rembourse_sos").as("montant_rembourse_sos"))
        .withColumn("montant_rembourse_sos", concat_ws("#", col("montant_rembourse_sos")))

      //montant_transactions_om
      val df_dm_orange_money_ca_msisdn = spark.sql(" SELECT msisdn as msisdn_d, 'ca_om_' || service_de_base || ':' || SUM(NVL(montant_transaction,0)) AS montant_transactions_om  FROM vision_uc1_datamart.vision_dm_orange_money WHERE             date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)             GROUP BY msisdn, service_de_base  ")
        .groupBy("msisdn_d")
        .agg(collect_set("montant_transactions_om").as("montant_transactions_om"))
        .withColumn("montant_transactions_om", concat_ws("#", col("montant_transactions_om")))

      //montants_achat_sva
      val df_dm_conso_achats_sva_msisdn = spark.sql(" select msisdn as msisdn_e, SUM(NVL(montant_sva,0)) AS montants_achat_sva FROM vision_uc1_datamart.vision_dm_conso_achats_sva where               date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)               group by msisdn ")

      //montant_rechargement
      val df_dm_rechargement = spark.sql(" select msisdn as msisdn_f, SUM(NVL(montant_total_rechargement,0)) AS montant_rechargement FROM vision_uc1_datamart.vision_dm_rechargement where             date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)               group by msisdn ")

      // conso_appel et conso_sms
      val df_dm_consommation_msisdn = spark.sql(" select msisdn as msisdn_g, SUM(NVL(montant_appel,0)) AS conso_appel, SUM(NVL(montant_sms,0)) AS conso_sms FROM vision_uc1_datamart.vision_dm_consommation where             date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)             group by msisdn ")

      //achat_bundle
      val df_dm_conso_achats_bundles_msisdn = spark.sql(" select msisdn as msisdn_h, SUM(NVL(montant_achat_pass,0)) AS achat_bundle FROM vision_uc1_datamart.vision_dm_conso_achats_bundles where             date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)                 group by msisdn ")

      //nb_appels_aboutis , temps_passe_ivr , nb_reiteration
      val df_dm_exp_client_call_center = spark.sql(" select msisdn as msisdn_u ,SUM(NVL(nb_appels_aboutis,0)) as nb_appels_aboutis , SUM(NVL(temps_passe_ivr,0)) as  temps_passe_ivr ,SUM(NVL(nb_reiteration,0)) as  nb_reiteration FROM vision_uc1_datamart.vision_dm_exp_client_call_center where              date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)              group by msisdn")

      val df_dm_exp_client_reclamation = spark.sql(" SELECT msisdn as msisdn_o, SUM(NVL(nb_prise_contact,0)) as nbr_prise_contact_reseau_sociaux from vision_uc1_datamart.vision_dm_exp_client_reclamation where               date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1) AND canal='Facebook'              group by msisdn")

      // moyenne des DMT par MSISDN distinct
      val df_dm_exp_client_reclamation_moy_dmt = spark.sql(" SELECT msisdn as msisdn_x, AVG(NVL(DMT,0)) as dmt from vision_uc1_datamart.vision_dm_exp_client_reclamation where               date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)              group by msisdn")

      // nbr reclamation par type
      val df_dm_exp_client_reclamation_nb_contact_type = spark.sql("SELECT msisdn as msisdn_y, categorie||':'|| SUM(NVL(nb_prise_contact,0)) AS nb_prise_contact_par_type FROM vision_uc1_datamart.vision_dm_exp_client_reclamation WHERE              date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)                   group by msisdn, categorie ")
        .groupBy("msisdn_y")
        .agg(collect_set("nb_prise_contact_par_type").as("nb_prise_contact_par_type"))
        .withColumn("nb_prise_contact_par_type", concat_ws("#", col("nb_prise_contact_par_type")))

      val df_dm_ca_mobile_prepaid_msisdn = spark.sql(" select msisdn as msisdn_z,'MOBILE' as univers, SUM(NVL(montant_appel,0)) + SUM(NVL(montant_sms,0)) + SUM(NVL(montant_achat_pass,0)) + SUM(NVL(montant_sva,0)) + SUM(NVL(montant_rembourse,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_prepaid where            date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)           group by msisdn ")
      val df_dm_ca_mobile_hybride_msisdn = spark.sql(" select msisdn as msisdn_z,'MOBILE' as univers, SUM(NVL(montant_appel,0)) + SUM(NVL(montant_sms,0)) + SUM(NVL(montant_achat_pass,0)) + SUM(NVL(montant_sva,0)) + SUM(NVL(montant_rembourse,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_hybrid  where            date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)           group by msisdn")
      val df_dm_ca_mobile_postpaid_msisdn = spark.sql("select msisdn as msisdn_z,'MOBILE' as univers, SUM(NVL(montant_achat_pass,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_postpaid where                  date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)              group by msisdn")

      val df_ca_mobile_msisdn = df_dm_ca_mobile_prepaid_msisdn.union(df_dm_ca_mobile_hybride_msisdn).union(df_dm_ca_mobile_postpaid_msisdn)
        .withColumn("ca_global", concat_ws(":", col("univers"), col("ca_global")))
        .select("msisdn_z", "ca_global")


      // affcer sans master_id
      //ajouter le chiffre d'affaire global

      val join2 = df_dm_offre_msisdn
        .join(df_dm_usage_voix_sms_nb_types, df_dm_offre_msisdn("msisdn") === df_dm_usage_voix_sms_nb_types("msisdn_a"), "LEFT")
        .join(df_dm_usage_data_nb_types, df_dm_offre_msisdn("msisdn") === df_dm_usage_data_nb_types("msisdn_s"), "LEFT")
        .join(df_dm_conso_achats_bundles_type_pass, df_dm_offre_msisdn("msisdn") === df_dm_conso_achats_bundles_type_pass("msisdn_b"), "LEFT")
        .join(df_dm_conso_credit_sos_service, df_dm_offre_msisdn("msisdn") === df_dm_conso_credit_sos_service("msisdn_c"), "LEFT")
        .join(df_dm_orange_money_ca_msisdn, df_dm_offre_msisdn("msisdn") === df_dm_orange_money_ca_msisdn("msisdn_d"), "LEFT")
        .join(df_dm_conso_achats_sva_msisdn, df_dm_offre_msisdn("msisdn") === df_dm_conso_achats_sva_msisdn("msisdn_e"), "LEFT")
        .join(df_dm_rechargement, df_dm_offre_msisdn("msisdn") === df_dm_rechargement("msisdn_f"), "LEFT")
        .join(df_dm_consommation_msisdn, df_dm_offre_msisdn("msisdn") === df_dm_consommation_msisdn("msisdn_g"), "LEFT")
        .join(df_dm_conso_achats_bundles_msisdn, df_dm_offre_msisdn("msisdn") === df_dm_conso_achats_bundles_msisdn("msisdn_h"), "LEFT")
        .join(df_dm_exp_client_call_center, df_dm_offre_msisdn("msisdn") === df_dm_exp_client_call_center("msisdn_u"), "LEFT")
        .join(df_dm_exp_client_reclamation, df_dm_offre_msisdn("msisdn") === df_dm_exp_client_reclamation("msisdn_o"), "LEFT")
        .join(df_dm_exp_client_reclamation_moy_dmt, df_dm_offre_msisdn("msisdn") === df_dm_exp_client_reclamation_moy_dmt("msisdn_x"), "LEFT")
        .join(df_dm_exp_client_reclamation_nb_contact_type, df_dm_offre_msisdn("msisdn") === df_dm_exp_client_reclamation_nb_contact_type("msisdn_y"), "LEFT")
        .join(df_ca_mobile_msisdn, df_dm_offre_msisdn("msisdn") === df_ca_mobile_msisdn("msisdn_z"), "LEFT")
        .select(
          df_dm_offre_msisdn("_id"),
          df_dm_offre_msisdn("master_id"),
          df_dm_offre_msisdn("msisdn"),
          df_dm_offre_msisdn("date_jour"),
          df_dm_offre_msisdn("univers"),
          df_dm_offre_msisdn("statut_technique"),
          df_dm_offre_msisdn("statut_business"),
          df_dm_usage_voix_sms_nb_types("usage_voix_sortant"),
          df_dm_usage_voix_sms_nb_types("usage_sms_sortant"),
          df_dm_usage_data_nb_types("usage_data"),
          df_dm_conso_achats_bundles_type_pass("montant_achat_pass"),
          df_dm_conso_credit_sos_service("montant_rembourse_sos"),
          df_dm_orange_money_ca_msisdn("montant_transactions_om"),
          df_dm_conso_achats_sva_msisdn("montants_achat_sva"),
          df_dm_rechargement("montant_rechargement"),
          df_dm_consommation_msisdn("conso_appel"),
          df_dm_consommation_msisdn("conso_sms"),
          df_dm_conso_achats_bundles_msisdn("achat_bundle"),
          df_dm_exp_client_call_center("nb_appels_aboutis"),
          df_dm_exp_client_call_center("temps_passe_ivr"),
          df_dm_exp_client_call_center("nb_reiteration"),
          df_dm_exp_client_reclamation("nbr_prise_contact_reseau_sociaux"),
          df_dm_exp_client_reclamation_moy_dmt("dmt"),
          df_dm_exp_client_reclamation_nb_contact_type("nb_prise_contact_par_type"),
          df_ca_mobile_msisdn("ca_global"))


      // montant_rembourse_sos  + montants_achat_sva + conso_appel + conso_sms + achat_bundle

      MongoSpark.save(join2.write.option("spark.mongodb.output.uri", "mongodb://OciAppVision:3zNgDFE5kuQp9bEs@10.242.30.114/ociVision.vision_dm_msisdn?authSource=admin").mode("append"))





    }














    else if (args(0) == "last_month") {


      // table 1 ****************************************************************************************************************** vision_dm_client_physique

      // lecture des données de la datamart vision_uc1_datamart.vision_dm_socio_pro_geolocalisation
      //master_id unique , nom_prenom , date_naissance , profession , ville ,quartier  , commune, email, date_jour
      val df_dm_socio_pro_geolocalisation = spark.sql(" SELECT master_id||'_'||date_format(date_jour,'yyyy-MM') as _id , master_id , nom_prenom , date_naissance , profession , ville ,quartier  , commune, email, date_jour FROM  (select  master_id , nom_prenom , date_naissance , profession , ville_idbase as ville ,quartier_idbase as quartier  , commune_idbase as commune, email_parc as email , ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY date_identification_idbase DESC) AS row_num , last_day(add_months(current_date, -1)) as date_jour FROM vision_uc1_datamart.vision_dm_socio_pro_geolocalisation   WHERE                           date_jour = (   SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_socio_pro_geolocalisation  WHERE date_jour BETWEEN date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))    )                AND master_id is not null               ) first_req WHERE row_num=1 ")

      // lecture des données de la datamart vision_uc1_datamart.vision_dm_offre
      val df_dm_offre_nb_statut = spark.sql(" SELECT master_id as master_id_offre, univers||':'|| COUNT(*) AS univers_nb  FROM vision_uc1_datamart.vision_dm_offre WHERE              date_jour =  (  SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre  WHERE date_jour BETWEEN date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))   )                 AND master_id is not null      GROUP BY master_id, univers  ")
        .groupBy("master_id_offre")
        .agg(collect_set("univers_nb").as("univers_nb"))
        .withColumn("univers_nb", concat_ws("#", col("univers_nb")))

      // lecture des données de la datamart vision_uc1_datamart.vision_dm_usage master_id unique
      val df_dm_usage_nb_univers = spark.sql(" SELECT master_id as master_id_usage, univers||':'|| SUM(NVL(usage_voix_sortant,0)) AS usage_voix_sortant, univers||':'|| SUM(NVL(usage_sms_sortant,0)) AS usage_sms_sortant , univers||':'|| SUM(NVL(usage_data,0)) AS usage_data FROM vision_uc1_datamart.vision_dm_usage WHERE date_jour                             between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))                      AND master_id is not null       group by master_id, univers ")
        .groupBy("master_id_usage")
        .agg(collect_set("usage_voix_sortant").as("usage_voix_sortant"), collect_set("usage_sms_sortant").as("usage_sms_sortant"), collect_set("usage_data").as("usage_data"))
        .withColumn("usage_voix_sortant", concat_ws("#", col("usage_voix_sortant")))
        .withColumn("usage_sms_sortant", concat_ws("#", col("usage_sms_sortant")))
        .withColumn("usage_data", concat_ws("#", col("usage_data")))

      // lecture des données de la datamart vision_uc1_datamart.vision_dm_orange_money master_id unique
      val df_dm_orange_money_ca = spark.sql(" SELECT master_id as master_id_om, 'ca_om_' || service_de_base || ':' || SUM(NVL(chiffre_affaire,0)) AS ca_om  FROM vision_uc1_datamart.vision_dm_orange_money WHERE       date_jour                  between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))                    and master_id is not null       GROUP BY master_id, service_de_base  ")
        .groupBy("master_id_om")
        .agg(collect_set("ca_om").as("ca_om"))
        .withColumn("ca_om", concat_ws("#", col("ca_om")))


      // lecture des données des datamarts de chiffre d'affaire master_id unique
      val df_ca_fixe_postpaid = spark.sql(" select master_id as master_id_ca    , 'FIXE' as univers  ,    SUM(NVL(mnt_solde_fact_fixe,0)) as ca_global FROM vision_uc1_datamart.vision_dm_ca_fixe_postpaid WHERE          date_jour               between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))              and master_id is not null   group by master_id ")
      val df_dm_ca_mobile_prepaid = spark.sql(" select master_id as master_id_ca, 'MOBILE' as univers,    SUM(NVL(montant_appel,0)) + SUM(NVL(montant_sms,0)) + SUM(NVL(montant_achat_pass,0)) + SUM(NVL(montant_sva,0)) + SUM(NVL(montant_rembourse,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_prepaid WHERE     date_jour              between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))              and master_id is not null  group by master_id ")
      val df_dm_ca_mobile_hybrid = spark.sql(" select master_id as master_id_ca , 'MOBILE' as univers,    SUM(NVL(montant_appel,0)) + SUM(NVL(montant_sms,0)) + SUM(NVL(montant_achat_pass,0)) + SUM(NVL(montant_sva,0)) + SUM(NVL(montant_rembourse,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_hybrid WHERE     date_jour              between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))             and master_id is not null   group by master_id")
      val df_dm_ca_mobile_postpaid = spark.sql("select master_id as master_id_ca, 'MOBILE' as univers,    SUM(NVL(montant_achat_pass,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_postpaid WHERE     date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))  and master_id is not null   group by master_id")
      val df_dm_ca_adsl = spark.sql(" select master_id as master_id_ca         , 'ADSL' as univers,      SUM(NVL(ca_rechargement_ods,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_adsl WHERE     date_jour between      date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))    and master_id is not null  group by master_id ")
      val df_dm_ca_ftth = spark.sql(" select master_id as master_id_ca         , 'FTTH' as univers,      SUM(NVL(ca_base_contrat,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_ftth WHERE     date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))  and master_id is not null  group by master_id ")
      val df_dm_ca_tdd = spark.sql(" select master_id as master_id_ca          , 'TDD' as univers,       SUM(NVL(ca_e_recharge,0)) + SUM(NVL(rechargemet_om,0)) + SUM(NVL(rechargemet_om,0)) + SUM(NVL(ca_base_contrat,0)) + SUM(NVL(ca_rechargement_ods,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_tdd WHERE     date_jour              between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))             and master_id is not null  group by master_id   ")
      val df_dm_ca_fdd = spark.sql(" select master_id as master_id_ca          , 'FDD' as univers,       SUM(NVL(ca_e_recharge,0)) + SUM(NVL(rechargemet_om,0)) + SUM(NVL(ca_rechargement_ods,0)) as ca_global FROM vision_uc1_datamart.vision_dm_ca_fdd WHERE     date_jour                between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))            and master_id is not null group by master_id  ")
      // le premier jour du mois courant
      val df_dm_ca_factures_mobile = spark.sql(" select master_id as master_id_ca , 'MOBILE' as univers, SUM(NVL(amount_ttc,0)) AS ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_mobile  WHERE       date_jour=date_format(current_date,'yyyy-MM-01')           and master_id is not null       group by master_id ")
      val df_dm_ca_factures_adsl = spark.sql(" select master_id as master_id_ca    , 'ADSL' as univers,   SUM(NVL(amount_ttc,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_adsl  WHERE       date_jour=date_format(current_date,'yyyy-MM-01')           and master_id is not null       group by master_id ")
      val df_dm_ca_factures_ftth = spark.sql(" select master_id as master_id_ca    , 'FTTH' as univers,   SUM(NVL(amount_ttc,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_ftth  WHERE       date_jour=date_format(current_date,'yyyy-MM-01')           and master_id is not null       group by master_id ")
      val df_dm_ca_factures_tdd = spark.sql(" select master_id as master_id_ca     , 'TDD' as univers,    SUM(NVL(amount_ttc,0))   as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_tdd  WHERE       date_jour=date_format(current_date,'yyyy-MM-01')           and master_id is not null       group by master_id ")
      val df_dm_ca_factures_fdd = spark.sql(" select master_id as master_id_ca     , 'FDD' as univers,    SUM(NVL(amount_ttc,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_fdd   WHERE       date_jour=date_format(current_date,'yyyy-MM-01')           and master_id is not null       group by master_id ")

      val df_ca_mobile = df_dm_ca_mobile_prepaid.union(df_dm_ca_mobile_hybrid).union(df_dm_ca_mobile_postpaid).union(df_dm_ca_factures_mobile).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_adsl = df_dm_ca_adsl.union(df_dm_ca_factures_adsl).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_ftth = df_dm_ca_ftth.union(df_dm_ca_factures_ftth).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_tdd = df_dm_ca_tdd.union(df_dm_ca_factures_tdd).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_fdd = df_dm_ca_fdd.union(df_dm_ca_factures_fdd).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))

      val ca_global = df_ca_fixe_postpaid.union(df_ca_mobile).union(df_ca_adsl).union(df_ca_ftth).union(df_ca_tdd).union(df_ca_fdd)
        .withColumn("ca_global", concat_ws(":", col("univers"), col("ca_global")))
        .select("master_id_ca", "ca_global")
        .groupBy("master_id_ca")
        .agg(collect_set("ca_global").as("ca_global"))
        .withColumn("ca_global", concat_ws("#", col("ca_global")))


      //création de la dataframe finale
      val join1 = df_dm_socio_pro_geolocalisation
        .join(df_dm_offre_nb_statut, df_dm_socio_pro_geolocalisation("master_id") === df_dm_offre_nb_statut("master_id_offre"), "LEFT")
        .join(df_dm_usage_nb_univers, df_dm_socio_pro_geolocalisation("master_id") === df_dm_usage_nb_univers("master_id_usage"), "LEFT")
        .join(df_dm_orange_money_ca, df_dm_socio_pro_geolocalisation("master_id") === df_dm_orange_money_ca("master_id_om"), "LEFT")
        .join(ca_global, df_dm_socio_pro_geolocalisation("master_id") === ca_global("master_id_ca"), "LEFT")
        .select(
          df_dm_socio_pro_geolocalisation("_id"),
          df_dm_socio_pro_geolocalisation("master_id"),
          df_dm_socio_pro_geolocalisation("date_jour"),
          df_dm_socio_pro_geolocalisation("nom_prenom"),
          df_dm_socio_pro_geolocalisation("date_naissance"),
          df_dm_socio_pro_geolocalisation("profession"),
          df_dm_socio_pro_geolocalisation("ville"),
          df_dm_socio_pro_geolocalisation("quartier"),
          df_dm_socio_pro_geolocalisation("commune"),
          df_dm_socio_pro_geolocalisation("email"),
          df_dm_offre_nb_statut("univers_nb"),
          df_dm_usage_nb_univers("usage_voix_sortant"),
          df_dm_usage_nb_univers("usage_sms_sortant"),
          df_dm_usage_nb_univers("usage_data"),
          df_dm_orange_money_ca("ca_om"),
          ca_global("ca_global"))

      //join1.write.format("org.apache.phoenix.spark").mode("overwrite").option("table", "VISION_DM_CLIENT_PHYSIQUE").option("zkUrl", "10.240.36.86,10.240.36.87,10.240.36.90:2181").save()


      MongoSpark.save(join1.write.option("spark.mongodb.output.uri", "mongodb://OciAppVision:3zNgDFE5kuQp9bEs@10.242.30.114/ociVision.vision_dm_client_physique?authSource=admin").mode("append"))



      // table 2 ****************************************************************** vision_dm_msisdn

      //master_id, msisdn, univers, statut_technique, statut_business, date_jour
      val df_dm_offre_msisdn = spark.sql(" SELECT master_id||'_'||msisdn||'_'||univers||'_'||date_format(date_jour,'yyyy-MM') as _id, master_id, msisdn, univers, statut_technique, statut_business, last_day(add_months(current_date, -1)) as date_jour FROM vision_uc1_datamart.vision_dm_offre WHERE                   date_jour =  ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre WHERE date_jour BETWEEN date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1)) )              and master_id is not null            GROUP BY  master_id, msisdn, univers, statut_technique, statut_business, date_jour  ")

      //usage_voix_sortant , usage_sms_sortant
      val df_dm_usage_voix_sms_nb_types = spark.sql(" SELECT msisdn as msisdn_a, type||':'|| SUM(NVL(usage_voix_sortant,0)) AS usage_voix_sortant, type||':'|| SUM(NVL(usage_sms_sortant,0)) AS usage_sms_sortant FROM vision_uc1_datamart.vision_dm_usage WHERE                date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))                 group by msisdn, type ")
        .groupBy("msisdn_a")
        .agg(collect_set("usage_voix_sortant").as("usage_voix_sortant"), collect_set("usage_sms_sortant").as("usage_sms_sortant"))
        .withColumn("usage_voix_sortant", concat_ws("#", col("usage_voix_sortant")))
        .withColumn("usage_sms_sortant", concat_ws("#", col("usage_sms_sortant")))

      //usage_data
      val df_dm_usage_data_nb_types = spark.sql("SELECT msisdn_s, type||':'|| SUM(NVL(usage_data,0)) AS usage_data FROM ( SELECT msisdn as msisdn_s, case when type='offnet' then 'national' when type='onnet' then 'national' when type='international' then 'national' else type end AS type, usage_data FROM vision_uc1_datamart.vision_dm_usage WHERE              date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))                ) req_1         group by msisdn_s, type ")
        .groupBy("msisdn_s")
        .agg(collect_set("usage_data").as("usage_data"))
        .withColumn("usage_data", concat_ws("#", col("usage_data")))

      //montant_achat_pass
      val df_dm_conso_achats_bundles_type_pass = spark.sql(" SELECT msisdn as msisdn_b, TRIM(type_de_pass) || ':' || SUM(NVL(montant_achat_pass,0)) AS montant_achat_pass  FROM vision_uc1_datamart.vision_dm_conso_achats_bundles WHERE               date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))               GROUP BY msisdn, type_de_pass  ")
        .groupBy("msisdn_b")
        .agg(collect_set("montant_achat_pass").as("montant_achat_pass"))
        .withColumn("montant_achat_pass", concat_ws("#", col("montant_achat_pass")))

      //montant_rembourse_sos
      val df_dm_conso_credit_sos_service = spark.sql(" SELECT msisdn as msisdn_c, TRIM(service) || ':' || SUM(NVL(montant_rembourse,0)) AS montant_rembourse_sos  FROM vision_uc1_datamart.vision_dm_conso_credit_sos WHERE               date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))              GROUP BY msisdn, service  ")
        .groupBy("msisdn_c")
        .agg(collect_set("montant_rembourse_sos").as("montant_rembourse_sos"))
        .withColumn("montant_rembourse_sos", concat_ws("#", col("montant_rembourse_sos")))

      //montant_transactions_om
      val df_dm_orange_money_ca_msisdn = spark.sql(" SELECT msisdn as msisdn_d, 'ca_om_' || service_de_base || ':' || SUM(NVL(montant_transaction,0)) AS montant_transactions_om  FROM vision_uc1_datamart.vision_dm_orange_money WHERE             date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))             GROUP BY msisdn, service_de_base  ")
        .groupBy("msisdn_d")
        .agg(collect_set("montant_transactions_om").as("montant_transactions_om"))
        .withColumn("montant_transactions_om", concat_ws("#", col("montant_transactions_om")))

      //montants_achat_sva
      val df_dm_conso_achats_sva_msisdn = spark.sql(" select msisdn as msisdn_e, SUM(NVL(montant_sva,0)) AS montants_achat_sva FROM vision_uc1_datamart.vision_dm_conso_achats_sva where               date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))               group by msisdn ")

      //montant_rechargement
      val df_dm_rechargement = spark.sql(" select msisdn as msisdn_f, SUM(NVL(montant_total_rechargement,0)) AS montant_rechargement FROM vision_uc1_datamart.vision_dm_rechargement where             date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))               group by msisdn ")

      // conso_appel et conso_sms
      val df_dm_consommation_msisdn = spark.sql(" select msisdn as msisdn_g, SUM(NVL(montant_appel,0)) AS conso_appel, SUM(NVL(montant_sms,0)) AS conso_sms FROM vision_uc1_datamart.vision_dm_consommation where             date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))             group by msisdn ")

      //achat_bundle
      val df_dm_conso_achats_bundles_msisdn = spark.sql(" select msisdn as msisdn_h, SUM(NVL(montant_achat_pass,0)) AS achat_bundle FROM vision_uc1_datamart.vision_dm_conso_achats_bundles where             date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))                 group by msisdn ")

      //nb_appels_aboutis , temps_passe_ivr , nb_reiteration
      val df_dm_exp_client_call_center = spark.sql(" select msisdn as msisdn_u ,SUM(NVL(nb_appels_aboutis,0)) as nb_appels_aboutis , SUM(NVL(temps_passe_ivr,0)) as  temps_passe_ivr ,SUM(NVL(nb_reiteration,0)) as  nb_reiteration FROM vision_uc1_datamart.vision_dm_exp_client_call_center where              date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))              group by msisdn")

      val df_dm_exp_client_reclamation = spark.sql(" SELECT msisdn as msisdn_o, SUM(NVL(nb_prise_contact,0)) as nbr_prise_contact_reseau_sociaux from vision_uc1_datamart.vision_dm_exp_client_reclamation where               date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))      AND canal='Facebook'              group by msisdn")


      // moyenne des DMT par MSISDN distinct
      val df_dm_exp_client_reclamation_moy_dmt = spark.sql(" SELECT msisdn as msisdn_x, AVG(NVL(DMT,0)) as dmt from vision_uc1_datamart.vision_dm_exp_client_reclamation where               date_jour between         date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))              group by msisdn")

      // nbr reclamation par type
      val df_dm_exp_client_reclamation_nb_contact_type = spark.sql("SELECT msisdn as msisdn_y, categorie||':'|| SUM(NVL(nb_prise_contact,0)) AS nb_prise_contact_par_type FROM vision_uc1_datamart.vision_dm_exp_client_reclamation WHERE              date_jour between         date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))                  group by msisdn, categorie ")
        .groupBy("msisdn_y")
        .agg(collect_set("nb_prise_contact_par_type").as("nb_prise_contact_par_type"))
        .withColumn("nb_prise_contact_par_type", concat_ws("#", col("nb_prise_contact_par_type")))



      val df_dm_ca_mobile_prepaid_msisdn = spark.sql(" select msisdn as msisdn_z,'MOBILE' as univers, SUM(NVL(montant_appel,0)) + SUM(NVL(montant_sms,0)) + SUM(NVL(montant_achat_pass,0)) + SUM(NVL(montant_sva,0)) + SUM(NVL(montant_rembourse,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_prepaid where            date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))           group by msisdn ")
      val df_dm_ca_mobile_hybride_msisdn = spark.sql(" select msisdn as msisdn_z,'MOBILE' as univers, SUM(NVL(montant_appel,0)) + SUM(NVL(montant_sms,0)) + SUM(NVL(montant_achat_pass,0)) + SUM(NVL(montant_sva,0)) + SUM(NVL(montant_rembourse,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_hybrid  where            date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))           group by msisdn")
      val df_dm_ca_mobile_postpaid_msisdn = spark.sql("select msisdn as msisdn_z,'MOBILE' as univers, SUM(NVL(montant_achat_pass,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_postpaid where                  date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))              group by msisdn")

      val df_ca_mobile_msisdn = df_dm_ca_mobile_prepaid_msisdn.union(df_dm_ca_mobile_hybride_msisdn).union(df_dm_ca_mobile_postpaid_msisdn)
        .withColumn("ca_global", concat_ws(":", col("univers"), col("ca_global")))
        .select("msisdn_z", "ca_global")


      // affcer sans master_id
      //ajouter le chiffre d'affaire global

      val join2 = df_dm_offre_msisdn
        .join(df_dm_usage_voix_sms_nb_types, df_dm_offre_msisdn("msisdn") === df_dm_usage_voix_sms_nb_types("msisdn_a"), "LEFT")
        .join(df_dm_usage_data_nb_types, df_dm_offre_msisdn("msisdn") === df_dm_usage_data_nb_types("msisdn_s"), "LEFT")
        .join(df_dm_conso_achats_bundles_type_pass, df_dm_offre_msisdn("msisdn") === df_dm_conso_achats_bundles_type_pass("msisdn_b"), "LEFT")
        .join(df_dm_conso_credit_sos_service, df_dm_offre_msisdn("msisdn") === df_dm_conso_credit_sos_service("msisdn_c"), "LEFT")
        .join(df_dm_orange_money_ca_msisdn, df_dm_offre_msisdn("msisdn") === df_dm_orange_money_ca_msisdn("msisdn_d"), "LEFT")
        .join(df_dm_conso_achats_sva_msisdn, df_dm_offre_msisdn("msisdn") === df_dm_conso_achats_sva_msisdn("msisdn_e"), "LEFT")
        .join(df_dm_rechargement, df_dm_offre_msisdn("msisdn") === df_dm_rechargement("msisdn_f"), "LEFT")
        .join(df_dm_consommation_msisdn, df_dm_offre_msisdn("msisdn") === df_dm_consommation_msisdn("msisdn_g"), "LEFT")
        .join(df_dm_conso_achats_bundles_msisdn, df_dm_offre_msisdn("msisdn") === df_dm_conso_achats_bundles_msisdn("msisdn_h"), "LEFT")
        .join(df_dm_exp_client_call_center, df_dm_offre_msisdn("msisdn") === df_dm_exp_client_call_center("msisdn_u"), "LEFT")
        .join(df_dm_exp_client_reclamation, df_dm_offre_msisdn("msisdn") === df_dm_exp_client_reclamation("msisdn_o"), "LEFT")
        .join(df_dm_exp_client_reclamation_moy_dmt, df_dm_offre_msisdn("msisdn") === df_dm_exp_client_reclamation_moy_dmt("msisdn_x"), "LEFT")
        .join(df_dm_exp_client_reclamation_nb_contact_type, df_dm_offre_msisdn("msisdn") === df_dm_exp_client_reclamation_nb_contact_type("msisdn_y"), "LEFT")
        .join(df_ca_mobile_msisdn, df_dm_offre_msisdn("msisdn") === df_ca_mobile_msisdn("msisdn_z"), "LEFT")
        .select(
          df_dm_offre_msisdn("_id"),
          df_dm_offre_msisdn("master_id"),
          df_dm_offre_msisdn("msisdn"),
          df_dm_offre_msisdn("date_jour"),
          df_dm_offre_msisdn("univers"),
          df_dm_offre_msisdn("statut_technique"),
          df_dm_offre_msisdn("statut_business"),
          df_dm_usage_voix_sms_nb_types("usage_voix_sortant"),
          df_dm_usage_voix_sms_nb_types("usage_sms_sortant"),
          df_dm_usage_data_nb_types("usage_data"),
          df_dm_conso_achats_bundles_type_pass("montant_achat_pass"),
          df_dm_conso_credit_sos_service("montant_rembourse_sos"),
          df_dm_orange_money_ca_msisdn("montant_transactions_om"),
          df_dm_conso_achats_sva_msisdn("montants_achat_sva"),
          df_dm_rechargement("montant_rechargement"),
          df_dm_consommation_msisdn("conso_appel"),
          df_dm_consommation_msisdn("conso_sms"),
          df_dm_conso_achats_bundles_msisdn("achat_bundle"),
          df_dm_exp_client_call_center("nb_appels_aboutis"),
          df_dm_exp_client_call_center("temps_passe_ivr"),
          df_dm_exp_client_call_center("nb_reiteration"),
          df_dm_exp_client_reclamation("nbr_prise_contact_reseau_sociaux"),
          df_dm_exp_client_reclamation_moy_dmt("dmt"),
          df_dm_exp_client_reclamation_nb_contact_type("nb_prise_contact_par_type"),
          df_ca_mobile_msisdn("ca_global"))


      // montant_rembourse_sos  + montants_achat_sva + conso_appel + conso_sms + achat_bundle

      MongoSpark.save(join2.write.option("spark.mongodb.output.uri", "mongodb://OciAppVision:3zNgDFE5kuQp9bEs@10.242.30.114/ociVision.vision_dm_msisdn?authSource=admin").mode("append"))




    }









    spark.stop()
    spark.close()



  }

}
