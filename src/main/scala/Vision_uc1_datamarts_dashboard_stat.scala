import com.mongodb.spark.MongoSpark
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{coalesce, col, date_sub, _}


object Vision_uc1_datamarts_dashboard_stat {

  def main(args: Array[String]): Unit = {


    // création de spark session
    val spark = SparkSession.builder().appName("Vision_uc1_datamarts_dashboard_stat").master("yarn").enableHiveSupport().getOrCreate()
    // création de hive warehouse connection session
    //val hive = com.hortonworks.hwc.HiveWarehouseSession.session(spark).build()




    if (args(0) == "current_month") {



      // ****************************************************************************************************************** Partie commun

      // chargement des colonnes master_id, msisdn DISTINCT, region, commune_idbase de la datamart socio pro & geoloc
      val df_socio_pro_geoloc_msisdn = spark.sql("SELECT master_id ,msisdn, count(*) as nb_msisdn  FROM (select master_id ,msisdn, ROW_NUMBER() OVER (PARTITION BY msisdn ORDER BY date_creation_mid_rcu DESC) AS row_num FROM vision_uc1_datamart.vision_dm_socio_pro_geolocalisation where                   date_jour =  ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_socio_pro_geolocalisation )   AND master_id is not null                ) first_req WHERE row_num=1 GROUP BY master_id ,msisdn ")

      // df_socio_pro_geoloc_segment par master_id unique
      val df_socio_pro_geoloc_segment_master_id = spark.sql("SELECT master_id, region ,commune, date_jour, count(*) as nbTotalClient  FROM (select master_id, region , commune_idbase as commune, ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY date_creation_mid_rcu DESC) AS row_num , date_sub(current_date(),1) as date_jour FROM vision_uc1_datamart.vision_dm_socio_pro_geolocalisation where                 date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_socio_pro_geolocalisation )   AND master_id is not null                ) first_req WHERE row_num=1 GROUP BY master_id, region ,commune, date_jour ")

      // nouveau_df = datamart offre (MSISDN,UNIVERS par dérnier date_aquisition) UNIQUES
      val df_dm_offre_univers = spark.sql("SELECT msisdn, univers FROM (SELECT msisdn, univers , ROW_NUMBER() OVER (PARTITION BY msisdn ORDER BY date_aquisation DESC) AS row_num FROM vision_uc1_datamart.vision_dm_offre where                 date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre ) AND statut_technique IN ('Actif','En attente','Suspendu')       ) first_req WHERE row_num=1 ")

      // lecture des données des datamarts de chiffre d'affaire
      val df_dm_ca_mobile_prepaid = spark.sql(" select master_id as master_id_ca,'MOBILE' as univers, SUM(NVL(montant_appel,0)) + SUM(NVL(montant_sms,0)) + SUM(NVL(montant_achat_pass,0)) + SUM(NVL(montant_sva,0)) + SUM(NVL(montant_rembourse,0)) as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_prepaid where           date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)             and master_id is not null          group by master_id ")
      val df_dm_ca_mobile_hybrid = spark.sql(" select master_id as master_id_ca,'MOBILE' as univers,  SUM(NVL(montant_appel,0)) + SUM(NVL(montant_sms,0)) + SUM(NVL(montant_achat_pass,0)) + SUM(NVL(montant_sva,0)) + SUM(NVL(montant_rembourse,0)) as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_hybrid where            date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)             and master_id is not null          group by master_id")
      val df_dm_ca_mobile_postpaid = spark.sql("select master_id as master_id_ca ,'MOBILE' as univers,  SUM(NVL(montant_achat_pass,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_postpaid where          date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1) and master_id is not null         group by master_id")
      val df_dm_ca_adsl = spark.sql(" select master_id as master_id_ca,'ADSL' as univers, SUM(NVL(ca_rechargement_ods,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_adsl where         date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1) and master_id is not null        group by master_id ")
      val df_dm_ca_ftth = spark.sql(" select master_id as master_id_ca ,'FTTH' as univers,   SUM(NVL(ca_base_contrat,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_ftth where          date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1) and master_id is not null        group by master_id ")
      val df_dm_ca_tdd = spark.sql(" select master_id as master_id_ca ,'TDD' as univers,   SUM(NVL(ca_e_recharge,0)) + SUM(NVL(rechargemet_om,0)) + SUM(NVL(rechargemet_om,0)) + SUM(NVL(ca_base_contrat,0)) + SUM(NVL(ca_rechargement_ods,0)) as ca_global FROM vision_uc1_datamart.vision_dm_ca_tdd where         date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1) and master_id is not null           group by master_id   ")
      val df_dm_ca_fdd = spark.sql(" select master_id as master_id_ca ,'FDD' as univers,   SUM(NVL(ca_e_recharge,0)) + SUM(NVL(rechargemet_om,0)) + SUM(NVL(ca_rechargement_ods,0)) as ca_global FROM vision_uc1_datamart.vision_dm_ca_fdd where         date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1) and master_id is not null          group by master_id  ")
      val df_dm_ca_factures_mobile = spark.sql(" select master_id as master_id_ca ,'MOBILE' as univers, SUM(NVL(amount_ttc,0)) AS ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_mobile where            date_jour=date_format(add_months(current_date,1),'yyyy-MM-01')               and master_id is not null            group by master_id ")
      val df_dm_ca_factures_adsl = spark.sql(" select master_id as master_id_ca ,'ADSL' as univers, SUM(NVL(amount_ttc,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_adsl where                 date_jour=date_format(add_months(current_date,1),'yyyy-MM-01')               and master_id is not null            group by master_id ")
      val df_dm_ca_factures_ftth = spark.sql(" select master_id as master_id_ca ,'FTTH' as univers, SUM(NVL(amount_ttc,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_ftth where                 date_jour=date_format(add_months(current_date,1),'yyyy-MM-01')               and master_id is not null            group by master_id ")
      val df_dm_ca_factures_tdd = spark.sql(" select master_id as master_id_ca ,'TDD' as univers, SUM(NVL(amount_ttc,0))   as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_tdd where                   date_jour=date_format(add_months(current_date,1),'yyyy-MM-01')               and master_id is not null            group by master_id ")
      val df_dm_ca_factures_fdd = spark.sql(" select master_id as master_id_ca ,'FDD' as univers, SUM(NVL(amount_ttc,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_fdd where                    date_jour=date_format(add_months(current_date,1),'yyyy-MM-01')               and master_id is not null            group by master_id ")

      // univers et master_id distinct
      val df_ca_fixe_postpaid = spark.sql(" select master_id as master_id_ca ,'FIXE' as univers, SUM(NVL(mnt_solde_fact_fixe,0)) as ca_global FROM vision_uc1_datamart.vision_dm_ca_fixe_postpaid where                 date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)  and master_id is not null                group by master_id ")
      val df_ca_mobile = df_dm_ca_mobile_prepaid.union(df_dm_ca_mobile_hybrid).union(df_dm_ca_mobile_postpaid).union(df_dm_ca_factures_mobile).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_adsl = df_dm_ca_adsl.union(df_dm_ca_factures_adsl).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_ftth = df_dm_ca_ftth.union(df_dm_ca_factures_ftth).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_tdd = df_dm_ca_tdd.union(df_dm_ca_factures_tdd).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_fdd = df_dm_ca_fdd.union(df_dm_ca_factures_fdd).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_om = spark.sql("select master_id as master_id_ca , 'OM' as univers, SUM(NVL(chiffre_affaire,0)) as ca_global FROM vision_uc1_datamart.vision_dm_orange_money where                    date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)  and master_id is not null                group by master_id")


      // ==> des master_id dupliqués
      val df_univers_ca_global = df_ca_fixe_postpaid.union(df_ca_mobile).union(df_ca_adsl).union(df_ca_ftth).union(df_ca_tdd).union(df_ca_fdd).union(df_ca_om)
        .groupBy("master_id_ca", "univers")
        .agg(sum("ca_global").as("ca_global"))
        .select("master_id_ca", "univers", "ca_global")

      //profilconvergence ==> Un client mono_univers, multi_univers par master_id ==> count distinct univers group by master_id
      // ==> des master_id distincts ==> master_id|cnt|profilconvergence
      val df_profileconvergence = df_univers_ca_global
        .groupBy("master_id_ca").agg(count("*").alias("cnt"))
        .withColumn("profilConvergence", when(col("cnt") === 1, "mono_univers").otherwise("multi_univers"))
        .withColumnRenamed("master_id_ca", "master_id_profile")

      //quartile ==> ajouter 4 tranches dans la table de la datamart CA
      // somme des CA par master_id pour calculer le quartile: ==> master_id distinct
      val df_ca_global = df_univers_ca_global
        .groupBy("master_id_ca")
        .agg(sum("ca_global").as("ca_global"))
        .select("master_id_ca", "ca_global")

      val Q1 = df_ca_global.filter(df_ca_global("ca_global") > 0).stat.approxQuantile("ca_global", Array(0.25), 0)(0)
      val Q2 = df_ca_global.filter(df_ca_global("ca_global") > 0).stat.approxQuantile("ca_global", Array(0.5), 0)(0)
      val Q3 = df_ca_global.filter(df_ca_global("ca_global") > 0).stat.approxQuantile("ca_global", Array(0.75), 0)(0)

      // ==> master_id unique
      val df_ca_global_tranche_valeur = df_ca_global.withColumn("trancheValeur",
        when(col("ca_global") === 0, "Very Low")
          .when(col("ca_global") <= Q1, "Low")
          .when(col("ca_global") > Q1 && col("ca_global") <= Q2, "Middle")
          .when(col("ca_global") > Q2 && col("ca_global") <= Q3, "High")
          .otherwise("Very high"))
        .withColumnRenamed("master_id_ca", "master_id_tranche")

      // table 1 ****************************************************************************************************************** vision_dm_client_stat

      // chargement des colonnes msisdn DISTINCT, nbr_contact_call_center
      // source Genesys ==> |master_id DISTINCT |nbr_contact_call_center|
      val df_nb_contact_call_center = spark.sql("select msisdn as msisdn_nb_call ,SUM( NVL(nb_appels_aboutis,0) ) as nbr_contact_call_center FROM vision_uc1_datamart.vision_dm_exp_client_call_center where                date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)            group by msisdn ")

      // chargement des colonnes msisdn DISTINCT, nb_contact_reseau_sociaux
      // source Jade ==> master_id DISTINCT |nbr_contact_reseaux_sociaux|
      val df_nb_contact_reseau_sociaux = spark.sql("SELECT msisdn as msisdn_nb_res , SUM( NVL(nb_prise_contact,0) ) as nbr_contact_reseaux_sociaux FROM vision_uc1_datamart.vision_dm_exp_client_reclamation where                 date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)              AND canal='Facebook' group by msisdn")

      // chargement des colonnes msisdn DISTINCT, nb_prise_charge_conseiller
      // source Jade ==> master_id DISTINCT |nbr_prise_charge_conseiller|
      val df_nb_prise_charge_conseiller = spark.sql("SELECT msisdn as msisdn_nb_chr , SUM( NVL(nb_prise_en_charge,0) ) as nbr_prise_charge_conseiller FROM vision_uc1_datamart.vision_dm_exp_client_reclamation where               date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)             group by msisdn")

      // chargement des colonnes msisdn DISTINCT, nb_prise_charge_conseiller
      // source Jade ==> master_id|nbr_prise_charge_conseiller_autre|nbr_prise_charge_conseiller_demandeInfo|nbr_prise_charge_conseiller_reclamation|nbr_prise_charge_conseiller_operation|
      val df_nb_prise_charge_conseiller_categorie = spark.sql("SELECT msisdn , CASE WHEN categorie = 'autre' then SUM( NVL(nb_prise_en_charge,0) ) else 0 end AS nbr_prise_charge_conseiller_autre, CASE WHEN categorie = 'demandeInfo' then SUM( NVL(nb_prise_en_charge,0) ) else 0 end AS nbr_prise_charge_conseiller_demandeInfo, CASE WHEN categorie = 'reclamation' then SUM( NVL(nb_prise_en_charge,0) ) else 0 end AS nbr_prise_charge_conseiller_reclamation, CASE WHEN categorie = 'operation' then SUM( NVL(nb_prise_en_charge,0) ) else 0 end AS nbr_prise_charge_conseiller_operation FROM vision_uc1_datamart.vision_dm_exp_client_reclamation where                 date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)               group by msisdn,categorie")

      //master_id unique
      val df_socio_pro_geoloc_metric = df_socio_pro_geoloc_msisdn
        .join(df_nb_contact_call_center, df_socio_pro_geoloc_msisdn("msisdn") === df_nb_contact_call_center("msisdn_nb_call"), "LEFT")
        .join(df_nb_contact_reseau_sociaux, df_socio_pro_geoloc_msisdn("msisdn") === df_nb_contact_reseau_sociaux("msisdn_nb_res"), "LEFT")
        .join(df_nb_prise_charge_conseiller, df_socio_pro_geoloc_msisdn("msisdn") === df_nb_prise_charge_conseiller("msisdn_nb_chr"), "LEFT")
        .join(df_nb_prise_charge_conseiller_categorie, df_socio_pro_geoloc_msisdn("msisdn") === df_nb_prise_charge_conseiller_categorie("msisdn"), "LEFT")
        .join(df_dm_offre_univers, df_socio_pro_geoloc_msisdn("msisdn") === df_dm_offre_univers("msisdn"), "LEFT")
        .select(df_socio_pro_geoloc_msisdn("master_id"),
          df_socio_pro_geoloc_msisdn("msisdn"),
          df_dm_offre_univers("univers"),
          df_nb_contact_call_center("nbr_contact_call_center"),
          df_nb_contact_reseau_sociaux("nbr_contact_reseaux_sociaux"),
          df_nb_prise_charge_conseiller("nbr_prise_charge_conseiller"),
          df_nb_prise_charge_conseiller_categorie("nbr_prise_charge_conseiller_autre"),
          df_nb_prise_charge_conseiller_categorie("nbr_prise_charge_conseiller_demandeInfo"),
          df_nb_prise_charge_conseiller_categorie("nbr_prise_charge_conseiller_reclamation"),
          df_nb_prise_charge_conseiller_categorie("nbr_prise_charge_conseiller_operation"))
        .groupBy("master_id","univers")
        .agg(
          sum("nbr_contact_call_center").as("nbr_contact_call_center"),
          sum("nbr_contact_reseaux_sociaux").as("nbr_contact_reseaux_sociaux"),
          sum("nbr_prise_charge_conseiller").as("nbr_prise_charge_conseiller"),
          sum("nbr_prise_charge_conseiller_autre").as("nbr_prise_charge_conseiller_autre"),
          sum("nbr_prise_charge_conseiller_demandeInfo").as("nbr_prise_charge_conseiller_demandeInfo"),
          sum("nbr_prise_charge_conseiller_reclamation").as("nbr_prise_charge_conseiller_reclamation"),
          sum("nbr_prise_charge_conseiller_operation").as("nbr_prise_charge_conseiller_operation"))

      //création de la dataframe finale
      val join1 = df_socio_pro_geoloc_metric
        .join(df_profileconvergence, df_socio_pro_geoloc_metric("master_id") === df_profileconvergence("master_id_profile"), "LEFT")
        .join(df_ca_global_tranche_valeur, df_socio_pro_geoloc_metric("master_id") === df_ca_global_tranche_valeur("master_id_tranche"), "LEFT")
        .join(df_socio_pro_geoloc_segment_master_id, df_socio_pro_geoloc_metric("master_id") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_socio_pro_geoloc_metric("master_id"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_metric("univers"),
          df_profileconvergence("profilConvergence"),
          df_ca_global_tranche_valeur("trancheValeur"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_socio_pro_geoloc_metric("nbr_contact_call_center"),
          df_socio_pro_geoloc_metric("nbr_contact_reseaux_sociaux"),
          df_socio_pro_geoloc_metric("nbr_prise_charge_conseiller"),
          df_socio_pro_geoloc_metric("nbr_prise_charge_conseiller_autre"),
          df_socio_pro_geoloc_metric("nbr_prise_charge_conseiller_demandeInfo"),
          df_socio_pro_geoloc_metric("nbr_prise_charge_conseiller_reclamation"),
          df_socio_pro_geoloc_metric("nbr_prise_charge_conseiller_operation"))


      val join1_result = join1.groupBy("date_jour", "univers", "profilConvergence", "trancheValeur", "region", "commune")
        .agg(
          sum("nbr_contact_call_center").as("nbr_contact_call_center"),
          sum("nbr_contact_reseaux_sociaux").as("nbr_contact_reseaux_sociaux"),
          sum("nbr_prise_charge_conseiller").as("nbr_prise_charge_conseiller"),
          sum("nbr_prise_charge_conseiller_autre").as("nbr_prise_charge_conseiller_autre"),
          sum("nbr_prise_charge_conseiller_demandeInfo").as("nbr_prise_charge_conseiller_demandeInfo"),
          sum("nbr_prise_charge_conseiller_reclamation").as("nbr_prise_charge_conseiller_reclamation"),
          sum("nbr_prise_charge_conseiller_operation").as("nbr_prise_charge_conseiller_operation"))
        .na.drop(Seq("date_jour"))
        .withColumn("_id", concat(
          coalesce(col("univers"), lit("")),
          lit("_"),
          coalesce(col("profilConvergence"), lit("")),
          lit("_"),
          coalesce(col("trancheValeur"), lit("")),
          lit("_"),
          coalesce(col("region"), lit("")),
          lit("_"),
          coalesce(col("commune"), lit("")),
          lit("_"),
          date_format(col("date_jour"), "yyyy-MM")
        ))

      //join1_result.write.format("org.apache.phoenix.spark").mode("overwrite").option("table", "vision_dm_client_stat").option("zkUrl", "10.240.36.86,10.240.36.87,10.240.36.90:2181").save()
      MongoSpark.save(join1_result.write.option("spark.mongodb.output.uri", "mongodb://OciAppVision:3zNgDFE5kuQp9bEs@10.242.30.114/ociVision.vision_dm_client_stat?authSource=admin").mode("append"))
      //MongoSpark.save(df_rcu_socio.write.option("spark.mongodb.output.uri", "mongodb://10.242.36.46/ociVision.vision_dm_df_rcu_socio_test").mode("overwrite"))





      // table 2 ****************************************************************************************************************** vision_dm_top_motif
      //  motif                       | valeur  |    date_jour
      // ==> des motif DISTINCTS order by top 10
      val df_dm_client_reclamation_top_motifs = spark.sql(" SELECT motif, count(*) as valeur, date_format(date_jour,'yyyy-MM') as periode  , date_sub(current_date,1) as date_jour FROM vision_uc1_datamart.vision_dm_exp_client_reclamation WHERE                    date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)                        GROUP BY motif, date_format(date_jour,'yyyy-MM') ORDER BY valeur DESC LIMIT 10 ")
      MongoSpark.save(df_dm_client_reclamation_top_motifs.write.option("spark.mongodb.output.uri", "mongodb://OciAppVision:3zNgDFE5kuQp9bEs@10.242.30.114/ociVision.vision_dm_top_motifs?authSource=admin").mode("overwrite"))






      // table 3 ****************************************************************************************************************** vision_dm_finance_stat

      val df_usage_voix_soratnt_offnet = spark.sql("SELECT msisdn as msisdn_vsof , SUM( NVL(usage_voix_sortant,0)) as usage_voix_soratnt_offnet FROM vision_uc1_datamart.vision_dm_usage WHERE            date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)              AND type='offnet' GROUP BY msisdn ")
      val df_usage_voix_soratnt_onnet = spark.sql("SELECT msisdn as msisdn_vson, SUM( NVL(usage_voix_sortant,0)) as usage_voix_soratnt_onnet FROM vision_uc1_datamart.vision_dm_usage WHERE               date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)              AND type='onnet' GROUP BY msisdn ")
      val df_usage_voix_soratnt_roaming = spark.sql("SELECT msisdn as msisdn_vsor, SUM( NVL(usage_voix_sortant,0)) as usage_voix_soratnt_roaming FROM vision_uc1_datamart.vision_dm_usage WHERE           date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)              AND type='roaming' GROUP BY msisdn ")
      val df_usage_data = spark.sql("SELECT msisdn as msisdn_udat, SUM( NVL(usage_data,0)) as usage_data FROM vision_uc1_datamart.vision_dm_usage WHERE             date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)            GROUP BY msisdn")
      val df_ca_appels_sms = spark.sql("SELECT msisdn as msisdn_apsm, SUM( NVL(montant_appel,0)) as ca_appels, SUM( NVL(montant_sms,0)) as ca_sms  FROM vision_uc1_datamart.vision_dm_consommation WHERE              date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)            GROUP BY msisdn  ")
      //achat_bundle
      val df_ca_achats_bundles = spark.sql(" select msisdn as msisdn_bund, SUM(NVL(montant_achat_pass,0)) AS ca_achats_bundles FROM vision_uc1_datamart.vision_dm_conso_achats_bundles WHERE               date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)            group by msisdn ")
      //montant_rembourse_sos
      val df_ca_remboursement_sos = spark.sql(" SELECT msisdn as msisdn_rems, SUM(NVL(montant_rembourse,0) ) AS ca_remboursement_sos  FROM vision_uc1_datamart.vision_dm_conso_credit_sos WHERE            date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)            GROUP BY msisdn ")
      //montants_achat_sva
      val df_ca_sva = spark.sql(" select msisdn as msisdn_sva, SUM(NVL(montant_sva,0)) AS ca_sva FROM vision_uc1_datamart.vision_dm_conso_achats_sva WHERE             date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)             group by msisdn ")


      //master_id unique
      val df_socio_pro_geoloc_metric2 = df_socio_pro_geoloc_msisdn
        .join(df_usage_voix_soratnt_offnet, df_socio_pro_geoloc_msisdn("msisdn") === df_usage_voix_soratnt_offnet("msisdn_vsof"), "LEFT")
        .join(df_usage_voix_soratnt_onnet, df_socio_pro_geoloc_msisdn("msisdn") === df_usage_voix_soratnt_onnet("msisdn_vson"), "LEFT")
        .join(df_usage_voix_soratnt_roaming, df_socio_pro_geoloc_msisdn("msisdn") === df_usage_voix_soratnt_roaming("msisdn_vsor"), "LEFT")
        .join(df_usage_data, df_socio_pro_geoloc_msisdn("msisdn") === df_usage_data("msisdn_udat"), "LEFT")
        .join(df_ca_appels_sms, df_socio_pro_geoloc_msisdn("msisdn") === df_ca_appels_sms("msisdn_apsm"), "LEFT")
        .join(df_ca_achats_bundles, df_socio_pro_geoloc_msisdn("msisdn") === df_ca_achats_bundles("msisdn_bund"), "LEFT")
        .join(df_ca_remboursement_sos, df_socio_pro_geoloc_msisdn("msisdn") === df_ca_remboursement_sos("msisdn_rems"), "LEFT")
        .join(df_ca_sva, df_socio_pro_geoloc_msisdn("msisdn") === df_ca_sva("msisdn_sva"), "LEFT")
        .join(df_dm_offre_univers, df_socio_pro_geoloc_msisdn("msisdn") === df_dm_offre_univers("msisdn"), "LEFT")
        .select(df_socio_pro_geoloc_msisdn("master_id"),
          df_socio_pro_geoloc_msisdn("nb_msisdn"),
          df_socio_pro_geoloc_msisdn("msisdn"),
          df_dm_offre_univers("univers"),
          df_usage_voix_soratnt_offnet("usage_voix_soratnt_offnet"),
          df_usage_voix_soratnt_onnet("usage_voix_soratnt_onnet"),
          df_usage_voix_soratnt_roaming("usage_voix_soratnt_roaming"),
          df_usage_data("usage_data"),
          df_ca_appels_sms("ca_appels"),
          df_ca_appels_sms("ca_sms"),
          df_ca_achats_bundles("ca_achats_bundles"),
          df_ca_remboursement_sos("ca_remboursement_sos"),
          df_ca_sva("ca_sva"))
        .groupBy("master_id","univers")
        .agg(
          sum("nb_msisdn").as("nb_msisdn"),
          sum("usage_voix_soratnt_offnet").as("usage_voix_soratnt_offnet"),
          sum("usage_voix_soratnt_onnet").as("usage_voix_soratnt_onnet"),
          sum("usage_voix_soratnt_roaming").as("usage_voix_soratnt_roaming"),
          sum("usage_data").as("usage_data"),
          sum("ca_appels").as("ca_appels"),
          sum("ca_sms").as("ca_sms"),
          sum("ca_achats_bundles").as("ca_achats_bundles"),
          sum("ca_remboursement_sos").as("ca_remboursement_sos"),
          sum("ca_sva").as("ca_sva"))


      val df_ca_factures = spark.sql(" select master_id as master_id_fact , univers, SUM(NVL(montant_total_paye,0)) as ca_factures FROM vision_uc1_datamart.vision_dm_facturation WHERE              date_jour between date_format(date_sub(current_date,1),'yyyy-MM-01') and date_sub(current_date,1)                    and univers='FIXE' and master_id is not null    group by master_id, univers ")


      //val df_facturesbroadband ==> mensuelle


      //création de la dataframe finale
      val join2 = df_socio_pro_geoloc_metric2
        .join(df_profileconvergence, df_socio_pro_geoloc_metric2("master_id") === df_profileconvergence("master_id_profile"), "LEFT")
        .join(df_ca_global_tranche_valeur, df_socio_pro_geoloc_metric2("master_id") === df_ca_global_tranche_valeur("master_id_tranche"), "LEFT")
        .join(df_ca_factures, df_socio_pro_geoloc_metric2("master_id") === df_ca_factures("master_id_fact") && df_socio_pro_geoloc_metric2("univers") === df_ca_factures("univers"), "LEFT")
        .join(df_socio_pro_geoloc_segment_master_id, df_socio_pro_geoloc_metric2("master_id") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .join(df_univers_ca_global, df_socio_pro_geoloc_metric2("master_id") === df_univers_ca_global("master_id_ca") && df_socio_pro_geoloc_metric2("univers") === df_univers_ca_global("univers") , "LEFT")
        .select(
          df_socio_pro_geoloc_metric2("master_id"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_metric2("univers"),
          df_profileconvergence("profilConvergence"),
          df_ca_global_tranche_valeur("trancheValeur"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_socio_pro_geoloc_metric2("nb_msisdn"),
          df_univers_ca_global("ca_global"),
          df_socio_pro_geoloc_metric2("usage_voix_soratnt_offnet"),
          df_socio_pro_geoloc_metric2("usage_voix_soratnt_onnet"),
          df_socio_pro_geoloc_metric2("usage_voix_soratnt_roaming"),
          df_socio_pro_geoloc_metric2("usage_data"),
          df_socio_pro_geoloc_metric2("ca_appels"),
          df_socio_pro_geoloc_metric2("ca_sms"),
          df_socio_pro_geoloc_metric2("ca_achats_bundles"),
          df_socio_pro_geoloc_metric2("ca_remboursement_sos"),
          df_socio_pro_geoloc_metric2("ca_sva"),
          df_ca_factures("ca_factures"))

      val join2_result = join2.groupBy("date_jour", "univers", "profilConvergence", "trancheValeur", "region", "commune")
        .agg(
          count("master_id").as("nb_client"),
          sum("nb_msisdn").as("nb_msisdn"),
          sum("ca_global").as("CA_total"),
          sum("usage_voix_soratnt_offnet").as("usage_voix_soratnt_offnet"),
          sum("usage_voix_soratnt_onnet").as("usage_voix_soratnt_onnet"),
          sum("usage_voix_soratnt_roaming").as("usage_voix_soratnt_roaming"),
          sum("usage_data").as("usage_data"),
          sum("ca_appels").as("CA_appels"),
          sum("ca_sms").as("CA_SMS"),
          sum("ca_achats_bundles").as("CA_achats_bundles"),
          sum("ca_remboursement_sos").as("CA_remboursement_SOS"),
          sum("ca_sva").as("CA_SVA"),
          sum("ca_factures").as("CA_factures"))
        .na.drop(Seq("date_jour"))
        .withColumn("_id", concat(
          coalesce(col("univers"), lit("")),
          lit("_"),
          coalesce(col("profilConvergence"), lit("")),
          lit("_"),
          coalesce(col("trancheValeur"), lit("")),
          lit("_"),
          coalesce(col("region"), lit("")),
          lit("_"),
          coalesce(col("commune"), lit("")),
          lit("_"),
          date_format(col("date_jour"), "yyyy-MM")
        ))

      //join1_result.write.format("org.apache.phoenix.spark").mode("overwrite").option("table", "vision_dm_client_stat").option("zkUrl", "10.240.36.86,10.240.36.87,10.240.36.90:2181").save()
      MongoSpark.save(join2_result.write.option("spark.mongodb.output.uri", "mongodb://OciAppVision:3zNgDFE5kuQp9bEs@10.242.30.114/ociVision.vision_dm_finance_stat?authSource=admin").mode("append"))






      // table 4 ****************************************************************************************************************** vision_dm_segmentation

      val df_dm_offre_statut_business_mobile = spark.sql(" SELECT master_id_sb, statut_business FROM (SELECT master_id as master_id_sb, statut_business , ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY statut_business) AS row_num FROM vision_uc1_datamart.vision_dm_offre  where            date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre )             AND univers='MOBILE' and statut_business is not null  ) first_req WHERE row_num=1  ")
      val df_dm_offre_statut_business_adsl = spark.sql(" SELECT master_id_sb, statut_business FROM (SELECT master_id as master_id_sb, statut_business , ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY statut_business) AS row_num FROM vision_uc1_datamart.vision_dm_offre   where             date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre )             AND univers='ADSL'   and statut_business is not null  ) first_req WHERE row_num=1  ")
      val df_dm_offre_statut_business_ftth = spark.sql(" SELECT master_id_sb, statut_business FROM (SELECT master_id as master_id_sb, statut_business , ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY statut_business) AS row_num FROM vision_uc1_datamart.vision_dm_offre   where             date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre )             AND univers='FTTH'   and statut_business is not null  ) first_req WHERE row_num=1  ")
      val df_dm_offre_statut_business_tdd = spark.sql(" SELECT master_id_sb, statut_business FROM (SELECT master_id as master_id_sb, statut_business , ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY statut_business) AS row_num FROM vision_uc1_datamart.vision_dm_offre    where             date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre )             AND univers='TDD'    and statut_business is not null  ) first_req WHERE row_num=1  ")
      val df_dm_offre_statut_business_fdd = spark.sql(" SELECT master_id_sb, statut_business FROM (SELECT master_id as master_id_sb, statut_business , ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY statut_business) AS row_num FROM vision_uc1_datamart.vision_dm_offre    where             date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre )             AND univers='FDD'    and statut_business is not null  ) first_req WHERE row_num=1  ")

      val df_ca_mobile_statut_business = df_ca_mobile.join(df_dm_offre_statut_business_mobile, df_ca_mobile("master_id_ca") === df_dm_offre_statut_business_mobile("master_id_sb"), "LEFT")
        .select(df_ca_mobile("master_id_ca"), df_ca_mobile("univers"), df_ca_mobile("ca_global"), df_dm_offre_statut_business_mobile("statut_business"))

      val df_ca_adsl_statut_business = df_ca_adsl.join(df_dm_offre_statut_business_adsl, df_ca_adsl("master_id_ca") === df_dm_offre_statut_business_adsl("master_id_sb"), "LEFT")
        .select(df_ca_adsl("master_id_ca"), df_ca_adsl("univers"), df_ca_adsl("ca_global"), df_dm_offre_statut_business_adsl("statut_business"))

      val df_ca_ftth_statut_business = df_ca_ftth.join(df_dm_offre_statut_business_ftth, df_ca_ftth("master_id_ca") === df_dm_offre_statut_business_ftth("master_id_sb"), "LEFT")
        .select(df_ca_ftth("master_id_ca"), df_ca_ftth("univers"), df_ca_ftth("ca_global"), df_dm_offre_statut_business_ftth("statut_business"))

      val df_ca_tdd_statut_business = df_ca_tdd.join(df_dm_offre_statut_business_tdd, df_ca_tdd("master_id_ca") === df_dm_offre_statut_business_tdd("master_id_sb"), "LEFT")
        .select(df_ca_tdd("master_id_ca"), df_ca_tdd("univers"), df_ca_tdd("ca_global"), df_dm_offre_statut_business_tdd("statut_business"))

      val df_ca_fdd_statut_business = df_ca_fdd.join(df_dm_offre_statut_business_fdd, df_ca_fdd("master_id_ca") === df_dm_offre_statut_business_fdd("master_id_sb"), "LEFT")
        .select(df_ca_fdd("master_id_ca"), df_ca_fdd("univers"), df_ca_fdd("ca_global"), df_dm_offre_statut_business_fdd("statut_business"))

      // colonne master_id et ca_global pour mobile
      val df_ca_mobile_segment = df_ca_mobile_statut_business
        .withColumn("segment", when(col("statut_business") =!= "Actif", "Inactif")
          .when(col("ca_global") <= 1000, "Very Low")
          .when(col("ca_global") > 1000 && col("ca_global") <= 2500, "Low")
          .when(col("ca_global") > 2500 && col("ca_global") <= 10000, "Middle")
          .when(col("ca_global") > 10000 && col("ca_global") <= 50000, "High")
          .otherwise("Very High"))


      // colonne master_id et ca_global pour adsl
      val df_ca_adsl_segment = df_ca_adsl_statut_business
        .withColumn("segment", when(col("statut_business") =!= "Actif", "Inactif")
          .when(col("ca_global") <= 5000, "Very Low")
          .when(col("ca_global") > 5000 && col("ca_global") <= 15000, "Low")
          .when(col("ca_global") > 15000 && col("ca_global") <= 50000, "Middle")
          .when(col("ca_global") > 50000 && col("ca_global") <= 100000, "High")
          .otherwise("Very High"))

      // colonne master_id et ca_global pour ftth
      val df_ca_ftth_segment = df_ca_ftth_statut_business
        .withColumn("segment", when(col("statut_business") =!= "Actif", "Inactif")
          .when(col("ca_global") <= 5000, "Very Low")
          .when(col("ca_global") > 5000 && col("ca_global") <= 15000, "Low")
          .when(col("ca_global") > 15000 && col("ca_global") <= 50000, "Middle")
          .when(col("ca_global") > 50000 && col("ca_global") <= 100000, "High")
          .otherwise("Very High"))

      // colonne master_id et ca_global pour tdd
      val df_ca_tdd_segment = df_ca_tdd_statut_business
        .withColumn("segment", when(col("statut_business") =!= "Actif", "Inactif")
          .when(col("ca_global") <= 5000, "Very Low")
          .when(col("ca_global") > 5000 && col("ca_global") <= 15000, "Low")
          .when(col("ca_global") > 15000 && col("ca_global") <= 50000, "Middle")
          .when(col("ca_global") > 50000 && col("ca_global") <= 100000, "High")
          .otherwise("Very High"))

      // colonne master_id et ca_global pour fdd
      val df_ca_fdd_segment = df_ca_fdd_statut_business
        .withColumn("segment", when(col("statut_business") =!= "Actif", "Inactif")
          .when(col("ca_global") <= 5000, "Very Low")
          .when(col("ca_global") > 5000 && col("ca_global") <= 15000, "Low")
          .when(col("ca_global") > 15000 && col("ca_global") <= 50000, "Middle")
          .when(col("ca_global") > 50000 && col("ca_global") <= 100000, "High")
          .otherwise("Very High"))

      // colonne master_id et ca_global pour om
      val df_ca_om_segment = df_ca_om
        .withColumn("segment", when(col("ca_global") <= 500, "Very Low")
          .when(col("ca_global") > 500 && col("ca_global") <= 2000, "Low")
          .when(col("ca_global") > 2000 && col("ca_global") <= 5000, "Middle")
          .when(col("ca_global") > 5000 && col("ca_global") <= 20000, "High")
          .otherwise("Very High"))



      // ******************** Mobile*TDD, Mobile*FDD, Mobile*FTTH, Mobile*OM, Mobile*ADSL OM*TDD, OM*FDD, OM*FTTH, OM*ADSL


      // ******************** couple MOBILE*TDD
      val df_mobile_tdd_segment = df_ca_mobile_segment.union(df_ca_tdd_segment)
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_mobile_tdd_segment_all = df_mobile_tdd_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_mobile_tdd_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_mobile_tdd_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_mobile_tdd_segment("univers1"),
          df_mobile_tdd_segment("univers2"),
          df_mobile_tdd_segment("segment1"),
          df_mobile_tdd_segment("segment2"),
          df_mobile_tdd_segment("ca_global").as("caClientCroise"),
          df_mobile_tdd_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))

      // ******************** couple MOBILE*FDD
      val df_mobile_fdd_segment = df_ca_mobile_segment.union(df_ca_fdd_segment)
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_mobile_fdd_segment_all = df_mobile_fdd_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_mobile_fdd_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_mobile_fdd_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_mobile_fdd_segment("univers1"),
          df_mobile_fdd_segment("univers2"),
          df_mobile_fdd_segment("segment1"),
          df_mobile_fdd_segment("segment2"),
          df_mobile_fdd_segment("ca_global").as("caClientCroise"),
          df_mobile_fdd_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))

      // ******************** couple MOBILE*FTTH
      val df_mobile_ftth_segment = df_ca_mobile_segment.union(df_ca_ftth_segment)
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_mobile_ftth_segment_all = df_mobile_ftth_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_mobile_ftth_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_mobile_ftth_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_mobile_ftth_segment("univers1"),
          df_mobile_ftth_segment("univers2"),
          df_mobile_ftth_segment("segment1"),
          df_mobile_ftth_segment("segment2"),
          df_mobile_ftth_segment("ca_global").as("caClientCroise"),
          df_mobile_ftth_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))

      // ******************** couple MOBILE*OM
      val df_mobile_om_segment = df_ca_mobile_segment.select("master_id_ca", "univers", "ca_global", "segment").union(df_ca_om_segment)
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_mobile_om_segment_all = df_mobile_om_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_mobile_om_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_mobile_om_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_mobile_om_segment("univers1"),
          df_mobile_om_segment("univers2"),
          df_mobile_om_segment("segment1"),
          df_mobile_om_segment("segment2"),
          df_mobile_om_segment("ca_global").as("caClientCroise"),
          df_mobile_om_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))

      // ******************** couple MOBILE*ADSL
      val df_mobile_adsl_segment = df_ca_mobile_segment.union(df_ca_adsl_segment)
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_mobile_adsl_segment_all = df_mobile_adsl_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_mobile_adsl_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_mobile_adsl_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_mobile_adsl_segment("univers1"),
          df_mobile_adsl_segment("univers2"),
          df_mobile_adsl_segment("segment1"),
          df_mobile_adsl_segment("segment2"),
          df_mobile_adsl_segment("ca_global").as("caClientCroise"),
          df_mobile_adsl_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))


      // ******************** couple OM*TDD
      val df_om_tdd_segment = df_ca_om_segment.union(df_ca_tdd_segment.select("master_id_ca", "univers", "ca_global", "segment"))
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "OM", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "OM", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "OM", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "OM", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_om_tdd_segment_all = df_om_tdd_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_om_tdd_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_om_tdd_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_om_tdd_segment("univers1"),
          df_om_tdd_segment("univers2"),
          df_om_tdd_segment("segment1"),
          df_om_tdd_segment("segment2"),
          df_om_tdd_segment("ca_global").as("caClientCroise"),
          df_om_tdd_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))

      // ******************** couple OM*FDD
      val df_om_fdd_segment = df_ca_om_segment.union(df_ca_fdd_segment.select("master_id_ca", "univers", "ca_global", "segment"))
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "OM", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "OM", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "OM", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "OM", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_om_fdd_segment_all = df_om_fdd_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_om_fdd_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_om_fdd_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_om_fdd_segment("univers1"),
          df_om_fdd_segment("univers2"),
          df_om_fdd_segment("segment1"),
          df_om_fdd_segment("segment2"),
          df_om_fdd_segment("ca_global").as("caClientCroise"),
          df_om_fdd_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))

      // ******************** couple OM*FTTH
      val df_om_ftth_segment = df_ca_om_segment.union(df_ca_ftth_segment.select("master_id_ca", "univers", "ca_global", "segment"))
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "OM", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "OM", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "OM", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "OM", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_om_ftth_segment_all = df_om_ftth_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_om_ftth_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_om_ftth_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_om_ftth_segment("univers1"),
          df_om_ftth_segment("univers2"),
          df_om_ftth_segment("segment1"),
          df_om_ftth_segment("segment2"),
          df_om_ftth_segment("ca_global").as("caClientCroise"),
          df_om_ftth_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))

      // ******************** couple OM*ADSL
      val df_om_adsl_segment = df_ca_om_segment.union(df_ca_adsl_segment.select("master_id_ca", "univers", "ca_global", "segment"))
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "OM", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "OM", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "OM", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "OM", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_om_adsl_segment_all = df_om_adsl_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_om_adsl_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_om_adsl_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_om_adsl_segment("univers1"),
          df_om_adsl_segment("univers2"),
          df_om_adsl_segment("segment1"),
          df_om_adsl_segment("segment2"),
          df_om_adsl_segment("ca_global").as("caClientCroise"),
          df_om_adsl_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))


      val df_segment_all = df_mobile_tdd_segment_all.union(df_mobile_fdd_segment_all).union(df_mobile_ftth_segment_all).union(df_mobile_om_segment_all).union(df_mobile_adsl_segment_all)
        .union(df_om_tdd_segment_all).union(df_om_fdd_segment_all).union(df_om_ftth_segment_all).union(df_om_adsl_segment_all)
        .withColumn("_id", concat(
          coalesce(col("region"), lit("")),
          lit("_"),
          coalesce(col("commune"), lit("")),
          lit("_"),
          coalesce(col("univers1"), lit("")),
          lit("_"),
          coalesce(col("univers2"), lit("")),
          lit("_"),
          coalesce(col("segment1"), lit("")),
          lit("_"),
          coalesce(col("segment2"), lit("")),
          lit("_"),
          date_format(col("date_jour"), "yyyy-MM")
        ))

      MongoSpark.save(df_segment_all.write.option("spark.mongodb.output.uri", "mongodb://OciAppVision:3zNgDFE5kuQp9bEs@10.242.30.114/ociVision.vision_dm_segmentation?authSource=admin").mode("append"))






      // table 5 ****************************************************************************************************************** vision_dm_indicator

      val df_dm_indicator = df_ca_mobile.select("master_id_ca", "ca_global")
        .union(df_ca_adsl.select("master_id_ca", "ca_global"))
        .union(df_ca_ftth.select("master_id_ca", "ca_global"))
        .union(df_ca_tdd.select("master_id_ca", "ca_global"))
        .union(df_ca_fdd.select("master_id_ca", "ca_global"))
        .groupBy("master_id_ca")
        .agg(sum("ca_global").as("ca_global"))
        .agg(count("*").alias("nbr_client_total"),
          sum("ca_global").as("ca_total"))
        .withColumn("date_jour", date_sub(current_date(), 1))
        .withColumn("_id", date_format(date_sub(current_date(), 1), "yyyy-MM"))

      MongoSpark.save(df_dm_indicator.write.option("spark.mongodb.output.uri", "mongodb://OciAppVision:3zNgDFE5kuQp9bEs@10.242.30.114/ociVision.vision_dm_indicator?authSource=admin").mode("append"))





    }












    else if (args(0) == "last_month") {




      // ****************************************************************************************************************** Partie commun

      // chargement des colonnes master_id, msisdn DISTINCT, region, commune_idbase de la datamart socio pro & geoloc
      val df_socio_pro_geoloc_msisdn = spark.sql("SELECT master_id ,msisdn, count(*) as nb_msisdn  FROM (select master_id ,msisdn, ROW_NUMBER() OVER (PARTITION BY msisdn ORDER BY date_creation_mid_rcu DESC) AS row_num FROM vision_uc1_datamart.vision_dm_socio_pro_geolocalisation where                   date_jour =  ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_socio_pro_geolocalisation WHERE date_jour BETWEEN date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))     )   AND master_id is not null                ) first_req WHERE row_num=1 GROUP BY master_id ,msisdn ")

      // df_socio_pro_geoloc_segment par master_id unique
      val df_socio_pro_geoloc_segment_master_id = spark.sql("SELECT master_id, region ,commune, date_jour, count(*) as nbTotalClient  FROM (select master_id, region , commune_idbase as commune, ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY date_creation_mid_rcu DESC) AS row_num , last_day(add_months(current_date, -1)) as date_jour FROM vision_uc1_datamart.vision_dm_socio_pro_geolocalisation where                 date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_socio_pro_geolocalisation WHERE date_jour BETWEEN date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))      )   AND master_id is not null                ) first_req WHERE row_num=1 GROUP BY master_id, region ,commune, date_jour ")

            // nouveau_df = datamart offre (MSISDN,UNIVERS par dérnier date_aquisition) UNIQUES
      val df_dm_offre_univers = spark.sql("SELECT msisdn, univers FROM (SELECT msisdn, univers , ROW_NUMBER() OVER (PARTITION BY msisdn ORDER BY date_aquisation DESC) AS row_num FROM vision_uc1_datamart.vision_dm_offre where                  date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre WHERE date_jour BETWEEN date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))      ) AND statut_technique IN ('Actif','En attente','Suspendu')       ) first_req WHERE row_num=1 ")


      // lecture des données des datamarts de chiffre d'affaire
      val df_dm_ca_mobile_prepaid = spark.sql(" select master_id as master_id_ca,'MOBILE' as univers, SUM(NVL(montant_appel,0)) + SUM(NVL(montant_sms,0)) + SUM(NVL(montant_achat_pass,0)) + SUM(NVL(montant_sva,0)) + SUM(NVL(montant_rembourse,0)) as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_prepaid where           date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))             and master_id is not null          group by master_id ")
      val df_dm_ca_mobile_hybrid = spark.sql(" select master_id as master_id_ca,'MOBILE' as univers,  SUM(NVL(montant_appel,0)) + SUM(NVL(montant_sms,0)) + SUM(NVL(montant_achat_pass,0)) + SUM(NVL(montant_sva,0)) + SUM(NVL(montant_rembourse,0)) as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_hybrid where            date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))             and master_id is not null          group by master_id")
      val df_dm_ca_mobile_postpaid = spark.sql("select master_id as master_id_ca ,'MOBILE' as univers,  SUM(NVL(montant_achat_pass,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_mobile_postpaid where          date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1)) and master_id is not null         group by master_id")
      val df_dm_ca_adsl = spark.sql(" select master_id as master_id_ca,'ADSL' as univers, SUM(NVL(ca_rechargement_ods,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_adsl where         date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1)) and master_id is not null        group by master_id ")
      val df_dm_ca_ftth = spark.sql(" select master_id as master_id_ca ,'FTTH' as univers,   SUM(NVL(ca_base_contrat,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_ftth where          date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1)) and master_id is not null        group by master_id ")
      val df_dm_ca_tdd = spark.sql(" select master_id as master_id_ca ,'TDD' as univers,   SUM(NVL(ca_e_recharge,0)) + SUM(NVL(rechargemet_om,0)) + SUM(NVL(rechargemet_om,0)) + SUM(NVL(ca_base_contrat,0)) + SUM(NVL(ca_rechargement_ods,0)) as ca_global FROM vision_uc1_datamart.vision_dm_ca_tdd where         date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1)) and master_id is not null           group by master_id   ")
      val df_dm_ca_fdd = spark.sql(" select master_id as master_id_ca ,'FDD' as univers,   SUM(NVL(ca_e_recharge,0)) + SUM(NVL(rechargemet_om,0)) + SUM(NVL(ca_rechargement_ods,0)) as ca_global FROM vision_uc1_datamart.vision_dm_ca_fdd where         date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1)) and master_id is not null          group by master_id  ")
      // le premier jour du mois courant fait référence aux données de mois précédent
      val df_dm_ca_factures_mobile = spark.sql(" select master_id as master_id_ca ,'MOBILE' as univers, SUM(NVL(amount_ttc,0)) AS ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_mobile where           date_jour=date_format(current_date,'yyyy-MM-01')               and master_id is not null            group by master_id ")
      val df_dm_ca_factures_adsl = spark.sql(" select master_id as master_id_ca ,'ADSL' as univers, SUM(NVL(amount_ttc,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_adsl where                 date_jour=date_format(current_date,'yyyy-MM-01')               and master_id is not null            group by master_id ")
      val df_dm_ca_factures_ftth = spark.sql(" select master_id as master_id_ca ,'FTTH' as univers, SUM(NVL(amount_ttc,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_ftth where                 date_jour=date_format(current_date,'yyyy-MM-01')               and master_id is not null            group by master_id ")
      val df_dm_ca_factures_tdd = spark.sql(" select master_id as master_id_ca ,'TDD' as univers, SUM(NVL(amount_ttc,0))   as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_tdd where                   date_jour=date_format(current_date,'yyyy-MM-01')               and master_id is not null            group by master_id ")
      val df_dm_ca_factures_fdd = spark.sql(" select master_id as master_id_ca ,'FDD' as univers, SUM(NVL(amount_ttc,0))  as ca_global FROM vision_uc1_datamart.vision_dm_ca_factures_fdd where                    date_jour=date_format(current_date,'yyyy-MM-01')               and master_id is not null            group by master_id ")

      // univers et master_id distinct
      val df_ca_fixe_postpaid = spark.sql(" select master_id as master_id_ca ,'FIXE' as univers, SUM(NVL(mnt_solde_fact_fixe,0)) as ca_global FROM vision_uc1_datamart.vision_dm_ca_fixe_postpaid where                 date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))  and master_id is not null                group by master_id ")
      val df_ca_mobile = df_dm_ca_mobile_prepaid.union(df_dm_ca_mobile_hybrid).union(df_dm_ca_mobile_postpaid).union(df_dm_ca_factures_mobile).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_adsl = df_dm_ca_adsl.union(df_dm_ca_factures_adsl).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_ftth = df_dm_ca_ftth.union(df_dm_ca_factures_ftth).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_tdd = df_dm_ca_tdd.union(df_dm_ca_factures_tdd).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_fdd = df_dm_ca_fdd.union(df_dm_ca_factures_fdd).groupBy("master_id_ca", "univers").agg(sum("ca_global").as("ca_global"))
      val df_ca_om = spark.sql("select master_id as master_id_ca , 'OM' as univers, SUM(NVL(chiffre_affaire,0)) as ca_global FROM vision_uc1_datamart.vision_dm_orange_money where                    date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))  and master_id is not null                group by master_id")


      val df_univers_ca_global = df_ca_fixe_postpaid.union(df_ca_mobile).union(df_ca_adsl).union(df_ca_ftth).union(df_ca_tdd).union(df_ca_fdd).union(df_ca_om)
        .groupBy("master_id_ca", "univers")
        .agg(sum("ca_global").as("ca_global"))
        .select("master_id_ca", "univers", "ca_global")

      //profilconvergence ==> Un client mono_univers, multi_univers par master_id ==> count distinct univers group by master_id
      // ==> des master_id distincts ==> master_id|cnt|profilconvergence
      val df_profileconvergence = df_univers_ca_global
        .groupBy("master_id_ca").agg(count("*").alias("cnt"))
        .withColumn("profilConvergence", when(col("cnt") === 1, "mono_univers").otherwise("multi_univers"))
        .withColumnRenamed("master_id_ca", "master_id_profile")

      //quartile ==> ajouter 4 tranches dans la table de la datamart CA
      // somme des CA par master_id pour calculer le quartile: ==> master_id distinct
      val df_ca_global = df_univers_ca_global
        .groupBy("master_id_ca")
        .agg(sum("ca_global").as("ca_global"))
        .select("master_id_ca", "ca_global")

      val Q1 = df_ca_global.filter(df_ca_global("ca_global") > 0).stat.approxQuantile("ca_global", Array(0.25), 0)(0)
      val Q2 = df_ca_global.filter(df_ca_global("ca_global") > 0).stat.approxQuantile("ca_global", Array(0.5), 0)(0)
      val Q3 = df_ca_global.filter(df_ca_global("ca_global") > 0).stat.approxQuantile("ca_global", Array(0.75), 0)(0)

      val df_ca_global_tranche_valeur = df_ca_global.withColumn("trancheValeur",
        when(col("ca_global") === 0, "Very Low")
          .when(col("ca_global") <= Q1, "Low")
          .when(col("ca_global") > Q1 && col("ca_global") <= Q2, "Middle")
          .when(col("ca_global") > Q2 && col("ca_global") <= Q3, "High")
          .otherwise("Very high"))
        .withColumnRenamed("master_id_ca", "master_id_tranche")

      // table 1 ****************************************************************************************************************** vision_dm_client_stat

      // chargement des colonnes msisdn DISTINCT, nbr_contact_call_center
      // source Genesys ==> |master_id DISTINCT |nbr_contact_call_center|
      val df_nb_contact_call_center = spark.sql("select msisdn as msisdn_nb_call ,SUM( NVL(nb_appels_aboutis,0) ) as nbr_contact_call_center FROM vision_uc1_datamart.vision_dm_exp_client_call_center where                date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))            group by msisdn ")

      // chargement des colonnes msisdn DISTINCT, nb_contact_reseau_sociaux
      // source Jade ==> master_id DISTINCT |nbr_contact_reseaux_sociaux|
      val df_nb_contact_reseau_sociaux = spark.sql("SELECT msisdn as msisdn_nb_res , SUM( NVL(nb_prise_contact,0) ) as nbr_contact_reseaux_sociaux FROM vision_uc1_datamart.vision_dm_exp_client_reclamation where                 date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))              AND canal='Facebook' group by msisdn")

      // chargement des colonnes msisdn DISTINCT, nb_prise_charge_conseiller
      // source Jade ==> master_id DISTINCT |nbr_prise_charge_conseiller|
      val df_nb_prise_charge_conseiller = spark.sql("SELECT msisdn as msisdn_nb_chr , SUM( NVL(nb_prise_en_charge,0) ) as nbr_prise_charge_conseiller FROM vision_uc1_datamart.vision_dm_exp_client_reclamation where               date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))             group by msisdn")

      // chargement des colonnes msisdn DISTINCT, nb_prise_charge_conseiller
      // source Jade ==> master_id|nbr_prise_charge_conseiller_autre|nbr_prise_charge_conseiller_demandeInfo|nbr_prise_charge_conseiller_reclamation|nbr_prise_charge_conseiller_operation|
      val df_nb_prise_charge_conseiller_categorie = spark.sql("SELECT msisdn , CASE WHEN categorie = 'autre' then SUM( NVL(nb_prise_en_charge,0) ) else 0 end AS nbr_prise_charge_conseiller_autre, CASE WHEN categorie = 'demandeInfo' then SUM( NVL(nb_prise_en_charge,0) ) else 0 end AS nbr_prise_charge_conseiller_demandeInfo, CASE WHEN categorie = 'reclamation' then SUM( NVL(nb_prise_en_charge,0) ) else 0 end AS nbr_prise_charge_conseiller_reclamation, CASE WHEN categorie = 'operation' then SUM( NVL(nb_prise_en_charge,0) ) else 0 end AS nbr_prise_charge_conseiller_operation FROM vision_uc1_datamart.vision_dm_exp_client_reclamation where                 date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))               group by msisdn,categorie")

      //master_id unique
      val df_socio_pro_geoloc_metric = df_socio_pro_geoloc_msisdn
        .join(df_nb_contact_call_center, df_socio_pro_geoloc_msisdn("msisdn") === df_nb_contact_call_center("msisdn_nb_call"), "LEFT")
        .join(df_nb_contact_reseau_sociaux, df_socio_pro_geoloc_msisdn("msisdn") === df_nb_contact_reseau_sociaux("msisdn_nb_res"), "LEFT")
        .join(df_nb_prise_charge_conseiller, df_socio_pro_geoloc_msisdn("msisdn") === df_nb_prise_charge_conseiller("msisdn_nb_chr"), "LEFT")
        .join(df_nb_prise_charge_conseiller_categorie, df_socio_pro_geoloc_msisdn("msisdn") === df_nb_prise_charge_conseiller_categorie("msisdn"), "LEFT")
        .join(df_dm_offre_univers, df_socio_pro_geoloc_msisdn("msisdn") === df_dm_offre_univers("msisdn"), "LEFT")
        .select(df_socio_pro_geoloc_msisdn("master_id"),
          df_socio_pro_geoloc_msisdn("msisdn"),
          df_dm_offre_univers("univers"),
          df_nb_contact_call_center("nbr_contact_call_center"),
          df_nb_contact_reseau_sociaux("nbr_contact_reseaux_sociaux"),
          df_nb_prise_charge_conseiller("nbr_prise_charge_conseiller"),
          df_nb_prise_charge_conseiller_categorie("nbr_prise_charge_conseiller_autre"),
          df_nb_prise_charge_conseiller_categorie("nbr_prise_charge_conseiller_demandeInfo"),
          df_nb_prise_charge_conseiller_categorie("nbr_prise_charge_conseiller_reclamation"),
          df_nb_prise_charge_conseiller_categorie("nbr_prise_charge_conseiller_operation"))
        .groupBy("master_id","univers")
        .agg(
          sum("nbr_contact_call_center").as("nbr_contact_call_center"),
          sum("nbr_contact_reseaux_sociaux").as("nbr_contact_reseaux_sociaux"),
          sum("nbr_prise_charge_conseiller").as("nbr_prise_charge_conseiller"),
          sum("nbr_prise_charge_conseiller_autre").as("nbr_prise_charge_conseiller_autre"),
          sum("nbr_prise_charge_conseiller_demandeInfo").as("nbr_prise_charge_conseiller_demandeInfo"),
          sum("nbr_prise_charge_conseiller_reclamation").as("nbr_prise_charge_conseiller_reclamation"),
          sum("nbr_prise_charge_conseiller_operation").as("nbr_prise_charge_conseiller_operation"))

      //création de la dataframe finale
      val join1 = df_socio_pro_geoloc_metric
        .join(df_profileconvergence, df_socio_pro_geoloc_metric("master_id") === df_profileconvergence("master_id_profile"), "LEFT")
        .join(df_ca_global_tranche_valeur, df_socio_pro_geoloc_metric("master_id") === df_ca_global_tranche_valeur("master_id_tranche"), "LEFT")
        .join(df_socio_pro_geoloc_segment_master_id, df_socio_pro_geoloc_metric("master_id") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_socio_pro_geoloc_metric("master_id"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_metric("univers"),
          df_profileconvergence("profilConvergence"),
          df_ca_global_tranche_valeur("trancheValeur"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_socio_pro_geoloc_metric("nbr_contact_call_center"),
          df_socio_pro_geoloc_metric("nbr_contact_reseaux_sociaux"),
          df_socio_pro_geoloc_metric("nbr_prise_charge_conseiller"),
          df_socio_pro_geoloc_metric("nbr_prise_charge_conseiller_autre"),
          df_socio_pro_geoloc_metric("nbr_prise_charge_conseiller_demandeInfo"),
          df_socio_pro_geoloc_metric("nbr_prise_charge_conseiller_reclamation"),
          df_socio_pro_geoloc_metric("nbr_prise_charge_conseiller_operation"))


      val join1_result = join1.groupBy("date_jour", "univers", "profilConvergence", "trancheValeur", "region", "commune")
        .agg(
          sum("nbr_contact_call_center").as("nbr_contact_call_center"),
          sum("nbr_contact_reseaux_sociaux").as("nbr_contact_reseaux_sociaux"),
          sum("nbr_prise_charge_conseiller").as("nbr_prise_charge_conseiller"),
          sum("nbr_prise_charge_conseiller_autre").as("nbr_prise_charge_conseiller_autre"),
          sum("nbr_prise_charge_conseiller_demandeInfo").as("nbr_prise_charge_conseiller_demandeInfo"),
          sum("nbr_prise_charge_conseiller_reclamation").as("nbr_prise_charge_conseiller_reclamation"),
          sum("nbr_prise_charge_conseiller_operation").as("nbr_prise_charge_conseiller_operation"))
        .na.drop(Seq("date_jour"))
        .withColumn("_id", concat(
          coalesce(col("univers"), lit("")),
          lit("_"),
          coalesce(col("profilConvergence"), lit("")),
          lit("_"),
          coalesce(col("trancheValeur"), lit("")),
          lit("_"),
          coalesce(col("region"), lit("")),
          lit("_"),
          coalesce(col("commune"), lit("")),
          lit("_"),
          date_format(col("date_jour"), "yyyy-MM")
        ))

      //join1_result.write.format("org.apache.phoenix.spark").mode("overwrite").option("table", "vision_dm_client_stat").option("zkUrl", "10.240.36.86,10.240.36.87,10.240.36.90:2181").save()
      MongoSpark.save(join1_result.write.option("spark.mongodb.output.uri", "mongodb://OciAppVision:3zNgDFE5kuQp9bEs@10.242.30.114/ociVision.vision_dm_client_stat?authSource=admin").mode("append"))
      //MongoSpark.save(df_rcu_socio.write.option("spark.mongodb.output.uri", "mongodb://10.242.36.46/ociVision.vision_dm_df_rcu_socio_test").mode("overwrite"))





      // table 3 ****************************************************************************************************************** vision_dm_finance_stat

      val df_usage_voix_soratnt_offnet = spark.sql("SELECT msisdn as msisdn_vsof , SUM( NVL(usage_voix_sortant,0)) as usage_voix_soratnt_offnet FROM vision_uc1_datamart.vision_dm_usage WHERE            date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))              AND type='offnet' GROUP BY msisdn ")
      val df_usage_voix_soratnt_onnet = spark.sql("SELECT msisdn as msisdn_vson, SUM( NVL(usage_voix_sortant,0)) as usage_voix_soratnt_onnet FROM vision_uc1_datamart.vision_dm_usage WHERE               date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))              AND type='onnet' GROUP BY msisdn ")
      val df_usage_voix_soratnt_roaming = spark.sql("SELECT msisdn as msisdn_vsor, SUM( NVL(usage_voix_sortant,0)) as usage_voix_soratnt_roaming FROM vision_uc1_datamart.vision_dm_usage WHERE           date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))              AND type='roaming' GROUP BY msisdn ")
      val df_usage_data = spark.sql("SELECT msisdn as msisdn_udat, SUM( NVL(usage_data,0)) as usage_data FROM vision_uc1_datamart.vision_dm_usage WHERE             date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))            GROUP BY msisdn")
      val df_ca_appels_sms = spark.sql("SELECT msisdn as msisdn_apsm, SUM( NVL(montant_appel,0)) as ca_appels, SUM( NVL(montant_sms,0)) as ca_sms  FROM vision_uc1_datamart.vision_dm_consommation WHERE              date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))            GROUP BY msisdn  ")
      //achat_bundle
      val df_ca_achats_bundles = spark.sql(" select msisdn as msisdn_bund, SUM(NVL(montant_achat_pass,0)) AS ca_achats_bundles FROM vision_uc1_datamart.vision_dm_conso_achats_bundles WHERE               date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))            group by msisdn ")
      //montant_rembourse_sos
      val df_ca_remboursement_sos = spark.sql(" SELECT msisdn as msisdn_rems, SUM(NVL(montant_rembourse,0) ) AS ca_remboursement_sos  FROM vision_uc1_datamart.vision_dm_conso_credit_sos WHERE            date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))            GROUP BY msisdn ")
      //montants_achat_sva
      val df_ca_sva = spark.sql(" select msisdn as msisdn_sva, SUM(NVL(montant_sva,0)) AS ca_sva FROM vision_uc1_datamart.vision_dm_conso_achats_sva WHERE             date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))             group by msisdn ")


      //master_id unique
      val df_socio_pro_geoloc_metric2 = df_socio_pro_geoloc_msisdn
        .join(df_usage_voix_soratnt_offnet, df_socio_pro_geoloc_msisdn("msisdn") === df_usage_voix_soratnt_offnet("msisdn_vsof"), "LEFT")
        .join(df_usage_voix_soratnt_onnet, df_socio_pro_geoloc_msisdn("msisdn") === df_usage_voix_soratnt_onnet("msisdn_vson"), "LEFT")
        .join(df_usage_voix_soratnt_roaming, df_socio_pro_geoloc_msisdn("msisdn") === df_usage_voix_soratnt_roaming("msisdn_vsor"), "LEFT")
        .join(df_usage_data, df_socio_pro_geoloc_msisdn("msisdn") === df_usage_data("msisdn_udat"), "LEFT")
        .join(df_ca_appels_sms, df_socio_pro_geoloc_msisdn("msisdn") === df_ca_appels_sms("msisdn_apsm"), "LEFT")
        .join(df_ca_achats_bundles, df_socio_pro_geoloc_msisdn("msisdn") === df_ca_achats_bundles("msisdn_bund"), "LEFT")
        .join(df_ca_remboursement_sos, df_socio_pro_geoloc_msisdn("msisdn") === df_ca_remboursement_sos("msisdn_rems"), "LEFT")
        .join(df_ca_sva, df_socio_pro_geoloc_msisdn("msisdn") === df_ca_sva("msisdn_sva"), "LEFT")
        .join(df_dm_offre_univers, df_socio_pro_geoloc_msisdn("msisdn") === df_dm_offre_univers("msisdn"), "LEFT")
        .select(df_socio_pro_geoloc_msisdn("master_id"),
          df_socio_pro_geoloc_msisdn("nb_msisdn"),
          df_socio_pro_geoloc_msisdn("msisdn"),
          df_dm_offre_univers("univers"),
          df_usage_voix_soratnt_offnet("usage_voix_soratnt_offnet"),
          df_usage_voix_soratnt_onnet("usage_voix_soratnt_onnet"),
          df_usage_voix_soratnt_roaming("usage_voix_soratnt_roaming"),
          df_usage_data("usage_data"),
          df_ca_appels_sms("ca_appels"),
          df_ca_appels_sms("ca_sms"),
          df_ca_achats_bundles("ca_achats_bundles"),
          df_ca_remboursement_sos("ca_remboursement_sos"),
          df_ca_sva("ca_sva"))
        .groupBy("master_id","univers")
        .agg(
          sum("nb_msisdn").as("nb_msisdn"),
          sum("usage_voix_soratnt_offnet").as("usage_voix_soratnt_offnet"),
          sum("usage_voix_soratnt_onnet").as("usage_voix_soratnt_onnet"),
          sum("usage_voix_soratnt_roaming").as("usage_voix_soratnt_roaming"),
          sum("usage_data").as("usage_data"),
          sum("ca_appels").as("ca_appels"),
          sum("ca_sms").as("ca_sms"),
          sum("ca_achats_bundles").as("ca_achats_bundles"),
          sum("ca_remboursement_sos").as("ca_remboursement_sos"),
          sum("ca_sva").as("ca_sva"))


      val df_ca_factures = spark.sql(" select master_id as master_id_fact , univers, SUM(NVL(montant_total_paye,0)) as ca_factures FROM vision_uc1_datamart.vision_dm_facturation WHERE              date_jour between date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1))                    and univers='FIXE' and master_id is not null    group by master_id,univers ")


      //val df_facturesbroadband ==> mensuelle


      //création de la dataframe finale
      val join2 = df_socio_pro_geoloc_metric2
        .join(df_profileconvergence, df_socio_pro_geoloc_metric2("master_id") === df_profileconvergence("master_id_profile"), "LEFT")
        .join(df_ca_global_tranche_valeur, df_socio_pro_geoloc_metric2("master_id") === df_ca_global_tranche_valeur("master_id_tranche"), "LEFT")
        .join(df_ca_factures, df_socio_pro_geoloc_metric2("master_id") === df_ca_factures("master_id_fact") && df_socio_pro_geoloc_metric2("univers") === df_ca_factures("univers"), "LEFT")
        .join(df_socio_pro_geoloc_segment_master_id, df_socio_pro_geoloc_metric2("master_id") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .join(df_univers_ca_global, df_socio_pro_geoloc_metric2("master_id") === df_univers_ca_global("master_id_ca") && df_socio_pro_geoloc_metric2("univers") === df_univers_ca_global("univers") , "LEFT")
        .select(
          df_socio_pro_geoloc_metric2("master_id"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_metric2("univers"),
          df_profileconvergence("profilConvergence"),
          df_ca_global_tranche_valeur("trancheValeur"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_socio_pro_geoloc_metric2("nb_msisdn"),
          df_univers_ca_global("ca_global"),
          df_socio_pro_geoloc_metric2("usage_voix_soratnt_offnet"),
          df_socio_pro_geoloc_metric2("usage_voix_soratnt_onnet"),
          df_socio_pro_geoloc_metric2("usage_voix_soratnt_roaming"),
          df_socio_pro_geoloc_metric2("usage_data"),
          df_socio_pro_geoloc_metric2("ca_appels"),
          df_socio_pro_geoloc_metric2("ca_sms"),
          df_socio_pro_geoloc_metric2("ca_achats_bundles"),
          df_socio_pro_geoloc_metric2("ca_remboursement_sos"),
          df_socio_pro_geoloc_metric2("ca_sva"),
          df_ca_factures("ca_factures"))

      val join2_result = join2.groupBy("date_jour", "univers", "profilConvergence", "trancheValeur", "region", "commune")
        .agg(
          count("master_id").as("nb_client"),
          sum("nb_msisdn").as("nb_msisdn"),
          sum("ca_global").as("CA_total"),
          sum("usage_voix_soratnt_offnet").as("usage_voix_soratnt_offnet"),
          sum("usage_voix_soratnt_onnet").as("usage_voix_soratnt_onnet"),
          sum("usage_voix_soratnt_roaming").as("usage_voix_soratnt_roaming"),
          sum("usage_data").as("usage_data"),
          sum("ca_appels").as("CA_appels"),
          sum("ca_sms").as("CA_SMS"),
          sum("ca_achats_bundles").as("CA_achats_bundles"),
          sum("ca_remboursement_sos").as("CA_remboursement_SOS"),
          sum("ca_sva").as("CA_SVA"),
          sum("ca_factures").as("CA_factures"))
        .na.drop(Seq("date_jour"))
        .withColumn("_id", concat(
          coalesce(col("univers"), lit("")),
          lit("_"),
          coalesce(col("profilConvergence"), lit("")),
          lit("_"),
          coalesce(col("trancheValeur"), lit("")),
          lit("_"),
          coalesce(col("region"), lit("")),
          lit("_"),
          coalesce(col("commune"), lit("")),
          lit("_"),
          date_format(col("date_jour"), "yyyy-MM")
        ))

      //join1_result.write.format("org.apache.phoenix.spark").mode("overwrite").option("table", "vision_dm_client_stat").option("zkUrl", "10.240.36.86,10.240.36.87,10.240.36.90:2181").save()
      MongoSpark.save(join2_result.write.option("spark.mongodb.output.uri", "mongodb://OciAppVision:3zNgDFE5kuQp9bEs@10.242.30.114/ociVision.vision_dm_finance_stat?authSource=admin").mode("append"))






      // table 4 ****************************************************************************************************************** vision_dm_segmentation

      val df_dm_offre_statut_business_mobile = spark.sql(" SELECT master_id_sb, statut_business FROM (SELECT master_id as master_id_sb, statut_business , ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY statut_business) AS row_num FROM vision_uc1_datamart.vision_dm_offre  where            date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre WHERE date_jour BETWEEN date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1)) )             AND univers='MOBILE' and statut_business is not null  ) first_req WHERE row_num=1  ")
      val df_dm_offre_statut_business_adsl = spark.sql(" SELECT master_id_sb, statut_business FROM (SELECT master_id as master_id_sb, statut_business , ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY statut_business) AS row_num FROM vision_uc1_datamart.vision_dm_offre   where             date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre WHERE date_jour BETWEEN date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1)) )             AND univers='ADSL'   and statut_business is not null  ) first_req WHERE row_num=1  ")
      val df_dm_offre_statut_business_ftth = spark.sql(" SELECT master_id_sb, statut_business FROM (SELECT master_id as master_id_sb, statut_business , ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY statut_business) AS row_num FROM vision_uc1_datamart.vision_dm_offre   where             date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre WHERE date_jour BETWEEN date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1)) )             AND univers='FTTH'   and statut_business is not null  ) first_req WHERE row_num=1  ")
      val df_dm_offre_statut_business_tdd = spark.sql(" SELECT master_id_sb, statut_business FROM (SELECT master_id as master_id_sb, statut_business , ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY statut_business) AS row_num FROM vision_uc1_datamart.vision_dm_offre    where             date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre WHERE date_jour BETWEEN date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1)) )             AND univers='TDD'    and statut_business is not null  ) first_req WHERE row_num=1  ")
      val df_dm_offre_statut_business_fdd = spark.sql(" SELECT master_id_sb, statut_business FROM (SELECT master_id as master_id_sb, statut_business , ROW_NUMBER() OVER (PARTITION BY master_id ORDER BY statut_business) AS row_num FROM vision_uc1_datamart.vision_dm_offre    where             date_jour = ( SELECT max(date_jour) FROM vision_uc1_datamart.vision_dm_offre WHERE date_jour BETWEEN date_format(add_months(current_date, -1),'yyyy-MM-01' ) AND last_day(add_months(current_date, -1)) )             AND univers='FDD'    and statut_business is not null  ) first_req WHERE row_num=1  ")

      val df_ca_mobile_statut_business = df_ca_mobile.join(df_dm_offre_statut_business_mobile, df_ca_mobile("master_id_ca") === df_dm_offre_statut_business_mobile("master_id_sb"), "LEFT")
        .select(df_ca_mobile("master_id_ca"), df_ca_mobile("univers"), df_ca_mobile("ca_global"), df_dm_offre_statut_business_mobile("statut_business"))

      val df_ca_adsl_statut_business = df_ca_adsl.join(df_dm_offre_statut_business_adsl, df_ca_adsl("master_id_ca") === df_dm_offre_statut_business_adsl("master_id_sb"), "LEFT")
        .select(df_ca_adsl("master_id_ca"), df_ca_adsl("univers"), df_ca_adsl("ca_global"), df_dm_offre_statut_business_adsl("statut_business"))

      val df_ca_ftth_statut_business = df_ca_ftth.join(df_dm_offre_statut_business_ftth, df_ca_ftth("master_id_ca") === df_dm_offre_statut_business_ftth("master_id_sb"), "LEFT")
        .select(df_ca_ftth("master_id_ca"), df_ca_ftth("univers"), df_ca_ftth("ca_global"), df_dm_offre_statut_business_ftth("statut_business"))

      val df_ca_tdd_statut_business = df_ca_tdd.join(df_dm_offre_statut_business_tdd, df_ca_tdd("master_id_ca") === df_dm_offre_statut_business_tdd("master_id_sb"), "LEFT")
        .select(df_ca_tdd("master_id_ca"), df_ca_tdd("univers"), df_ca_tdd("ca_global"), df_dm_offre_statut_business_tdd("statut_business"))

      val df_ca_fdd_statut_business = df_ca_fdd.join(df_dm_offre_statut_business_fdd, df_ca_fdd("master_id_ca") === df_dm_offre_statut_business_fdd("master_id_sb"), "LEFT")
        .select(df_ca_fdd("master_id_ca"), df_ca_fdd("univers"), df_ca_fdd("ca_global"), df_dm_offre_statut_business_fdd("statut_business"))

      // colonne master_id et ca_global pour mobile
      val df_ca_mobile_segment = df_ca_mobile_statut_business
        .withColumn("segment", when(col("statut_business") =!= "Actif", "Inactif")
          .when(col("ca_global") <= 1000, "Very Low")
          .when(col("ca_global") > 1000 && col("ca_global") <= 2500, "Low")
          .when(col("ca_global") > 2500 && col("ca_global") <= 10000, "Middle")
          .when(col("ca_global") > 10000 && col("ca_global") <= 50000, "High")
          .otherwise("Very High"))


      // colonne master_id et ca_global pour adsl
      val df_ca_adsl_segment = df_ca_adsl_statut_business
        .withColumn("segment", when(col("statut_business") =!= "Actif", "Inactif")
          .when(col("ca_global") <= 5000, "Very Low")
          .when(col("ca_global") > 5000 && col("ca_global") <= 15000, "Low")
          .when(col("ca_global") > 15000 && col("ca_global") <= 50000, "Middle")
          .when(col("ca_global") > 50000 && col("ca_global") <= 100000, "High")
          .otherwise("Very High"))

      // colonne master_id et ca_global pour ftth
      val df_ca_ftth_segment = df_ca_ftth_statut_business
        .withColumn("segment", when(col("statut_business") =!= "Actif", "Inactif")
          .when(col("ca_global") <= 5000, "Very Low")
          .when(col("ca_global") > 5000 && col("ca_global") <= 15000, "Low")
          .when(col("ca_global") > 15000 && col("ca_global") <= 50000, "Middle")
          .when(col("ca_global") > 50000 && col("ca_global") <= 100000, "High")
          .otherwise("Very High"))

      // colonne master_id et ca_global pour tdd
      val df_ca_tdd_segment = df_ca_tdd_statut_business
        .withColumn("segment", when(col("statut_business") =!= "Actif", "Inactif")
          .when(col("ca_global") <= 5000, "Very Low")
          .when(col("ca_global") > 5000 && col("ca_global") <= 15000, "Low")
          .when(col("ca_global") > 15000 && col("ca_global") <= 50000, "Middle")
          .when(col("ca_global") > 50000 && col("ca_global") <= 100000, "High")
          .otherwise("Very High"))

      // colonne master_id et ca_global pour fdd
      val df_ca_fdd_segment = df_ca_fdd_statut_business
        .withColumn("segment", when(col("statut_business") =!= "Actif", "Inactif")
          .when(col("ca_global") <= 5000, "Very Low")
          .when(col("ca_global") > 5000 && col("ca_global") <= 15000, "Low")
          .when(col("ca_global") > 15000 && col("ca_global") <= 50000, "Middle")
          .when(col("ca_global") > 50000 && col("ca_global") <= 100000, "High")
          .otherwise("Very High"))

      // colonne master_id et ca_global pour om
      val df_ca_om_segment = df_ca_om
        .withColumn("segment", when(col("ca_global") <= 500, "Very Low")
          .when(col("ca_global") > 500 && col("ca_global") <= 2000, "Low")
          .when(col("ca_global") > 2000 && col("ca_global") <= 5000, "Middle")
          .when(col("ca_global") > 5000 && col("ca_global") <= 20000, "High")
          .otherwise("Very High"))



      // ******************** Mobile*TDD, Mobile*FDD, Mobile*FTTH, Mobile*OM, Mobile*ADSL OM*TDD, OM*FDD, OM*FTTH, OM*ADSL


      // ******************** couple MOBILE*TDD
      val df_mobile_tdd_segment = df_ca_mobile_segment.union(df_ca_tdd_segment)
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_mobile_tdd_segment_all = df_mobile_tdd_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_mobile_tdd_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_mobile_tdd_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_mobile_tdd_segment("univers1"),
          df_mobile_tdd_segment("univers2"),
          df_mobile_tdd_segment("segment1"),
          df_mobile_tdd_segment("segment2"),
          df_mobile_tdd_segment("ca_global").as("caClientCroise"),
          df_mobile_tdd_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))

      // ******************** couple MOBILE*FDD
      val df_mobile_fdd_segment = df_ca_mobile_segment.union(df_ca_fdd_segment)
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_mobile_fdd_segment_all = df_mobile_fdd_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_mobile_fdd_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_mobile_fdd_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_mobile_fdd_segment("univers1"),
          df_mobile_fdd_segment("univers2"),
          df_mobile_fdd_segment("segment1"),
          df_mobile_fdd_segment("segment2"),
          df_mobile_fdd_segment("ca_global").as("caClientCroise"),
          df_mobile_fdd_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))

      // ******************** couple MOBILE*FTTH
      val df_mobile_ftth_segment = df_ca_mobile_segment.union(df_ca_ftth_segment)
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_mobile_ftth_segment_all = df_mobile_ftth_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_mobile_ftth_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_mobile_ftth_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_mobile_ftth_segment("univers1"),
          df_mobile_ftth_segment("univers2"),
          df_mobile_ftth_segment("segment1"),
          df_mobile_ftth_segment("segment2"),
          df_mobile_ftth_segment("ca_global").as("caClientCroise"),
          df_mobile_ftth_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))

      // ******************** couple MOBILE*OM
      val df_mobile_om_segment = df_ca_mobile_segment.select("master_id_ca", "univers", "ca_global", "segment").union(df_ca_om_segment)
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_mobile_om_segment_all = df_mobile_om_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_mobile_om_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_mobile_om_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_mobile_om_segment("univers1"),
          df_mobile_om_segment("univers2"),
          df_mobile_om_segment("segment1"),
          df_mobile_om_segment("segment2"),
          df_mobile_om_segment("ca_global").as("caClientCroise"),
          df_mobile_om_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))

      // ******************** couple MOBILE*ADSL
      val df_mobile_adsl_segment = df_ca_mobile_segment.union(df_ca_adsl_segment)
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "MOBILE", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "MOBILE", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_mobile_adsl_segment_all = df_mobile_adsl_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_mobile_adsl_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_mobile_adsl_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_mobile_adsl_segment("univers1"),
          df_mobile_adsl_segment("univers2"),
          df_mobile_adsl_segment("segment1"),
          df_mobile_adsl_segment("segment2"),
          df_mobile_adsl_segment("ca_global").as("caClientCroise"),
          df_mobile_adsl_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))


      // ******************** couple OM*TDD
      val df_om_tdd_segment = df_ca_om_segment.union(df_ca_tdd_segment.select("master_id_ca", "univers", "ca_global", "segment"))
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "OM", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "OM", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "OM", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "OM", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_om_tdd_segment_all = df_om_tdd_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_om_tdd_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_om_tdd_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_om_tdd_segment("univers1"),
          df_om_tdd_segment("univers2"),
          df_om_tdd_segment("segment1"),
          df_om_tdd_segment("segment2"),
          df_om_tdd_segment("ca_global").as("caClientCroise"),
          df_om_tdd_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))

      // ******************** couple OM*FDD
      val df_om_fdd_segment = df_ca_om_segment.union(df_ca_fdd_segment.select("master_id_ca", "univers", "ca_global", "segment"))
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "OM", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "OM", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "OM", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "OM", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_om_fdd_segment_all = df_om_fdd_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_om_fdd_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_om_fdd_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_om_fdd_segment("univers1"),
          df_om_fdd_segment("univers2"),
          df_om_fdd_segment("segment1"),
          df_om_fdd_segment("segment2"),
          df_om_fdd_segment("ca_global").as("caClientCroise"),
          df_om_fdd_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))

      // ******************** couple OM*FTTH
      val df_om_ftth_segment = df_ca_om_segment.union(df_ca_ftth_segment.select("master_id_ca", "univers", "ca_global", "segment"))
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "OM", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "OM", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "OM", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "OM", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_om_ftth_segment_all = df_om_ftth_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_om_ftth_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_om_ftth_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_om_ftth_segment("univers1"),
          df_om_ftth_segment("univers2"),
          df_om_ftth_segment("segment1"),
          df_om_ftth_segment("segment2"),
          df_om_ftth_segment("ca_global").as("caClientCroise"),
          df_om_ftth_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))

      // ******************** couple OM*ADSL
      val df_om_adsl_segment = df_ca_om_segment.union(df_ca_adsl_segment.select("master_id_ca", "univers", "ca_global", "segment"))
        .groupBy("master_id_ca")
        .agg(count("*").alias("cnt"),
          collect_list("univers").as("univers"),
          collect_list("segment").as("segment"),
          sum("ca_global").as("ca_global"))
        .filter(col("cnt") > 1)
        .withColumn("nbTotalCroise", lit(1))
        .withColumn("univers1", when(col("univers").getItem(0) === "OM", col("univers").getItem(0))
          .otherwise(col("univers").getItem(1)))
        .withColumn("univers2", when(col("univers").getItem(0) === "OM", col("univers").getItem(1))
          .otherwise(col("univers").getItem(0)))
        .withColumn("segment1", when(col("univers").getItem(0) === "OM", col("segment").getItem(0))
          .otherwise(col("segment").getItem(1)))
        .withColumn("segment2", when(col("univers").getItem(0) === "OM", col("segment").getItem(1))
          .otherwise(col("segment").getItem(0)))

      val df_om_adsl_segment_all = df_om_adsl_segment.
        join(df_socio_pro_geoloc_segment_master_id, df_om_adsl_segment("master_id_ca") === df_socio_pro_geoloc_segment_master_id("master_id"), "LEFT")
        .select(df_om_adsl_segment("master_id_ca"),
          df_socio_pro_geoloc_segment_master_id("date_jour"),
          df_socio_pro_geoloc_segment_master_id("region"),
          df_socio_pro_geoloc_segment_master_id("commune"),
          df_om_adsl_segment("univers1"),
          df_om_adsl_segment("univers2"),
          df_om_adsl_segment("segment1"),
          df_om_adsl_segment("segment2"),
          df_om_adsl_segment("ca_global").as("caClientCroise"),
          df_om_adsl_segment("nbTotalCroise"))
        .groupBy("date_jour", "region", "commune", "univers1", "univers2", "segment1", "segment2")
        .agg(sum("caClientCroise").as("caClientCroise"),
          sum("nbTotalCroise").as("nbTotalCroise")
        ).na.drop(Seq("date_jour"))


      val df_segment_all = df_mobile_tdd_segment_all.union(df_mobile_fdd_segment_all).union(df_mobile_ftth_segment_all).union(df_mobile_om_segment_all).union(df_mobile_adsl_segment_all)
        .union(df_om_tdd_segment_all).union(df_om_fdd_segment_all).union(df_om_ftth_segment_all).union(df_om_adsl_segment_all)
        .withColumn("_id", concat(
          coalesce(col("region"), lit("")),
          lit("_"),
          coalesce(col("commune"), lit("")),
          lit("_"),
          coalesce(col("univers1"), lit("")),
          lit("_"),
          coalesce(col("univers2"), lit("")),
          lit("_"),
          coalesce(col("segment1"), lit("")),
          lit("_"),
          coalesce(col("segment2"), lit("")),
          lit("_"),
          date_format(col("date_jour"), "yyyy-MM")
        ))

      MongoSpark.save(df_segment_all.write.option("spark.mongodb.output.uri", "mongodb://OciAppVision:3zNgDFE5kuQp9bEs@10.242.30.114/ociVision.vision_dm_segmentation?authSource=admin").mode("append"))






      // table 5 ****************************************************************************************************************** vision_dm_indicator

      val df_dm_indicator = df_ca_mobile.select("master_id_ca", "ca_global")
        .union(df_ca_adsl.select("master_id_ca", "ca_global"))
        .union(df_ca_ftth.select("master_id_ca", "ca_global"))
        .union(df_ca_tdd.select("master_id_ca", "ca_global"))
        .union(df_ca_fdd.select("master_id_ca", "ca_global"))
        .groupBy("master_id_ca")
        .agg(sum("ca_global").as("ca_global"))
        .agg(count("*").alias("nbr_client_total"),
          sum("ca_global").as("ca_total"))
        .withColumn("date_jour", date_sub(current_date(), 1))
        .withColumn("_id", date_format(date_sub(current_date(), 1), "yyyy-MM"))

      MongoSpark.save(df_dm_indicator.write.option("spark.mongodb.output.uri", "mongodb://OciAppVision:3zNgDFE5kuQp9bEs@10.242.30.114/ociVision.vision_dm_indicator?authSource=admin").mode("append"))





    }



    //hive.close()
    spark.stop()
    spark.close()
  }

}
