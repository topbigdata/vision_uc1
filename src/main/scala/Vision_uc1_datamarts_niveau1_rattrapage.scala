import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, collect_set, concat_ws, current_date, date_sub,sum, when}
import java.util.Date


object Vision_uc1_datamarts_niveau1_rattrapage {


  def main(args: Array[String]): Unit = {


    // création de spark session
    val spark = SparkSession.builder().appName("Vision_uc1_datamarts_niveau1_rattrapage").master("yarn").enableHiveSupport().getOrCreate()
    // création de hive warehouse connection session
    //val hive = com.hortonworks.hwc.HiveWarehouseSession.session(spark).build()

    //*****************************            *****************************//

    // chargement des données de datalake RCU dans les deux dataframes df_rcu et df_rcu_socio et persist dans la RAM
    val df_rcu=spark.sql(" SELECT master_id, MSISDN FROM  (    SELECT master_id , MSISDN , ROW_NUMBER() OVER (PARTITION BY MSISDN ORDER BY date_creation_mid DESC) AS row_num  FROM   ( SELECT master_id , CASE WHEN LENGTH(TRIM(phone_num)) = 8 THEN CASE WHEN LIBELLE_TYPESERVICE = 'FIXE' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'WIMAX' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'MOBILE' THEN CASE WHEN SUBSTR(TRIM(phone_num),2,1) IN ('0','1','2','3') THEN CONCAT('01',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('4','5','6') THEN CONCAT('05',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('7','8','9') THEN CONCAT('07',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'AVISO' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'ORANGE MONEY' THEN CASE WHEN SUBSTR(TRIM(phone_num),2,1) IN ('0','1','2','3') THEN CONCAT('01',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('4','5','6') THEN CONCAT('05',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('7','8','9') THEN CONCAT('07',TRIM(phone_num)) ELSE TRIM(phone_num) END ELSE TRIM(phone_num) END ELSE TRIM(phone_num) END AS MSISDN  , date_creation_mid FROM vision_uc1_datalake.rcu where date_jour = (SELECT MAX(date_jour) FROM vision_uc1_datalake.rcu WHERE date_jour <= '"+args(0)+"' )  ) first_req   ) second_req  WHERE row_num = 1 " )
      .persist()

    // chargement des données parc
    val df_parc_fixe = spark.sql   (" SELECT msisdn_parc,email_parc,ville_parc,commune_parc,quartier_parc,NUMERO_CLIENT,nom_offre,identifiant_offre,date_aquisation,type_offre,univers,categorie_client,statut_technique,statut_business,date_jour  FROM ( SELECT TRIM(numero_appel) as msisdn_parc ,e_mail as email_parc,'NULL'||' : SOURCE_PARC_FIXE' as ville_parc , COMMUNE||' : SOURCE_PARC_FIXE' as commune_parc , 'NULL'||' : SOURCE_PARC_FIXE'  as quartier_parc , TRIM(NUMERO_CLIENT) as NUMERO_CLIENT , 'NULL' as nom_offre  , 'NULL'  as identifiant_offre  , DATE_MS          as date_aquisation , TYPE_ABO as type_offre, 'FIXE' as univers ,        type_client as categorie_client     ,  CASE WHEN etat_client = 'ACTIF' then 'Actif' else etat_client END AS statut_technique  , CASE WHEN etat_client = 'ACTIF' then 'Actif' else etat_client END AS statut_business, to_date('"+args(0)+"') as date_jour, ROW_NUMBER() OVER (PARTITION BY TRIM(numero_appel) ORDER BY DATE_MS DESC) AS row_num   FROM vision_uc1_datalake.gaia_parc_fixe             WHERE date_jour = (SELECT MAX(date_jour) FROM vision_uc1_datalake.gaia_parc_fixe WHERE date_jour <= '"+args(0)+"' ) ) first_req WHERE row_num=1 ")
      .persist()

    val df_parc_mobile = spark.sql (" SELECT msisdn_parc,email_parc,ville_parc,commune_parc,quartier_parc,compte_client,nom_offre,identifiant_offre,date_aquisation,type_offre,univers,categorie_client,statut_technique,date_jour  FROM ( SELECT TRIM(msisdn) as msisdn_parc ,email as email_parc, VILLE||' : SOURCE_PARC_MOBILE'  as ville_parc , 'NULL'||' : SOURCE_PARC_MOBILE' as commune_parc ,'NULL'||' : SOURCE_PARC_MOBILE' as quartier_parc , TRIM(compte_client) as compte_client,  plan_tarifaire as nom_offre  ,  tmcode as identifiant_offre  , DATE_ACTIVATION  as date_aquisation , TRIM(type_offre) as type_offre           ,  'MOBILE' as univers,       categorie_client          ,  CASE WHEN statut_sim = 'r' then 'Utilisable' WHEN statut_sim = 'a' then 'Actif' WHEN statut_sim = 'd' then 'désactivé' WHEN statut_sim = 'v' then 'déclaré' else statut_sim END AS statut_technique   , to_date('"+args(0)+"') as date_jour , ROW_NUMBER() OVER (PARTITION BY TRIM(msisdn) ORDER BY DATE_ACTIVATION DESC) AS row_num  FROM vision_uc1_datalake.bscs_parc_mobile  WHERE date_jour = (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_mobile WHERE date_jour <= '"+args(0)+"' )  ) first_req WHERE row_num=1  ")
      .persist()

    val df_parc_adsl = spark.sql   (" SELECT msisdn_parc,email_parc,ville_parc,commune_parc,quartier_parc,custcode,nom_offre,identifiant_offre,date_aquisation,type_offre,univers,categorie_client,statut_technique,date_jour  FROM ( SELECT TRIM(numero_adsl) as msisdn_parc ,email as email_parc, VILLE||' : SOURCE_PARC_ADSL'  as ville_parc , COMMUNE||' : SOURCE_PARC_ADSL' as commune_parc , QUARTIER||' : SOURCE_PARC_ADSL'  as quartier_parc , TRIM(custcode) as custcode ,   plan_tarifaire as nom_offre  ,  id_offre as identifiant_offre , DATE_ACTIVATION as date_aquisation , type_offre,             'ADSL' as univers ,    category_client as categorie_client ,  etat as statut_technique                                         , to_date('"+args(0)+"') as date_jour , ROW_NUMBER() OVER (PARTITION BY TRIM(numero_adsl) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_adsl WHERE date_jour = (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_adsl WHERE date_jour <= '"+args(0)+"' ) ) first_req WHERE row_num=1 ")
      .persist()

    val df_parc_ftth = spark.sql   (" SELECT msisdn_parc,email_parc,ville_parc,commune_parc,quartier_parc,custcode,nom_offre,identifiant_offre,date_aquisation,type_offre,univers,categorie_client,statut_technique,date_jour  FROM ( SELECT TRIM(nd)          as msisdn_parc ,email as email_parc, VILLE||' : SOURCE_PARC_FTTH'  as ville_parc , COMMUNE||' : SOURCE_PARC_FTTH' as commune_parc , QUARTIER||' : SOURCE_PARC_FTTH'  as quartier_parc , TRIM(custcode) as custcode ,   plan_tarifaire as nom_offre  ,  'NULL' as identifiant_offre   , DATE_ACTIVATION as date_aquisation , type_offre,              'FTTH' as univers ,   categorie_client as categorie_client , etat as statut_technique                                         , to_date('"+args(0)+"') as date_jour , ROW_NUMBER() OVER (PARTITION BY TRIM(nd) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_ftth  WHERE date_jour = (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_ftth WHERE date_jour <= '"+args(0)+"' ) AND  date_resiliation is NULL ) first_req WHERE row_num=1 ")
      .persist()

    val df_parc_tdd = spark.sql    (" SELECT msisdn_parc,email_parc,ville_parc,commune_parc,quartier_parc,custcode,nom_offre,identifiant_offre,date_aquisation,type_offre,univers,categorie_client,statut_technique,date_jour  FROM ( SELECT TRIM(nd)          as msisdn_parc ,email as email_parc, VILLE||' : SOURCE_PARC_TDD'  as ville_parc , COMMUNE||' : SOURCE_PARC_TDD' as commune_parc , QUARTIER||' : SOURCE_PARC_TDD'  as quartier_parc , TRIM(custcode) as custcode ,   plan_tarifaire as Nom_offre  ,  id_offre as identifiant_offre  , DATE_ACTIVATION as date_aquisation , type_offre ,             'TDD' as univers,   categorie_client     ,     CASE WHEN statut = 'a' then 'Actif' WHEN statut = 'o' then 'En attente' WHEN statut = 's' then 'Suspendu' WHEN statut = 'd' then 'désactivé' else statut END AS statut_technique                                       , to_date('"+args(0)+"') as date_jour , ROW_NUMBER() OVER (PARTITION BY TRIM(nd) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_tdd  WHERE date_jour = (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_tdd WHERE date_jour <= '"+args(0)+"' ) ) first_req WHERE row_num=1 ")
      .persist()

    val df_parc_fdd = spark.sql    (" SELECT msisdn_parc,email_parc,ville_parc,commune_parc,quartier_parc,custcode,nom_offre,identifiant_offre,date_aquisation,type_offre,univers,categorie_client,statut_technique,date_jour  FROM ( SELECT TRIM(nd)          as msisdn_parc ,email as email_parc, VILLE||' : SOURCE_PARC_FDD'  as ville_parc , COMMUNE||' : SOURCE_PARC_FDD' as commune_parc , QUARTIER||' : SOURCE_PARC_FDD'  as quartier_parc , TRIM(custcode) as custcode ,   plan_tarifaire as Nom_offre  ,  id_offre as identifiant_offre  , DATE_ACTIVATION as date_aquisation , type_offre ,             'FDD' as univers,    categorie_client ,        CASE WHEN statut = 'a' then 'Actif' WHEN statut = 'o' then 'En attente' WHEN statut = 's' then 'Suspendu' WHEN statut = 'd' then 'désactivé' else statut END AS statut_technique                                       , to_date('"+args(0)+"') as date_jour , ROW_NUMBER() OVER (PARTITION BY TRIM(nd) ORDER BY DATE_ACTIVATION DESC) AS row_num FROM vision_uc1_datalake.bscs_parc_broadband_fdd   WHERE date_jour = (SELECT MAX(date_jour) FROM vision_uc1_datalake.bscs_parc_broadband_fdd WHERE date_jour <= '"+args(0)+"' ) ) first_req WHERE row_num=1 ")
      .persist()

    // calcul de couple master_id, custcode entre parc et rcu
    val df_union_parc_msisdn_custcode = df_parc_mobile.filter(df_parc_mobile("type_offre")==="POSTPAID").select("msisdn_parc","compte_client").withColumnRenamed("compte_client","custcode").distinct()
      .union(df_parc_adsl.select("msisdn_parc","custcode").distinct())
      .union(df_parc_ftth.select("msisdn_parc","custcode").distinct())
      .union(df_parc_fdd.select("msisdn_parc","custcode").distinct())
      .union(df_parc_tdd.select("msisdn_parc","custcode").distinct())
      .distinct()
      .dropDuplicates("msisdn_parc")

    // couple master_id / custcode distinct
    val df_union_parc_custcode_master_id = df_union_parc_msisdn_custcode.join(df_rcu, df_union_parc_msisdn_custcode("msisdn_parc")===df_rcu("msisdn") ,"LEFT")
      .select(df_rcu("master_id"), df_union_parc_msisdn_custcode("custcode"))
      .distinct()
      .dropDuplicates("custcode")

    //  *****************************************************DM CA ******************************************** usage simple / TERADATA_ACHAT_PASS / teradata_sva / teradata_sos_credit_data / gaia_encaissment_facture_fixe/ teradata_e_recharge/ dwocit_om/ 6 parc / RCU / dwocit_bub_fixe_ods_base_cont / dwocit_bub_rechargement_ods

    // données des achats des pass pour mobile prepaid & pour mobile hybride par MSISDN distinct
    val df_conso_achats_pass_mobile_prepaid_hybrid = spark.sql("SELECT SUBSTR(ACC_NBR,-10) AS MSISDN ,  SUM(CA_ACHAT_PASS) AS montant_achat_pass  FROM vision_uc1_datalake.TERADATA_ACHAT_PASS  WHERE date_jour = '"+args(0)+"'  GROUP BY SUBSTR(ACC_NBR,-10) ")

    // données des sva pour mobile prepaid & pour mobile hybride par MSISDN distinct
    val df_conso_achats_sva_mobile_prepaid_hybrid = spark.sql("SELECT CASE WHEN LENGTH(TRIM(SVA.compte_client)) = 13 AND SUBSTR(TRIM(SVA.compte_client),1,3) = '225' THEN SUBSTR(TRIM(SVA.compte_client),-10) WHEN LENGTH(TRIM(SVA.compte_client)) = 14 AND SUBSTR(TRIM(SVA.compte_client),1,3) = '225' AND SUBSTR(TRIM(SVA.compte_client),-1) = '.' THEN SUBSTR(TRIM(SVA.compte_client),4,10) ELSE TRIM(SVA.compte_client) END AS MSISDN  , SUM(SVA.MONTANT_SVA) AS MONTANT_SVA FROM vision_uc1_datalake.teradata_sva SVA WHERE SVA.date_jour = '"+args(0)+"'  GROUP BY CASE WHEN LENGTH(TRIM(SVA.compte_client)) = 13 AND SUBSTR(TRIM(SVA.compte_client),1,3) = '225' THEN SUBSTR(TRIM(SVA.compte_client),-10) WHEN LENGTH(TRIM(SVA.compte_client)) = 14 AND SUBSTR(TRIM(SVA.compte_client),1,3) = '225' AND SUBSTR(TRIM(SVA.compte_client),-1) = '.' THEN SUBSTR(TRIM(SVA.compte_client),4,10) ELSE TRIM(SVA.compte_client) END ")

    // données des credit sos pour mobile prepaid & pour mobile hybride par MSISDN distinct
    val df_conso_credit_sos_mobile_prepaid_hybrid = spark.sql(" SELECT SUBSTR(MSISDN,-10) AS MSISDN , SUM(MONTANT_REMBOURSE) AS MONTANT_REMBOURSE  FROM vision_uc1_datalake.teradata_sos_credit_data WHERE date_jour = '"+args(0)+"' GROUP BY SUBSTR(MSISDN,-10) ")

    // SEULEMENT POUR TDD & FDD MSISDN distinct & ca_e_recharge via zebra
    val df_conso_e_recharge_zebra = spark.sql("SELECT SUBSTR(client_final,-10) as MSISDN , SUM (TRANSFER_AMOUNT) AS ca_e_recharge FROM vision_uc1_datalake.teradata_e_recharge WHERE date_jour= '"+args(0)+"' GROUP BY  SUBSTR(client_final,-10)")
    //seuelement pour TDD & FDD MSISDN distinct & rechargemet_om
    val df_conso_rechargement_om = spark.sql(" SELECT compte_client as msisdn, sum(MONTANT_TRANSACTION) as rechargemet_om FROM vision_uc1_datalake.dwocit_om WHERE date_jour = '"+args(0)+"' AND TRIM(service_de_base)='RC' group by  compte_client ")






    if (args(1) == "vision_dm_usage")
    {
      // 10 **************************************************** datamart usage **************************************************** usage fixe / usage mobile / 6 parc / rcu
      // chargement des données de datalake GAIA_TRAFIC_FIXE_POSTPAID // presque tous les numéro à 10 chiffres
      val df_fixe_postpaid = spark.sql("SELECT NUMERO_APPELANT AS MSISDN  ,  SUM(NVL(DUREE_APPEL_MINUTE,0)) AS usage_voix_sortant ,  0  AS usage_sms_sortant , 0 AS usage_data, case  when type_appel in ( 'Mobiles Orange','Local') then 'onnet' when type_appel = 'International' then 'international' else 'offnet' end as type,  case  when SUBSTR(nd_appele, 1,2 ) = '11' then 'MOOV' when SUBSTR(nd_appele, 1,2 ) = '15' then 'MTN' else 'OCI' end as operateur,  'NULL' as destination,  'NULL' as type_de_compte ,  'FIXE_POSTPAID' as univers,  'VOIX' as canal,  date_jour FROM vision_uc1_datalake.GAIA_TRAFIC_FIXE_POSTPAID WHERE date_jour = '" + args(0) + "' GROUP BY NUMERO_APPELANT , case when type_appel in ( 'Mobiles Orange','Local') then 'onnet' when type_appel = 'International' then 'international' else 'offnet' end, case when SUBSTR(nd_appele, 1,2 ) = '11' then 'MOOV' when SUBSTR(nd_appele, 1,2 ) = '15' then 'MTN' else 'OCI' end, date_jour ")

      // chargement des données de datalake TERADATA_USAGE_SIMPLE
      val df_usage_simple_mobile = spark.sql("SELECT CASE WHEN LENGTH(CALLING_NBR) = 13 AND SUBSTR(CALLING_NBR,1,3) = '225' THEN SUBSTR(CALLING_NBR,-10) WHEN LENGTH(CALLING_NBR) = 18 AND SUBSTR(CALLING_NBR,1,8) = 'tel:+225' THEN SUBSTR(CALLING_NBR,-10) ELSE CALLING_NBR END AS MSISDN  ,   SUM (case when UPPER(CANAL) like '%VOIX%' THEN NVL(DURATION,0) else 0 END ) AS usage_voix_sortant , count(case when UPPER(CATEGORY_EVENEMENT) like '%SMS%' then 1 else 0 end)  AS usage_sms_sortant , SUM (case when TRIM(CANAL) like '%DATA%' THEN NVL(facturation,0) else 0 END ) AS usage_data , case when UPPER(category_evenement) like '%OFFNET%' then 'offnet' when UPPER(category_evenement) like '%INTER%' then 'international' when UPPER(category_evenement) like '%ROAMING%' then 'roaming' else 'onnet' end as type, OPERATEUR as operateur, DESTINATION as destination, BALANCE_IMPACTEE as type_de_compte , univers as univers_first, TRIM(CANAL) as canal, date_jour FROM vision_uc1_datalake.TERADATA_USAGE_SIMPLE WHERE date_jour = '" + args(0) + "' AND TRIM(univers)='MOBILE' GROUP BY  CASE WHEN LENGTH(CALLING_NBR) = 13 AND SUBSTR(CALLING_NBR,1,3) = '225' THEN SUBSTR(CALLING_NBR,-10) WHEN LENGTH(CALLING_NBR) = 18 AND SUBSTR(CALLING_NBR,1,8) = 'tel:+225' THEN SUBSTR(CALLING_NBR,-10) ELSE CALLING_NBR END,  case when UPPER(category_evenement) like '%OFFNET%' then 'offnet' when UPPER(category_evenement) like '%INTER%' then 'international' when UPPER(category_evenement) like '%ROAMING%' then 'roaming' else 'onnet' end, OPERATEUR, DESTINATION, BALANCE_IMPACTEE, univers, TRIM(CANAL), date_jour ")

      val df_parc_usage_union = df_parc_ftth.select("msisdn_parc", "univers").filter(df_parc_ftth("statut_technique") === "Actif")
        .union(df_parc_adsl.select("msisdn_parc", "univers").filter(df_parc_adsl("statut_technique") === "Actif"))
        .union(df_parc_tdd.select("msisdn_parc", "univers").filter(df_parc_tdd("statut_technique") === "Actif"))
        .union(df_parc_fdd.select("msisdn_parc", "univers").filter(df_parc_fdd("statut_technique") === "Actif"))
        .union(df_parc_mobile.select("msisdn_parc", "univers").filter(df_parc_mobile("statut_technique") === "Actif"))
        .union(df_parc_fixe.select("msisdn_parc", "univers").filter(df_parc_fixe("statut_technique") === "Actif"))

      val df_parc_usage = df_parc_usage_union
        .groupBy("msisdn_parc")
        .agg(collect_set("univers").as("univers"))
        .withColumn("univers", concat_ws("_", col("univers")))

      // ajout de l'univers
      val df_usage_simple_mobile_univers = df_usage_simple_mobile.join(df_parc_usage, df_usage_simple_mobile("msisdn") === df_parc_usage("msisdn_parc"), "left")
        .withColumn("univers", when(df_usage_simple_mobile("msisdn") === df_parc_usage("msisdn_parc"), df_parc_usage("univers")).otherwise(df_usage_simple_mobile("univers_first")))

      // union des deux sources d'usage GAIA_TRAFIC_FIXE_POSTPAID et TERADATA_USAGE_SIMPLE
      val df_usage_union = df_fixe_postpaid.union(df_usage_simple_mobile_univers.select("msisdn", "usage_voix_sortant", "usage_sms_sortant", "usage_data", "type", "operateur", "destination", "type_de_compte", "univers", "canal", "date_jour"))

      // ajout du master_id
      val df_usage_results = df_usage_union.join(df_rcu, df_usage_union("MSISDN") === df_rcu("msisdn"), "left")
        .select(df_rcu("master_id"), df_usage_union("MSISDN"), df_usage_union("usage_voix_sortant"), df_usage_union("usage_sms_sortant"), df_usage_union("usage_data"), df_usage_union("type"), df_usage_union("operateur"), df_usage_union("destination"), df_usage_union("type_de_compte"), df_usage_union("univers"), df_usage_union("canal"), df_usage_union("date_jour"))

      // insertion dans la datamart vision_dm_usage
      //df_usage_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_usage").save()
      df_usage_results.coalesce(1).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_usage")
      //df_usage_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_usage")
    }




    else if (args(1) == "vision_dm_consommation") {
      // 11 **************************************************** datamart consommation **************************************************** usage fixe / usage mobile / 6 parc / rcu
      // chargement des données de datalake GAIA_TRAFIC_FIXE_POSTPAID
      val df_conso_postpaid = spark.sql("SELECT NUMERO_APPELANT AS MSISDN  , SUM(NVL(MONTANT_COM,0)) AS montant_appel , 0 AS montant_sms , case when type_appel in ( 'Mobiles Orange','Local') then 'onnet' when type_appel = 'International' then 'international' else 'offnet' end as type, case  when SUBSTR(nd_appele, 1,2 ) = '11' then 'MOOV' when SUBSTR(nd_appele, 1,2 ) = '15' then 'MTN' else 'OCI' end as operateur, 'NULL' as destination, 'NULL' as type_de_compte , 'FIXE_POSTPAID' as univers, 'VOIX' as canal, date_jour FROM vision_uc1_datalake.GAIA_TRAFIC_FIXE_POSTPAID WHERE date_jour = '" + args(0) + "' GROUP BY NUMERO_APPELANT, case when type_appel in ( 'Mobiles Orange','Local') then 'onnet' when type_appel = 'International' then 'international' else 'offnet' end, case when SUBSTR(nd_appele, 1,2 ) = '11' then 'MOOV' when SUBSTR(nd_appele, 1,2 ) = '15' then 'MTN' else 'OCI' end, date_jour ")

      // chargement des données de datalake TERADATA_USAGE_SIMPLE
      val df_conso_simple_mobile = spark.sql("SELECT CASE WHEN LENGTH(CALLING_NBR) = 13 AND SUBSTR(CALLING_NBR,1,3) = '225' THEN SUBSTR(CALLING_NBR,-10) WHEN LENGTH(CALLING_NBR) = 18 AND SUBSTR(CALLING_NBR,1,8) = 'tel:+225' THEN SUBSTR(CALLING_NBR,-10) ELSE CALLING_NBR END AS MSISDN  , SUM(case when UPPER(canal) like '%VOIX%' then NVL(CONSO_MB,0)+NVL(CONSO_SHCRED,0)+NVL(CONSO_CRED,0) else 0 end)  AS montant_appel , SUM(case when UPPER(CATEGORY_EVENEMENT) like '%SMS%' then NVL(CONSO_MB,0)+NVL(CONSO_SHCRED,0)+NVL(CONSO_CRED,0) else 0 end)  AS montant_sms ,  case when UPPER(category_evenement) like '%OFFNET%' then 'offnet' when UPPER(category_evenement) like '%INTER%' then 'international' when UPPER(category_evenement) like '%ROAMING%' then 'roaming' else 'onnet' end as type, OPERATEUR as operateur, DESTINATION as destination, BALANCE_IMPACTEE as type_de_compte , UNIVERS as univers_first, TRIM(CANAL) as canal, date_jour FROM vision_uc1_datalake.TERADATA_USAGE_SIMPLE WHERE date_jour = '" + args(0) + "' AND TRIM(univers)='MOBILE' GROUP BY CASE WHEN LENGTH(CALLING_NBR) = 13 AND SUBSTR(CALLING_NBR,1,3) = '225' THEN SUBSTR(CALLING_NBR,-10) WHEN LENGTH(CALLING_NBR) = 18 AND SUBSTR(CALLING_NBR,1,8) = 'tel:+225' THEN SUBSTR(CALLING_NBR,-10) ELSE CALLING_NBR END, case when UPPER(category_evenement) like '%OFFNET%' then 'offnet' when UPPER(category_evenement) like '%INTER%' then 'international' when UPPER(category_evenement) like '%ROAMING%' then 'roaming' else 'onnet' end, OPERATEUR, DESTINATION, BALANCE_IMPACTEE, UNIVERS, TRIM(CANAL), date_jour ")

      val df_parc_conso_union = df_parc_ftth.select("msisdn_parc", "univers")
        .union(df_parc_adsl.select("msisdn_parc", "univers"))
        .union(df_parc_tdd.select("msisdn_parc", "univers"))
        .union(df_parc_fdd.select("msisdn_parc", "univers"))
        .union(df_parc_mobile.select("msisdn_parc", "univers"))
        .union(df_parc_fixe.select("msisdn_parc", "univers"))

      val df_parc_conso = df_parc_conso_union
        .groupBy("msisdn_parc")
        .agg(collect_set("univers").as("univers"))
        .withColumn("univers", concat_ws("_", col("univers")))

      // ajout de l'univers
      val df_usage_conso_mobile_univers = df_conso_simple_mobile.join(df_parc_conso, df_conso_simple_mobile("msisdn") === df_parc_conso("msisdn_parc"), "left")
        .withColumn("univers", when(df_conso_simple_mobile("msisdn") === df_parc_conso("msisdn_parc"), df_parc_conso("univers")).otherwise(df_conso_simple_mobile("univers_first")))

      // union des deux sources d'usage GAIA_TRAFIC_FIXE_POSTPAID et TERADATA_USAGE_SIMPLE
      val df_conso_union = df_conso_postpaid.union(df_usage_conso_mobile_univers.select("msisdn", "montant_appel", "montant_sms", "type", "operateur", "destination", "type_de_compte", "univers", "canal", "date_jour"))

      // ajout du master_id
      val df_conso_results = df_conso_union.join(df_rcu, df_conso_union("MSISDN") === df_rcu("msisdn"), "left")
        .select(df_rcu("master_id"), df_conso_union("MSISDN"), df_conso_union("montant_appel"), df_conso_union("montant_sms"), df_conso_union("type"), df_conso_union("operateur"), df_conso_union("destination"), df_conso_union("type_de_compte"), df_conso_union("univers"), df_conso_union("canal"), df_conso_union("date_jour"))

      // insertion dans la datamart vision_dm_consommation
      //df_conso_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_consommation").save()
      df_conso_results.coalesce(1).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_consommation")
      //df_conso_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_consommation")

    }




    else if (args(1) == "vision_dm_socio_pro_geolocalisation") {
      // 12 **************************************************** datamart sociopro et géo **************************************************** 1 rcu / idbase/ 6 parc / gelooc / zone commer
      // Meilleur num partition = 5
      // chargement des données de RCU
      val df_rcu_socio = spark.sql(" SELECT master_id, MSISDN ,  nom_prenom , date_naissance, date_creation_mid_rcu,  date_jour FROM  (    SELECT master_id , MSISDN ,  nom_prenom , date_naissance, date_creation_mid_rcu, ROW_NUMBER() OVER (PARTITION BY MSISDN ORDER BY date_creation_mid_rcu DESC) AS row_num , date_jour   FROM   ( SELECT master_id , CASE WHEN LENGTH(TRIM(phone_num)) = 8 THEN CASE WHEN LIBELLE_TYPESERVICE = 'FIXE' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'WIMAX' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'MOBILE' THEN CASE WHEN SUBSTR(TRIM(phone_num),2,1) IN ('0','1','2','3') THEN CONCAT('01',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('4','5','6') THEN CONCAT('05',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('7','8','9') THEN CONCAT('07',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'AVISO' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'ORANGE MONEY' THEN CASE WHEN SUBSTR(TRIM(phone_num),2,1) IN ('0','1','2','3') THEN CONCAT('01',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('4','5','6') THEN CONCAT('05',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('7','8','9') THEN CONCAT('07',TRIM(phone_num)) ELSE TRIM(phone_num) END ELSE TRIM(phone_num) END ELSE TRIM(phone_num) END AS MSISDN  ,      CONCAT( UPPER(nom),' ',LOWER(prenom) )  as nom_prenom,       CASE  when LENGTH(date_naissance) = 8 THEN TO_DATE ( from_unixtime(unix_timestamp(date_naissance, 'dd/MM/yy')) ) when LENGTH(date_naissance) = 10 AND date_naissance rlike '/'   THEN  TO_DATE (  from_unixtime(unix_timestamp(date_naissance, 'dd/MM/yyyy'))  ) when LENGTH(date_naissance) = 10 AND date_naissance rlike '-'   THEN  TO_DATE (  from_unixtime(unix_timestamp(date_naissance, 'dd-MM-yyyy'))  ) when LENGTH(date_naissance) = 9 THEN TO_DATE (  from_unixtime(unix_timestamp(date_naissance , 'dd-MM-yyyy'))  ) when LENGTH(date_naissance) = 21 THEN TO_DATE (date_naissance) else date_naissance end as date_naissance, date_creation_mid as date_creation_mid_rcu ,  to_date('"+args(0)+"') as date_jour FROM vision_uc1_datalake.rcu where date_jour = (SELECT MAX(date_jour) FROM vision_uc1_datalake.rcu WHERE date_jour <= '"+args(0)+"' )  ) first_req   ) second_req  WHERE row_num = 1 ")

      // chargement des données IDBASE
      val df_idbase_socio = spark.sql(" SELECT msisdn_idbase, profession, quartier_idbase, ville_idbase, commune_idbase, DATE_IDENTIFICATION AS DATE_IDENTIFICATION_IDBASE  FROM ( SELECT numero AS  msisdn_idbase   , profession AS profession  , quartier  AS quartier_idbase  , ville   AS ville_idbase, LOWER(TRIM(commune))   as commune_idbase  , DATE_IDENTIFICATION,  ROW_NUMBER() OVER (PARTITION BY numero ORDER BY DATE_IDENTIFICATION DESC )  AS row_num FROM    vision_uc1_datalake.idbase where date_jour <= '" + args(0) + "' ) tt where row_num = 1 ")

      val df_parc_socio_union = df_parc_ftth.select("msisdn_parc", "email_parc", "ville_parc", "commune_parc", "quartier_parc")
        .union(df_parc_fixe.select("msisdn_parc", "email_parc", "ville_parc", "commune_parc", "quartier_parc"))
        .union(df_parc_adsl.select("msisdn_parc", "email_parc", "ville_parc", "commune_parc", "quartier_parc"))
        .union(df_parc_tdd.select("msisdn_parc", "email_parc", "ville_parc", "commune_parc", "quartier_parc"))
        .union(df_parc_fdd.select("msisdn_parc", "email_parc", "ville_parc", "commune_parc", "quartier_parc"))
        .union(df_parc_mobile.select("msisdn_parc", "email_parc", "ville_parc", "commune_parc", "quartier_parc"))
        .distinct()

      val df_parc_socio = df_parc_socio_union.groupBy(df_parc_socio_union("msisdn_parc"))
        .agg(collect_set("email_parc").as("email_parc"), collect_set("ville_parc").as("ville_parc"), collect_set("commune_parc").as("commune_parc"), collect_set("quartier_parc").as("quartier_parc"))
        .withColumn("email_parc", concat_ws(",", col("email_parc")))
        .withColumn("ville_parc", concat_ws(",", col("ville_parc")))
        .withColumn("commune_parc", concat_ws(",", col("commune_parc")))
        .withColumn("quartier_parc", concat_ws(",", col("quartier_parc")))

      // chargement des données géolocalisation
      val df_mobile_geoloc = spark.sql("SELECT SUBSTR(msisdn,-10) as msisdn_geoloc, region, cell_id, nom_site from datalab.mobile_geoloc where date_jour = (SELECT MAX(date_jour) FROM datalab.mobile_geoloc WHERE date_jour <= '"+args(0)+"' ) ").dropDuplicates("msisdn_geoloc")

      // Extraction du zone commerciale
      val df_geo = spark.sql("select SUBSTR(zc.compte_stk,-10) as msisdn_zone_commercial, NOM_PARTENAIRE as zone_commercial from  vision_uc1_datalake.TERADATA_E_RECHARGE tr left join vision_uc1_datalake.dwocit_zone_commercial_e_rechrgement zc ON  tr.COMPTE_STK=zc.COMPTE_STK WHERE tr.date_jour = '" + args(0) + "' AND zc.date_jour = '" + args(0) + "' ").dropDuplicates("msisdn_zone_commercial")

      // jointure des differentes Datamart avec RCU
      val df_socio_geo_rcu_idbase = df_rcu_socio
        .join(df_idbase_socio, df_rcu_socio("msisdn") === df_idbase_socio("msisdn_idbase"), "LEFT")
        .join(df_parc_socio, df_rcu_socio("msisdn") === df_parc_socio("msisdn_parc"), "LEFT")
        .join(df_mobile_geoloc, df_rcu_socio("msisdn") === df_mobile_geoloc("msisdn_geoloc"), "LEFT")
        .join(df_geo, df_rcu_socio("msisdn") === df_geo("msisdn_zone_commercial"), "LEFT")
        .select(df_rcu_socio("master_id"), df_rcu_socio("msisdn"), df_rcu_socio("nom_prenom"), df_rcu_socio("date_naissance"), df_rcu_socio("date_creation_mid_rcu"), df_idbase_socio("profession"), df_idbase_socio("quartier_idbase"), df_idbase_socio("ville_idbase"), df_idbase_socio("commune_idbase"), df_idbase_socio("DATE_IDENTIFICATION_IDBASE"), df_parc_socio("email_parc"), df_parc_socio("ville_parc"), df_parc_socio("commune_parc"), df_parc_socio("quartier_parc"), df_mobile_geoloc("region"), df_mobile_geoloc("cell_id"), df_mobile_geoloc("nom_site"), df_geo("zone_commercial"), df_rcu_socio("date_jour"))

      // insertion dans la datamart vision_dm_socio_geo
      //df_socio_geo_result.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_socio_pro_geolocalisation").save()
      df_socio_geo_rcu_idbase.write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_socio_pro_geolocalisation")
      //df_socio_geo_result.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_socio_pro_geolocalisation")
    }



    else if (args(1) == "vision_dm_offre") {
      // 13 *****************************************************DM offre ******************************************* vbm + 6 parc et rcu
      // chargement des données des VBM 100% des MSISDN sont à 13 chiffres qui commencent par 225 donc on prends les 10 premiers chiffres // pas de MSISDN redondance
      val df_vbm = spark.sql(" select SUBSTR(msisdn,-10) as msisdn_vbm, CASE WHEN statut_activite=0 THEN 'Désactivé' ELSE 'Actif' END as statut_business from vision_uc1_datalake.vbm where date_jour =   (SELECT MAX(date_jour) FROM vision_uc1_datalake.vbm WHERE date_jour <= '"+args(0)+"' )  ")
      // MOBILE 99% des msisdn sont réglé à 10 chiffres
      val df_offre_parc_mobile = df_parc_mobile.select("msisdn_parc", "nom_offre", "identifiant_offre", "date_aquisation", "type_offre", "univers", "categorie_client", "statut_technique", "date_jour")
      //join mobile
      val df_join_mobile = df_offre_parc_mobile.join(df_vbm, df_offre_parc_mobile("msisdn_parc") === df_vbm("msisdn_vbm"), "left")
        .select(df_offre_parc_mobile("msisdn_parc"), df_offre_parc_mobile("nom_offre"), df_offre_parc_mobile("identifiant_offre"), df_offre_parc_mobile("date_aquisation"), df_offre_parc_mobile("type_offre"), df_offre_parc_mobile("univers"), df_offre_parc_mobile("categorie_client"), df_offre_parc_mobile("statut_technique"), df_vbm("statut_business"), df_offre_parc_mobile("date_jour"))

      // chargement des données parc fixe et 99% des MSISDN sont à 10 chiffres
      val df_offre_parc_fixe = df_parc_fixe.select("msisdn_parc", "nom_offre", "identifiant_offre", "date_aquisation", "type_offre", "univers", "categorie_client", "statut_technique", "statut_business", "date_jour")

      //Tous les factures dérniers avec max date_emission et customer_code distinct pour voir les dérniers statuts des clients ADSL et FTTH
      val df_last_facture = spark.sql(" SELECT customer_code, date_emission FROM (select TRIM(customer_code) AS customer_code , date_emission , ROW_NUMBER() OVER (PARTITION BY TRIM(customer_code) ORDER BY date_emission DESC) AS row_num  from vision_uc1_datalake.bscs_encaissement_facture_mobile_broadband WHERE TRIM(univers) = 'INTERNET' AND  date_jour <= '" + args(0) + "'  ) first_req WHERE row_num=1 ")

      val format = new java.text.SimpleDateFormat("yyyy-MM-dd")
      val date_rattrapage = format.parse(args(0))
      val date_today = new Date()
      val ms = date_today.getTime - date_rattrapage.getTime
      val days = ms / (60 * 60 * 24 * 1000)

      //chargement des données de la parc adsl 99% des MSISDN sont à 10 chiffres
      val df_offre_parc_adsl = df_parc_adsl.select("msisdn_parc", "custcode", "nom_offre", "identifiant_offre", "date_aquisation", "type_offre", "univers", "categorie_client", "statut_technique", "date_jour")
      val df_join_adsl = df_offre_parc_adsl.join(df_last_facture, df_offre_parc_adsl("custcode") === df_last_facture("customer_code"), "LEFT")
        .select(df_offre_parc_adsl("msisdn_parc"), df_offre_parc_adsl("nom_offre"), df_offre_parc_adsl("identifiant_offre"), df_offre_parc_adsl("date_aquisation"), df_offre_parc_adsl("type_offre"), df_offre_parc_adsl("univers"), df_offre_parc_adsl("categorie_client"), df_offre_parc_adsl("statut_technique"), df_last_facture("date_emission"), df_offre_parc_adsl("date_jour"))
        .withColumn("statut_business", when(col("date_aquisation") > date_sub(current_date, 31 + days.toInt), "Actif")
          .when(col("date_emission") > date_sub(current_date, 31 + days.toInt), "Actif")
          .when(col("date_emission") > date_sub(current_date, 91 + days.toInt) and col("date_emission") < date_sub(current_date, 31 + days.toInt), "Suspendu")
          .when(col("date_emission") < date_sub(current_date, 91 + days.toInt), "Désactivé")
          .otherwise("null"))
        .select(col("msisdn_parc"), col("nom_offre"), col("identifiant_offre"), col("date_aquisation"), col("type_offre"), col("univers"), col("categorie_client"), col("statut_technique"), col("statut_business"), col("date_jour"))

      //chargement des données de la parc FTTH 99% des MSISDN sont à 10 chiffres
      val df_offre_parc_ftth = df_parc_ftth.select("msisdn_parc", "custcode", "nom_offre", "identifiant_offre", "date_aquisation", "type_offre", "univers", "categorie_client", "statut_technique", "date_jour")
      val df_join_ftth = df_offre_parc_ftth.join(df_last_facture, df_offre_parc_ftth("custcode") === df_last_facture("customer_code"), "left")
        .select(df_offre_parc_ftth("msisdn_parc"), df_offre_parc_ftth("nom_offre"), df_offre_parc_ftth("identifiant_offre"), df_offre_parc_ftth("date_aquisation"), df_offre_parc_ftth("type_offre"), df_offre_parc_ftth("univers"), df_offre_parc_ftth("categorie_client"), df_offre_parc_ftth("statut_technique"), df_last_facture("date_emission"), df_offre_parc_ftth("date_jour"))
        .withColumn("statut_business", when(col("date_aquisation") > date_sub(current_date, 31 + days.toInt), "Actif")
          .when(col("date_emission") > date_sub(current_date, 31 + days.toInt), "Actif")
          .when(col("date_emission") > date_sub(current_date, 91 + days.toInt) and col("date_emission") < date_sub(current_date, 31 + days.toInt), "Suspendu")
          .when(col("date_emission") < date_sub(current_date, 91 + days.toInt), "Désactivé")
          .otherwise("null"))
        .select(col("msisdn_parc"), col("nom_offre"), col("identifiant_offre"), col("date_aquisation"), col("type_offre"), col("univers"), col("categorie_client"), col("statut_technique"), col("statut_business"), col("date_jour"))

      //dataframe teradata_rechargement_in et gaia_trafic_fixe_postpaid et teradata_usage_simple les MSISDN pour la durée des derniers 90 jours
      val df_last_interaction_rechargement_in = spark.sql("SELECT SUBSTR(MSISDN,-10) as ms from vision_uc1_datalake.teradata_rechargement_in where date_jour between date_sub('" + args(0) + "',90) and '" + args(0) + "'").distinct()
      val df_last_interaction_trafic_fixe = spark.sql("SELECT NUMERO_APPELANT as ms from vision_uc1_datalake.gaia_trafic_fixe_postpaid where date_jour between date_sub('" + args(0) + "',90) and '" + args(0) + "'").distinct()
      val df_last_interaction_usage_simple = spark.sql("SELECT CASE WHEN LENGTH(CALLING_NBR) = 13 AND SUBSTR(CALLING_NBR,1,3) = '225' THEN SUBSTR(CALLING_NBR,-10) WHEN LENGTH(CALLING_NBR) = 18 AND SUBSTR(CALLING_NBR,1,8) = 'tel:+225' THEN SUBSTR(CALLING_NBR,-10) ELSE CALLING_NBR END AS ms from vision_uc1_datalake.teradata_usage_simple where date_jour between date_sub('" + args(0) + "',90) and '" + args(0) + "'").distinct()
      val df_last_interaction = df_last_interaction_rechargement_in.union(df_last_interaction_trafic_fixe).union(df_last_interaction_usage_simple).distinct()

      //TDD
      val df_offre_parc_tdd = df_parc_tdd.select("msisdn_parc", "Nom_offre", "identifiant_offre", "date_aquisation", "type_offre", "univers", "categorie_client", "statut_technique", "date_jour")
      val df_join_tdd = df_offre_parc_tdd.join(df_last_interaction, df_offre_parc_tdd("msisdn_parc") === df_last_interaction("ms"), "left")
        .withColumn("statut_business", when(col("msisdn_parc") === col("ms"), "Actif").otherwise("Désactivé"))
        .select(col("msisdn_parc"), col("nom_offre"), col("identifiant_offre"), col("date_aquisation"), col("type_offre"), col("univers"), col("categorie_client"), col("statut_technique"), col("statut_business"), col("date_jour"))

      //fdd
      val df_offre_parc_fdd = df_parc_fdd.select("msisdn_parc", "Nom_offre", "identifiant_offre", "date_aquisation", "type_offre", "univers", "categorie_client", "statut_technique", "date_jour")
      val df_join_fdd = df_offre_parc_fdd.join(df_last_interaction, df_offre_parc_fdd("msisdn_parc") === df_last_interaction("ms"), "left")
        .withColumn("statut_business", when(df_offre_parc_fdd("msisdn_parc") === df_last_interaction("ms"), "Actif").otherwise("Désactivé"))
        .select(col("msisdn_parc"), col("nom_offre"), col("identifiant_offre"), col("date_aquisation"), col("type_offre"), col("univers"), col("categorie_client"), col("statut_technique"), col("statut_business"), col("date_jour"))

      // union de toute les requetes parc
      val df_union_parc = df_join_mobile
        .union(df_offre_parc_fixe)
        .union(df_join_adsl)
        .union(df_join_ftth)
        .union(df_join_tdd)
        .union(df_join_fdd)

      // ajout de master ID
      val df_offre_final = df_union_parc.join(df_rcu, df_union_parc("msisdn_parc") === df_rcu("msisdn"), "left")
        .select(df_rcu("master_id"), df_union_parc("msisdn_parc"), df_union_parc("nom_offre"), df_union_parc("identifiant_offre"), df_union_parc("date_aquisation"), df_union_parc("type_offre"), df_union_parc("univers"), df_union_parc("categorie_client"), df_union_parc("statut_technique"), df_union_parc("statut_business"), df_union_parc("date_jour"))
        .withColumnRenamed("msisdn_parc", "msisdn")

      // insertion dans la datamart vision_dm_offre
      //df_offre_final.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_offre").save()
      df_offre_final.coalesce(4).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_offre")
      //df_offre_final.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_offre")
    }




    else if (args(1) == "vision_dm_facturation") {
      // 15 *****************************************************DM facturation ******************************************** 2 factures fixe et broad / rcu / 6 parc
      // chargement des données gaia_encaissment_facture_fixe
      val df_fixe_facture = spark.sql("select NUMERO_FACTURE, sum(NVL(MNT_TTC_FACTURE,0)) as MONTANT_TOTAL_FACTURE , sum(NVL(MNT_SOLDE_FACT,0))  as MONTANT_TOTAL_PAYE, DATE_EMISSION_FACTURE, DATE_ENCAISSEMENT AS DATE_PAYEMENT_FACTURE,  date_echeance_facture as date_echeance, MODE_ENCAISSEMENT as mode_payement, 'FIXE' as UNIVERS, categorie_client, TRIM(NCLI) as NCLI,  DATE_JOUR from vision_uc1_datalake.gaia_encaissment_facture_fixe WHERE date_jour = '" + args(0) + "' group by NUMERO_FACTURE, DATE_EMISSION_FACTURE, DATE_ECHEANCE_FACTURE, DATE_ENCAISSEMENT, date_echeance_facture, MODE_ENCAISSEMENT, categorie_client, TRIM(NCLI), DATE_JOUR")

      // calcul de couple master_id, custcode entre parc et rcu
      val df_parc_fixe_msisdn_ncli = df_parc_fixe.select("msisdn_parc", "NUMERO_CLIENT").distinct()

      // ajout du master_id
      val df_parc_fixe_master_id_ncli = df_parc_fixe_msisdn_ncli
        .join(df_rcu, df_parc_fixe_msisdn_ncli("msisdn_parc") === df_rcu("msisdn"), "LEFT")
        .select(df_rcu("master_id"), df_parc_fixe_msisdn_ncli("NUMERO_CLIENT"))
        .distinct()
        .dropDuplicates("NUMERO_CLIENT")

      // ajout du master_id
      val df_fixe_facture_master_id = df_fixe_facture
        .join(df_parc_fixe_master_id_ncli, df_fixe_facture("NCLI") === df_parc_fixe_master_id_ncli("NUMERO_CLIENT"), "LEFT")
        .select(df_parc_fixe_master_id_ncli("master_id"), df_fixe_facture("NUMERO_FACTURE"), df_fixe_facture("MONTANT_TOTAL_FACTURE"), df_fixe_facture("MONTANT_TOTAL_PAYE"), df_fixe_facture("DATE_EMISSION_FACTURE"), df_fixe_facture("DATE_PAYEMENT_FACTURE"), df_fixe_facture("date_echeance"), df_fixe_facture("mode_payement"), df_fixe_facture("UNIVERS"), df_fixe_facture("categorie_client"), df_fixe_facture("DATE_JOUR"))


      // chargement des données bscs_encaissement_facture_mobile_broadband
      val df_bscs_broadband_facture = spark.sql("select NUMERO_FACTURE, sum(NVL(Montant_facture,0)) as MONTANT_TOTAL_FACTURE, sum(NVL(MONTANT_REGLE_FACTURE,0)) as MONTANT_TOTAL_PAYE , date_emission AS DATE_EMISSION_FACTURE, DATE_PAIEMENT AS DATE_PAYEMENT_FACTURE,date_echeance , mode_paiement as mode_payement, UNIVERS, categorie as categorie_client, TRIM(customer_code) as customer_code,  DATE_JOUR from vision_uc1_datalake.bscs_encaissement_facture_mobile_broadband WHERE date_jour = '" + args(0) + "' group by NUMERO_FACTURE, date_emission, DATE_PAIEMENT,date_echeance, mode_paiement, UNIVERS, categorie, customer_code, DATE_JOUR")

      // ajout du master_id
      val df_bscs_broadband_facture_master_id = df_bscs_broadband_facture
        .join(df_union_parc_custcode_master_id, df_bscs_broadband_facture("customer_code") === df_union_parc_custcode_master_id("custcode"), "left")
        .select(df_union_parc_custcode_master_id("master_id"), df_bscs_broadband_facture("NUMERO_FACTURE"), df_bscs_broadband_facture("MONTANT_TOTAL_FACTURE"), df_bscs_broadband_facture("MONTANT_TOTAL_PAYE"), df_bscs_broadband_facture("DATE_EMISSION_FACTURE"), df_bscs_broadband_facture("DATE_PAYEMENT_FACTURE"), df_bscs_broadband_facture("date_echeance"), df_bscs_broadband_facture("mode_payement"), df_bscs_broadband_facture("UNIVERS"), df_bscs_broadband_facture("categorie_client"), df_bscs_broadband_facture("DATE_JOUR"))
        .distinct()

      val df_vision_dm_facturation = df_fixe_facture_master_id.union(df_bscs_broadband_facture_master_id)

      // insertion dans la datamart vision_dm_facturation
      //df_vision_dm_facturation.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_facturation").save()
      df_vision_dm_facturation.write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_facturation")
      //df_vision_dm_facturation.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_facturation")
    }





    else if (args(1) == "vision_dm_ca_fixe_postpaid") {
      // 16 ******************************************************************* calcul CA FIXE POSTPAID GAIA
      // chargement des factures fixes gaia par NCLI distinct et MNT_SOLDE_FACT
      val df_ca_fixe_facture = spark.sql("select sum(MNT_SOLDE_FACT) as MNT_SOLDE_FACT_FIXE , TRIM(NCLI) as NCLI from vision_uc1_datalake.gaia_encaissment_facture_fixe WHERE date_jour = '" + args(0) + "' group by TRIM(NCLI) ")

      // données de parc fixe : msisdn, compte_client, univers et type_offre et date_jour par MSISDN distinct
      val df_parc_ca_fixe = df_parc_fixe.select("msisdn_parc", "numero_client", "univers", "type_offre", "date_jour")

      // Ajout de master_id aux données de parc gaia fixe croisement par msisdn_parc unique et msisdn de rcu unique
      val df_parc_ca_fixe_master_id = df_parc_ca_fixe.join(df_rcu, df_parc_ca_fixe("msisdn_parc") === df_rcu("msisdn"), "LEFT")
        .select(df_rcu("master_id"), df_parc_ca_fixe("numero_client"), df_parc_ca_fixe("univers"), df_parc_ca_fixe("type_offre"), df_parc_ca_fixe("date_jour"))
        .distinct()
        .dropDuplicates("numero_client")

      // ajout des données de factures fixe gaia aux parc fixe
      val df_parc_ca_fixe_master_id_result = df_parc_ca_fixe_master_id.join(df_ca_fixe_facture, df_parc_ca_fixe_master_id("numero_client") === df_ca_fixe_facture("NCLI"), "LEFT")
        .select(df_parc_ca_fixe_master_id("master_id"), df_parc_ca_fixe_master_id("numero_client"), df_parc_ca_fixe_master_id("univers"), df_parc_ca_fixe_master_id("type_offre"), df_ca_fixe_facture("MNT_SOLDE_FACT_FIXE"), df_parc_ca_fixe_master_id("date_jour"))

      df_parc_ca_fixe_master_id_result.coalesce(1).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_ca_fixe_postpaid")
      //df_parc_ca_fixe_master_id_result.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_ca_fixe_postpaid")
    }





    else if (args(1) == "vision_dm_ca_mobile_prepaid") {
      // 17 ******************************************************************* calcul CA mobile prepaid

      // données des consommations de montant_appel et montant_sms des clients mobile prepaid par MSISDN distinct
      val df_conso_usage_simple_mobile_prepaid = spark.sql(" SELECT CASE WHEN LENGTH(CALLING_NBR) = 13 AND SUBSTR(CALLING_NBR,1,3) = '225' THEN SUBSTR(CALLING_NBR,-10) WHEN LENGTH(CALLING_NBR) = 18 AND SUBSTR(CALLING_NBR,1,8) = 'tel:+225' THEN SUBSTR(CALLING_NBR,-10) ELSE CALLING_NBR END AS MSISDN  , SUM(case when UPPER(canal) like '%VOIX%' then NVL(CONSO_MB,0)+NVL(CONSO_SHCRED,0)+NVL(CONSO_CRED,0) else 0 end)  AS montant_appel , SUM(case when UPPER(CATEGORY_EVENEMENT) like '%SMS%' then NVL(CONSO_MB,0)+NVL(CONSO_SHCRED,0)+NVL(CONSO_CRED,0) else 0 end)  AS montant_sms  , date_jour FROM vision_uc1_datalake.TERADATA_USAGE_SIMPLE WHERE date_jour = '" + args(0) + "' AND TRIM(type_client)='PREPAID' AND TRIM(univers)='MOBILE' GROUP BY CASE WHEN LENGTH(CALLING_NBR) = 13 AND SUBSTR(CALLING_NBR,1,3) = '225' THEN SUBSTR(CALLING_NBR,-10) WHEN LENGTH(CALLING_NBR) = 18 AND SUBSTR(CALLING_NBR,1,8) = 'tel:+225' THEN SUBSTR(CALLING_NBR,-10) ELSE CALLING_NBR END, date_jour ")


      // données de parc mobile prepaid : msisdn, compte_client, univers et type_offre et date_jour par MSISDN distinct
      val df_parc_ca_mobile_prepaid = df_parc_mobile.filter(df_parc_mobile("type_offre") === "PREPAID").select("msisdn_parc", "compte_client", "univers", "type_offre", "date_jour")

      // Ajout de montant_appel, montant_sms, montant_achat_pass, MONTANT_SVA et MONTANT_REMBOURSE aux données mobile prepaid
      val df_ca_mobile_prepaid = df_parc_ca_mobile_prepaid
        .join(df_conso_usage_simple_mobile_prepaid, df_parc_ca_mobile_prepaid("msisdn_parc") === df_conso_usage_simple_mobile_prepaid("msisdn"), "LEFT")
        .join(df_conso_achats_pass_mobile_prepaid_hybrid, df_parc_ca_mobile_prepaid("msisdn_parc") === df_conso_achats_pass_mobile_prepaid_hybrid("msisdn"), "LEFT")
        .join(df_conso_achats_sva_mobile_prepaid_hybrid, df_parc_ca_mobile_prepaid("msisdn_parc") === df_conso_achats_sva_mobile_prepaid_hybrid("msisdn"), "LEFT")
        .join(df_conso_credit_sos_mobile_prepaid_hybrid, df_parc_ca_mobile_prepaid("msisdn_parc") === df_conso_credit_sos_mobile_prepaid_hybrid("msisdn"), "LEFT")
        .select(df_parc_ca_mobile_prepaid("msisdn_parc"), df_parc_ca_mobile_prepaid("compte_client"), df_parc_ca_mobile_prepaid("univers"), df_parc_ca_mobile_prepaid("type_offre"), df_conso_usage_simple_mobile_prepaid("montant_appel"), df_conso_usage_simple_mobile_prepaid("montant_sms"), df_conso_achats_pass_mobile_prepaid_hybrid("montant_achat_pass"), df_conso_achats_sva_mobile_prepaid_hybrid("MONTANT_SVA"), df_conso_credit_sos_mobile_prepaid_hybrid("MONTANT_REMBOURSE"), df_parc_ca_mobile_prepaid("date_jour"))

      val df_ca_mobile_prepaid_master_id = df_ca_mobile_prepaid.join(df_rcu, df_ca_mobile_prepaid("msisdn_parc") === df_rcu("msisdn"), "LEFT")
        .select(df_rcu("master_id"), df_ca_mobile_prepaid("msisdn_parc"), df_ca_mobile_prepaid("compte_client"), df_ca_mobile_prepaid("univers"), df_ca_mobile_prepaid("type_offre"), df_ca_mobile_prepaid("montant_appel"), df_ca_mobile_prepaid("montant_sms"), df_ca_mobile_prepaid("montant_achat_pass"), df_ca_mobile_prepaid("MONTANT_SVA"), df_ca_mobile_prepaid("MONTANT_REMBOURSE"), df_ca_mobile_prepaid("date_jour"))
        .withColumnRenamed("msisdn_parc", "msisdn")

      df_ca_mobile_prepaid_master_id.coalesce(3).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_ca_mobile_prepaid")
      //df_ca_mobile_prepaid_master_id.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_ca_mobile_prepaid")
    }




    else if (args(1) == "vision_dm_ca_mobile_hybrid") {
      // 18 ******************************************************************* calcul CA mobile hybrid

      // données des consommations de montant_appel et montant_sms des clients mobile hybrid par MSISDN distinct
      val df_conso_usage_simple_mobile_hybride = spark.sql(" SELECT CASE WHEN LENGTH(CALLING_NBR) = 13 AND SUBSTR(CALLING_NBR,1,3) = '225' THEN SUBSTR(CALLING_NBR,-10) WHEN LENGTH(CALLING_NBR) = 18 AND SUBSTR(CALLING_NBR,1,8) = 'tel:+225' THEN SUBSTR(CALLING_NBR,-10) ELSE CALLING_NBR END AS MSISDN  , SUM(case when UPPER(canal) like '%VOIX%' then NVL(CONSO_MB,0)+NVL(CONSO_SHCRED,0)+NVL(CONSO_CRED,0) else 0 end)  AS montant_appel , SUM(case when UPPER(CATEGORY_EVENEMENT) like '%SMS%' then NVL(CONSO_MB,0)+NVL(CONSO_SHCRED,0)+NVL(CONSO_CRED,0) else 0 end)  AS montant_sms  , date_jour FROM vision_uc1_datalake.TERADATA_USAGE_SIMPLE WHERE date_jour = '" + args(0) + "' AND TRIM(type_client)='HYBRID' AND TRIM(univers)='MOBILE' GROUP BY CASE WHEN LENGTH(CALLING_NBR) = 13 AND SUBSTR(CALLING_NBR,1,3) = '225' THEN SUBSTR(CALLING_NBR,-10) WHEN LENGTH(CALLING_NBR) = 18 AND SUBSTR(CALLING_NBR,1,8) = 'tel:+225' THEN SUBSTR(CALLING_NBR,-10) ELSE CALLING_NBR END , date_jour ")

      // données de parc mobile hybrid : msisdn, compte_client, univers et type_offre et date_jour par MSISDN distinct
      val df_parc_ca_mobile_hybrid = df_parc_mobile.filter(df_parc_mobile("type_offre") === "HYBRIDE").select("msisdn_parc", "compte_client", "univers", "type_offre", "date_jour")

      // Ajout de montant_appel, montant_sms, montant_achat_pass, MONTANT_SVA et MONTANT_REMBOURSE aux données mobile hybrid
      val df_ca_mobile_hybrid = df_parc_ca_mobile_hybrid
        .join(df_conso_usage_simple_mobile_hybride, df_parc_ca_mobile_hybrid("msisdn_parc") === df_conso_usage_simple_mobile_hybride("msisdn"), "LEFT")
        .join(df_conso_achats_pass_mobile_prepaid_hybrid, df_parc_ca_mobile_hybrid("msisdn_parc") === df_conso_achats_pass_mobile_prepaid_hybrid("msisdn"), "LEFT")
        .join(df_conso_achats_sva_mobile_prepaid_hybrid, df_parc_ca_mobile_hybrid("msisdn_parc") === df_conso_achats_sva_mobile_prepaid_hybrid("msisdn"), "LEFT")
        .join(df_conso_credit_sos_mobile_prepaid_hybrid, df_parc_ca_mobile_hybrid("msisdn_parc") === df_conso_credit_sos_mobile_prepaid_hybrid("msisdn"), "LEFT")
        .select(df_parc_ca_mobile_hybrid("msisdn_parc"), df_parc_ca_mobile_hybrid("compte_client"), df_parc_ca_mobile_hybrid("univers"), df_parc_ca_mobile_hybrid("type_offre"), df_conso_usage_simple_mobile_hybride("montant_appel"), df_conso_usage_simple_mobile_hybride("montant_sms"), df_conso_achats_pass_mobile_prepaid_hybrid("montant_achat_pass"), df_conso_achats_sva_mobile_prepaid_hybrid("MONTANT_SVA"), df_conso_credit_sos_mobile_prepaid_hybrid("MONTANT_REMBOURSE"), df_parc_ca_mobile_hybrid("date_jour"))

      // Ajout de master_id
      val df_ca_mobile_hybrid_master_id = df_ca_mobile_hybrid.join(df_rcu, df_ca_mobile_hybrid("msisdn_parc") === df_rcu("msisdn"), "LEFT")
        .select(df_rcu("master_id"), df_ca_mobile_hybrid("msisdn_parc"), df_ca_mobile_hybrid("compte_client"), df_ca_mobile_hybrid("univers"), df_ca_mobile_hybrid("type_offre"), df_ca_mobile_hybrid("montant_appel"), df_ca_mobile_hybrid("montant_sms"), df_ca_mobile_hybrid("montant_achat_pass"), df_ca_mobile_hybrid("MONTANT_SVA"), df_ca_mobile_hybrid("MONTANT_REMBOURSE"), df_ca_mobile_hybrid("date_jour"))
        .withColumnRenamed("msisdn_parc", "msisdn")

      df_ca_mobile_hybrid_master_id.coalesce(1).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_ca_mobile_hybrid")
      //df_ca_mobile_hybrid_master_id.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_ca_mobile_hybrid")
    }



    else if (args(1) == "vision_dm_ca_mobile_postpaid") {
      // 19 ******************************************************************* calcul CA mobile postpaid
      // données des achat pass seulement pour mobile postpaid par MSISDN distinct
      val df_conso_achats_pass_mobile_postpaid = spark.sql("SELECT SUBSTR(ACC_NBR,-10) AS MSISDN ,  SUM(CA_ACHAT_PASS) AS montant_achat_pass  FROM vision_uc1_datalake.TERADATA_ACHAT_PASS  WHERE date_jour = '" + args(0) + "' AND canal IN (29,60,64,66) GROUP BY SUBSTR(ACC_NBR,-10) ")

      // données de parc mobile postpaid : msisdn, compte_client, univers et type_offre et date_jour par MSISDN distinct
      val df_parc_ca_mobile_postpaid = df_parc_mobile.filter(df_parc_mobile("type_offre") === "POSTPAID").select("msisdn_parc", "compte_client", "univers", "type_offre", "date_jour")

      // Ajout de montant_achat_pass aux données mobile postpaid par MSISDN distinct
      val df_ca_mobile_postpaid = df_parc_ca_mobile_postpaid
        .join(df_conso_achats_pass_mobile_postpaid, df_parc_ca_mobile_postpaid("msisdn_parc") === df_conso_achats_pass_mobile_postpaid("msisdn"), "LEFT")
        .select(df_parc_ca_mobile_postpaid("msisdn_parc"), df_parc_ca_mobile_postpaid("compte_client"), df_parc_ca_mobile_postpaid("univers"), df_parc_ca_mobile_postpaid("type_offre"), df_conso_achats_pass_mobile_postpaid("montant_achat_pass"), df_parc_ca_mobile_postpaid("date_jour"))

      // Ajout de master_id par MSISDN distinct, master_id, compte_client, univers, type_offre, montant_achat_pass, date_jour
      val df_ca_mobile_postpaid_master_id = df_ca_mobile_postpaid.join(df_rcu, df_ca_mobile_postpaid("msisdn_parc") === df_rcu("msisdn"), "LEFT")
        .select(df_rcu("master_id"), df_ca_mobile_postpaid("msisdn_parc"), df_parc_ca_mobile_postpaid("compte_client"), df_ca_mobile_postpaid("univers"), df_ca_mobile_postpaid("type_offre"), df_ca_mobile_postpaid("montant_achat_pass"), df_ca_mobile_postpaid("date_jour"))
        .withColumnRenamed("msisdn_parc", "msisdn")

      df_ca_mobile_postpaid_master_id.coalesce(1).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_ca_mobile_postpaid")
      //df_ca_mobile_postpaid_master_id.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_ca_mobile_postpaid")
      // 20 21 22 23 ******************************************************************* calcul CA ADSL, FTTH, TDD, FDD
    }



    else if (args(1) == "vision_dm_ca_adsl") {
      // parc adsl custcode distinct et univers
      val df_parc_ca_adsl = df_parc_adsl.select("msisdn_parc", "custcode", "univers", "date_jour")
      val df_parc_ca_adsl_master_id = df_parc_ca_adsl
        .join(df_rcu, df_parc_ca_adsl("msisdn_parc") === df_rcu("msisdn"), "LEFT")
        .select(df_rcu("master_id"), df_parc_ca_adsl("custcode"), df_parc_ca_adsl("univers"), df_parc_ca_adsl("date_jour"))
        .distinct()
        .dropDuplicates("custcode")
      // custcode distinct et ca_rechargement_ods pour ADSL WHERE TRIM(perimetre)='ADSL'
      val df_rechargement_ods_adsl = spark.sql("SELECT custcode, SUM(montant_facture) as ca_rechargement_ods FROM vision_uc1_datalake.dwocit_bub_rechargement_ods WHERE date_jour = '" + args(0) + "' AND TRIM(perimetre)='ADSL' GROUP BY custcode  ")
      // ajout de ca_rechargement_ods aux parc ADSL
      val df_parc_ca_adsl_master_id_rech_ods = df_parc_ca_adsl_master_id
        .join(df_rechargement_ods_adsl, df_parc_ca_adsl_master_id("custcode") === df_rechargement_ods_adsl("custcode"), "LEFT")
        .select(df_parc_ca_adsl_master_id("master_id"), df_parc_ca_adsl_master_id("custcode"), df_parc_ca_adsl_master_id("univers"), df_rechargement_ods_adsl("ca_rechargement_ods"), df_parc_ca_adsl_master_id("date_jour"))

      df_parc_ca_adsl_master_id_rech_ods.coalesce(1).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_ca_adsl")
      //df_parc_ca_adsl_master_id_rech_ods.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_ca_adsl")
    }




    else if (args(1) == "vision_dm_ca_ftth") {
      // parc ftth custcode distinct et univers
      val df_parc_ca_ftth = df_parc_ftth.select("msisdn_parc", "custcode", "univers", "date_jour")
      val df_parc_ca_ftth_master_id = df_parc_ca_ftth
        .join(df_rcu, df_parc_ftth("msisdn_parc") === df_rcu("msisdn"), "LEFT")
        .select(df_rcu("master_id"), df_parc_ftth("custcode"), df_parc_ftth("univers"), df_parc_ftth("date_jour"))
        .distinct()
        .dropDuplicates("custcode")
      // custcode distinct et ca_base_contrat pour FTTH WHERE TRIM(service)='Fibre'
      val df_base_contrat_ftth = spark.sql("SELECT custcode, SUM(montant_facture_ttc) as ca_base_contrat FROM vision_uc1_datalake.dwocit_bub_fixe_ods_base_cont WHERE date_jour = '" + args(0) + "' AND TRIM(service)='Fibre' GROUP BY custcode ")
      // ajout de ca_base_contrat aux parc FTTH
      val df_parc_ca_ftth_master_id_base_contrat = df_parc_ca_ftth_master_id
        .join(df_base_contrat_ftth, df_parc_ca_ftth_master_id("custcode") === df_base_contrat_ftth("custcode"), "LEFT")
        .select(df_parc_ca_ftth_master_id("master_id"), df_parc_ca_ftth_master_id("custcode"), df_parc_ca_ftth_master_id("univers"), df_base_contrat_ftth("ca_base_contrat"), df_parc_ca_ftth_master_id("date_jour"))

      df_parc_ca_ftth_master_id_base_contrat.coalesce(1).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_ca_ftth")
      //df_parc_ca_ftth_master_id_base_contrat.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_ca_ftth")
    }





    else if (args(1) == "vision_dm_ca_tdd") {
      // parc tdd MSISDN distinct et custcode et univers
      val df_parc_ca_tdd = df_parc_tdd.select("msisdn_parc", "custcode", "univers", "date_jour")
      // ajout de ca_e_recharge via zebra et rechargemet_om au parc TDD et groupé par custcode distinct
      val df_parc_ca_tdd_e_recharge_om = df_parc_ca_tdd
        .join(df_conso_e_recharge_zebra, df_parc_ca_tdd("msisdn_parc") === df_conso_e_recharge_zebra("msisdn"), "LEFT")
        .join(df_conso_rechargement_om, df_parc_ca_tdd("msisdn_parc") === df_conso_rechargement_om("msisdn"), "LEFT")
        .select(df_parc_ca_tdd("msisdn_parc"), df_parc_ca_tdd("custcode"), df_parc_ca_tdd("univers"), df_conso_e_recharge_zebra("ca_e_recharge"), df_conso_rechargement_om("rechargemet_om"), df_parc_ca_tdd("date_jour"))
        .groupBy("custcode", "univers", "date_jour")
        .agg(sum("ca_e_recharge").as("ca_e_recharge"), sum("rechargemet_om").as("rechargemet_om"))
        .select("custcode", "univers", "ca_e_recharge", "rechargemet_om", "date_jour")
      // custcode distinct et ca_base_contrat pour TDD WHERE TRIM(service)='Wimax'
      val df_base_contrat_tdd = spark.sql("SELECT custcode, SUM(montant_facture_ttc) as ca_base_contrat FROM vision_uc1_datalake.dwocit_bub_fixe_ods_base_cont WHERE date_jour = '" + args(0) + "' AND TRIM(service)='Wimax' GROUP BY custcode ")
      // custcode distinct et ca_rechargement_ods pour TDD WHERE TRIM(perimetre)='Wimax'
      val df_rechargement_ods_tdd = spark.sql("SELECT custcode, SUM(montant_facture) as ca_rechargement_ods FROM vision_uc1_datalake.dwocit_bub_rechargement_ods WHERE date_jour = '" + args(0) + "' AND TRIM(perimetre)='Wimax' GROUP BY custcode  ")
      //Ajout de ca_base_contrat et ca_rechargement_ods
      val df_parc_ca_tdd_e_recharge_om_base_ods = df_parc_ca_tdd_e_recharge_om
        .join(df_base_contrat_tdd, df_parc_ca_tdd_e_recharge_om("custcode") === df_base_contrat_tdd("custcode"), "LEFT")
        .join(df_rechargement_ods_tdd, df_parc_ca_tdd_e_recharge_om("custcode") === df_rechargement_ods_tdd("custcode"), "LEFT")
        .select(df_parc_ca_tdd_e_recharge_om("custcode"), df_parc_ca_tdd_e_recharge_om("univers"), df_parc_ca_tdd_e_recharge_om("ca_e_recharge"), df_parc_ca_tdd_e_recharge_om("rechargemet_om"), df_base_contrat_tdd("ca_base_contrat"), df_rechargement_ods_tdd("ca_rechargement_ods"), df_parc_ca_tdd_e_recharge_om("date_jour"))
      // Ajout de master_id
      val df_parc_ca_tdd_e_recharge_om_base_ods_master_id = df_parc_ca_tdd_e_recharge_om_base_ods
        .join(df_union_parc_custcode_master_id, df_parc_ca_tdd_e_recharge_om_base_ods("custcode") === df_union_parc_custcode_master_id("custcode"), "LEFT")
        .select(df_union_parc_custcode_master_id("master_id"), df_parc_ca_tdd_e_recharge_om_base_ods("custcode"), df_parc_ca_tdd_e_recharge_om_base_ods("univers"), df_parc_ca_tdd_e_recharge_om_base_ods("ca_e_recharge"), df_parc_ca_tdd_e_recharge_om_base_ods("rechargemet_om"), df_parc_ca_tdd_e_recharge_om_base_ods("ca_base_contrat"), df_parc_ca_tdd_e_recharge_om_base_ods("ca_rechargement_ods"), df_parc_ca_tdd_e_recharge_om_base_ods("date_jour"))

      df_parc_ca_tdd_e_recharge_om_base_ods_master_id.coalesce(1).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_ca_tdd")
      //df_parc_ca_tdd_e_recharge_om_base_ods_master_id.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_ca_tdd")
    }



    else if (args(1) == "vision_dm_ca_fdd") {
      //parc fdd MSISDN distinct et custcode et univers
      val df_parc_ca_fdd = df_parc_fdd.select("msisdn_parc", "custcode", "univers", "date_jour")
      // ajout de ca_e_recharge via zebra et rechargemet_om au parc FDD et groupé par custcode distinct
      val df_parc_ca_fdd_e_recharge_om = df_parc_ca_fdd
        .join(df_conso_e_recharge_zebra, df_parc_ca_fdd("msisdn_parc") === df_conso_e_recharge_zebra("msisdn"), "LEFT")
        .join(df_conso_rechargement_om, df_parc_ca_fdd("msisdn_parc") === df_conso_rechargement_om("msisdn"), "LEFT")
        .select(df_parc_ca_fdd("msisdn_parc"), df_parc_ca_fdd("custcode"), df_parc_ca_fdd("univers"), df_conso_e_recharge_zebra("ca_e_recharge"), df_conso_rechargement_om("rechargemet_om"), df_parc_ca_fdd("date_jour"))
        .groupBy("custcode", "univers", "date_jour")
        .agg(sum("ca_e_recharge").as("ca_e_recharge"), sum("rechargemet_om").as("rechargemet_om"))
        .select("custcode", "univers", "ca_e_recharge", "rechargemet_om", "date_jour")
      // custcode distinct et ca_rechargement_ods pour FDD WHERE TRIM(perimetre)='FDD'
      val df_rechargement_ods_fdd = spark.sql("SELECT custcode, SUM(montant_facture) as ca_rechargement_ods FROM vision_uc1_datalake.dwocit_bub_rechargement_ods WHERE date_jour = '" + args(0) + "' AND TRIM(perimetre)='FDD' GROUP BY custcode  ")
      //Ajout de ca_rechargement_ods
      val df_parc_ca_fdd_e_recharge_om_base_ods = df_parc_ca_fdd_e_recharge_om
        .join(df_rechargement_ods_fdd, df_parc_ca_fdd_e_recharge_om("custcode") === df_rechargement_ods_fdd("custcode"), "LEFT")
        .select(df_parc_ca_fdd_e_recharge_om("custcode"), df_parc_ca_fdd_e_recharge_om("univers"), df_parc_ca_fdd_e_recharge_om("ca_e_recharge"), df_parc_ca_fdd_e_recharge_om("rechargemet_om"), df_rechargement_ods_fdd("ca_rechargement_ods"), df_parc_ca_fdd_e_recharge_om("date_jour"))
      // Ajout de master_id
      val df_parc_ca_fdd_e_recharge_om_base_ods_master_id = df_parc_ca_fdd_e_recharge_om_base_ods
        .join(df_union_parc_custcode_master_id, df_parc_ca_fdd_e_recharge_om_base_ods("custcode") === df_union_parc_custcode_master_id("custcode"), "LEFT")
        .select(df_union_parc_custcode_master_id("master_id"), df_parc_ca_fdd_e_recharge_om_base_ods("custcode"), df_parc_ca_fdd_e_recharge_om_base_ods("univers"), df_parc_ca_fdd_e_recharge_om_base_ods("ca_e_recharge"), df_parc_ca_fdd_e_recharge_om_base_ods("rechargemet_om"), df_parc_ca_fdd_e_recharge_om_base_ods("ca_rechargement_ods"), df_parc_ca_fdd_e_recharge_om_base_ods("date_jour"))

      df_parc_ca_fdd_e_recharge_om_base_ods_master_id.coalesce(1).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_ca_fdd")
      //df_parc_ca_fdd_e_recharge_om_base_ods_master_id.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_ca_fdd")
    }


    else if (args(1) == "vision_dm_rechargement") {
      // 1 **************************************************** datamart rechargement **************************************************** rechargement et rcu
      // chargement des données de datalake rechargement_in // tous les numéros à 13 chiffres
      val df_rechargement = spark.sql ("SELECT SUM (montant) AS MONTANT_TOTAL_RECHARGEMENT, COUNT(*) AS NB_TOTAL_RECHARGEMENT , CANAL AS TYPE_PAYEMENT , SUBSTR(MSISDN,-10) AS MSISDN , DATE_JOUR FROM vision_uc1_datalake.teradata_rechargement_in WHERE date_jour = '"+args(0)+"' GROUP BY CANAL  , SUBSTR(MSISDN,-10) , DATE_JOUR ")
      // ajout des master_id
      val df_rechargement_results = df_rechargement.join(df_rcu, df_rechargement("MSISDN") === df_rcu("msisdn"),"left" )
        .select(df_rcu("master_id"), df_rechargement("MSISDN"), df_rechargement("MONTANT_TOTAL_RECHARGEMENT"), df_rechargement("NB_TOTAL_RECHARGEMENT"), df_rechargement("TYPE_PAYEMENT"), df_rechargement("date_jour"))
      // insertion dans la datamart vision_dm_rechargement
      //df_rechargement_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_rechargement").save()
      df_rechargement_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_rechargement")
      //df_rechargement_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_rechargement")
      //  spark.sql("ANALYZE TABLE vision_uc1_datamart.vision_dm_rechargement COMPUTE STATISTICS")
    }


    else if (args(1) == "vision_dm_exp_client_call_center") {
      // 2 **************************************************** datamart call center **************************************************** call center et rcu
      // chargement des données de datalake genesys1_call_center
      val df_call_center = spark.sql ("SELECT CASE WHEN LENGTH(num_appelant) = 13 AND SUBSTR(num_appelant,1,3) = '225' THEN SUBSTR(num_appelant,-10) WHEN LENGTH(num_appelant) = 14 AND SUBSTR(num_appelant,1,4) = '+225' THEN SUBSTR(num_appelant,-10) ELSE num_appelant END AS MSISDN  , count(case issue_appel when 'Completed' then 1 else null end) AS nb_appels_aboutis , SUM (  UNIX_TIMESTAMP(  TO_DATE (  from_unixtime(unix_timestamp(date_fin_appel , 'dd/MM/yyyy'))  )  || ' ' || heure_fin_appel  )     -     UNIX_TIMESTAMP(  TO_DATE (  from_unixtime(unix_timestamp(date_debut_appel , 'dd/MM/yyyy'))  )  || ' ' || heure_debut_appel   )   )  AS temps_passe_ivr , issue_appel AS status_appel, date_jour FROM vision_uc1_datalake.genesys1_call_center WHERE date_jour= '"+args(0)+"' GROUP BY CASE WHEN LENGTH(num_appelant) = 13 AND SUBSTR(num_appelant,1,3) = '225' THEN SUBSTR(num_appelant,-10) WHEN LENGTH(num_appelant) = 14 AND SUBSTR(num_appelant,1,4) = '+225' THEN SUBSTR(num_appelant,-10) ELSE num_appelant END, issue_appel, date_jour ")

      // chargement des données de datalake genesys1_call_center
      val df_call_center_reiteration = spark.sql ("SELECT CASE WHEN LENGTH(num_appelant) = 13 AND SUBSTR(num_appelant,1,3) = '225' THEN SUBSTR(num_appelant,-10) WHEN LENGTH(num_appelant) = 14 AND SUBSTR(num_appelant,1,4) = '+225' THEN SUBSTR(num_appelant,-10) ELSE num_appelant END AS MSISDN , count(*) AS  nb_reiteration FROM vision_uc1_datalake.genesys1_call_center WHERE date_jour = '"+args(0)+"' OR date_jour = date_sub('"+args(0)+"', 1) OR date_jour = date_sub('"+args(0)+"', 2)  GROUP BY CASE WHEN LENGTH(num_appelant) = 13 AND SUBSTR(num_appelant,1,3) = '225' THEN SUBSTR(num_appelant,-10) WHEN LENGTH(num_appelant) = 14 AND SUBSTR(num_appelant,1,4) = '+225' THEN SUBSTR(num_appelant,-10) ELSE num_appelant END ")

      // ajouter la colonne reiteration à la dataframe genesys1_call_center
      val df_call_center_all = df_call_center.join(df_call_center_reiteration, df_call_center("MSISDN") === df_call_center_reiteration("MSISDN"),"left")
        .select(df_call_center("MSISDN"), df_call_center("nb_appels_aboutis"), df_call_center("temps_passe_ivr"), df_call_center_reiteration("nb_reiteration"), df_call_center("status_appel"), df_call_center("date_jour"))

      // ajout du master_id
      val df_call_center_results = df_call_center_all.join(df_rcu, df_call_center_all("MSISDN") === df_rcu("msisdn"),"left" )
        .select(df_rcu("master_id"), df_call_center_all("MSISDN"), df_call_center_all("nb_appels_aboutis"), df_call_center_all("temps_passe_ivr"), df_call_center_all("nb_reiteration"), df_call_center_all("status_appel") , df_call_center_all("date_jour"))

      // insertion dans la datamart vision_dm_call_center
      //df_call_center_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_exp_client_call_center").save()
      df_call_center_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_exp_client_call_center")
      //df_call_center_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_exp_client_call_center")
      // spark.sql("ANALYZE TABLE vision_uc1_datamart.vision_dm_exp_client_call_center COMPUTE STATISTICS")
    }


    else if (args(1) == "vision_dm_exp_client_reclamation") {
      // 3 **************************************************** datamart reclamation jade **************************************************** reclamation et rcu
      // chargement des données de datalake jade_reclamations_fmio
      // à changer pour spark sql
      // val df_reclamation = spark.sql ("SELECT TRANSLATE(ligne_client,' ','') AS MSISDN  , count(ligne_client) AS nb_prise_contact, TRIM(type_interation) as canal , univers, motif , CASE WHEN AVG (  DAY( MODIFIER_LE -       CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END        )*86400  +  hour( MODIFIER_LE -       CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END       )*3600  +  minute( MODIFIER_LE -        CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END       )*60  +  second( MODIFIER_LE -       CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END        )   )  < 0  THEN AVG (  DAY( MODIFIER_LE - CREE_LE )*86400  +  hour( MODIFIER_LE - CREE_LE )*3600  +  minute( MODIFIER_LE - CREE_LE )*60  +  second( MODIFIER_LE - CREE_LE )   )  ELSE  AVG (  DAY( MODIFIER_LE -       CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END        )*86400  +  hour( MODIFIER_LE -       CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END       )*3600  +  minute( MODIFIER_LE -        CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END       )*60  +  second( MODIFIER_LE -       CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END        )   )  END AS DMT  , statut , date_jour FROM vision_uc1_datalake.jade_reclamations_fmio WHERE date_jour = date_sub(current_date,1) GROUP BY TRANSLATE(ligne_client,' ','') , type_interation, univers, motif, statut, date_jour ")
      val df_reclamation = spark.sql ("SELECT TRANSLATE(ligne_client,' ','') AS MSISDN  , count(*) AS nb_prise_contact, count(case when statut in ('Résolu','Actif') then 1 else null end) AS nb_prise_en_charge , TRIM(type_interation) as canal , univers, motif , CASE WHEN AVG ( unix_timestamp(MODIFIER_LE)       -    unix_timestamp(   CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END   )   )  < 0 THEN AVG (  unix_timestamp(MODIFIER_LE)   -   unix_timestamp(CREE_LE)   ) ELSE  AVG ( unix_timestamp(MODIFIER_LE)       -    unix_timestamp(   CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END  )   )  END AS DMT  , statut , CASE WHEN SUBSTR(categorie,1,2) ='IN' THEN 'demandeInfo' WHEN SUBSTR(categorie,1,2) ='OP' THEN 'operation' WHEN SUBSTR(categorie,1,2) ='RE' THEN 'reclamation' ELSE 'autre' END AS categorie , date_jour FROM vision_uc1_datalake.jade_reclamations_fmio WHERE date_jour = '"+args(0)+"' GROUP BY TRANSLATE(ligne_client,' ','') , type_interation, univers, motif, statut,  CASE WHEN SUBSTR(categorie,1,2) ='IN' THEN 'demandeInfo' WHEN SUBSTR(categorie,1,2) ='OP' THEN 'operation' WHEN SUBSTR(categorie,1,2) ='RE' THEN 'reclamation' ELSE 'autre' END , date_jour ")
      // ajout du master_id
      val df_reclamation_results = df_reclamation.join(df_rcu, df_reclamation("MSISDN") === df_rcu("msisdn"),"left" )
        .select(df_rcu("master_id"), df_reclamation("MSISDN"), df_reclamation("nb_prise_contact"), df_reclamation("nb_prise_en_charge"), df_reclamation("canal"), df_reclamation("univers") , df_reclamation("motif") , df_reclamation("dmt") , df_reclamation("statut") , df_reclamation("categorie")  , df_reclamation("date_jour"))

      // insertion dans la datamart vision_dm_reclamation
      //df_reclamation_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_exp_client_reclamation").save()
      df_reclamation_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_exp_client_reclamation")
      //df_reclamation_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_exp_client_reclamation")
    }



    else if (args(1) == "vision_dm_equipements") {
      // 4 **************************************************** datamart equipements **************************************************** equipement et rcu
      // chargement des données des datalakes teradata_equipement_1 et teradata_equipement_2
      // à changer pour spark sql
      val df_equipements = spark.sql ("SELECT  SUBSTR(E2.MSISDN,-10)   AS msisdn, E1.modele AS modele, E1.fabriquant AS fabriquant, E1.type_technologie AS type_technologie, E1.os AS os, E1.version_os AS version_os, E1.type_device AS type_device, E1.date_jour AS date_jour FROM vision_uc1_datalake.teradata_equipement_1 E1  LEFT JOIN vision_uc1_datalake.teradata_equipement_2 E2 ON E1.TAC=E2.TAC_ID WHERE E1.date_jour = '"+args(0)+"' AND E2.date_jour = '"+args(0)+"' ")
        .distinct()

      val df_usim_detection = spark.sql("SELECT SUBSTR(msisdn,-10)  AS msisdn_usim FROM vision_uc1_datalake.dwocit_usim_detection WHERE date_jour =  '"+args(0)+"'  ")
        .distinct()

      val df_equipements_usim = df_equipements
        .join(df_usim_detection,  df_equipements("msisdn") === df_usim_detection("msisdn_usim"),"LEFT")
        .withColumn("statut_usim", when(df_equipements("msisdn") === df_usim_detection("msisdn_usim"),"Y").otherwise("N"))
        .select(df_equipements("msisdn"), df_equipements("modele"), df_equipements("fabriquant"), df_equipements("type_technologie"), df_equipements("os"), df_equipements("version_os") , df_equipements("type_device") , col("statut_usim") , df_equipements("date_jour"))

      val df_equipement_results=df_equipements_usim
        .join(df_rcu, df_equipements_usim("msisdn")===df_rcu("msisdn"), "LEFT")
        .select(df_rcu("master_id"),df_equipements_usim("msisdn"),df_equipements_usim("modele"),df_equipements_usim("fabriquant"),df_equipements_usim("type_technologie"),df_equipements_usim("os"),df_equipements_usim("version_os"),df_equipements_usim("type_device"),df_equipements_usim("statut_usim"),df_equipements_usim("date_jour"))
      // insertion dans la datamart vision_datamart_equipements
      //df_equipements_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_equipements").save()
      df_equipement_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_equipements")
      //df_equipements_usim.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_equipements")
    }



    else if (args(1) == "vision_dm_conso_achats_bundles") {
      // 5 **************************************************** datamart conso_achats_bundles **************************************************** bundles et rcu
      // chargement des données de datalake TERADATA_ACHAT_PASS & file_liste_channel_id
      val df_conso_achats_bundles = spark.sql ("SELECT SUBSTR(ACC_NBR,-10) AS MSISDN , SUM(CA_ACHAT_PASS) AS montant_achat_pass , type_de_pass , canal, date_jour as date_jour FROM vision_uc1_datalake.TERADATA_ACHAT_PASS WHERE date_jour = '"+args(0)+"' GROUP BY SUBSTR(ACC_NBR,-10), type_de_pass , canal, date_jour ")
      val df_file_categorisation_bundles = spark.sql("SELECT type_pass, service_id FROM vision_uc1_datalake.file_categorisation_bundles WHERE date_jour = (SELECT MAX(date_jour) FROM vision_uc1_datalake.file_categorisation_bundles WHERE date_jour <= '"+args(0)+"' ) ")
      val df_file_liste_channel_id = spark.sql("SELECT contact_channel_id, contact_channel_name, libelle_commercial FROM vision_uc1_datalake.file_liste_channel_id WHERE date_jour = (SELECT MAX(date_jour) FROM vision_uc1_datalake.file_liste_channel_id WHERE date_jour <= '"+args(0)+"' ) ")

      val df_conso_achats_bundles_type=df_conso_achats_bundles
        .join(df_file_categorisation_bundles,  df_conso_achats_bundles("type_de_pass") === df_file_categorisation_bundles("type_pass"),"LEFT" )
        .select(df_conso_achats_bundles("MSISDN"),df_conso_achats_bundles("montant_achat_pass"),df_file_categorisation_bundles("type_pass"),df_conso_achats_bundles("canal"),df_conso_achats_bundles("date_jour"))

      val df_conso_achats_bundles_type_canal=df_conso_achats_bundles_type
        .join(df_file_liste_channel_id, df_conso_achats_bundles_type("canal") === df_file_liste_channel_id("contact_channel_id"),"LEFT" )
        .select(df_conso_achats_bundles_type("MSISDN"),df_conso_achats_bundles_type("montant_achat_pass"),df_conso_achats_bundles_type("type_pass"),df_file_liste_channel_id("contact_channel_name"),df_file_liste_channel_id("libelle_commercial"),df_conso_achats_bundles_type("date_jour"))
        .withColumnRenamed("type_pass","type_de_pass")
        .withColumnRenamed("contact_channel_name","canal")
        .withColumnRenamed("libelle_commercial","moyen_de_payement")

      // ajout du master_id
      val df_conso_achats_bundles_results = df_conso_achats_bundles_type_canal.join(df_rcu, df_conso_achats_bundles_type_canal("MSISDN") === df_rcu("msisdn"),"left" )
        .select(df_rcu("master_id"), df_conso_achats_bundles_type_canal("MSISDN"), df_conso_achats_bundles_type_canal("montant_achat_pass"), df_conso_achats_bundles_type_canal("type_de_pass"), df_conso_achats_bundles_type_canal("canal"), df_conso_achats_bundles_type_canal("moyen_de_payement")  , df_conso_achats_bundles_type_canal("date_jour") )

      // insertion dans la datamart vision_dm_conso_achats_bundles
      //df_conso_achats_bundles_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_conso_achats_bundles").save()
      df_conso_achats_bundles_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_conso_achats_bundles")
      //df_conso_achats_bundles_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_conso_achats_bundles")
    }



    else if (args(1) == "vision_dm_conso_transactions_data") {
      // 6 **************************************************** datamart conso_transactions_data **************************************************** conso network data et rcu

      // chargement des données de datalake teradata_social_network_data
      val df_conso_transactions_data = spark.sql ("SELECT SUBSTR(MSISDN,-10) AS MSISDN , SUM(data_MB) AS conso_transactions_data , TYPE_TRANSACTION , TYPE_ACTION, date_jour FROM vision_uc1_datalake.teradata_social_network_data WHERE date_jour = '"+args(0)+"' AND TYPE_DONNEES like '%VOLUME%' GROUP BY SUBSTR(MSISDN,-10), TYPE_TRANSACTION , TYPE_ACTION, date_jour ")

      // ajout du master_id
      val df_conso_transactions_data_results = df_conso_transactions_data.join(df_rcu, df_conso_transactions_data("MSISDN") === df_rcu("msisdn"),"left" )
        .select(df_rcu("master_id"), df_conso_transactions_data("MSISDN"), df_conso_transactions_data("conso_transactions_data"), df_conso_transactions_data("type_transaction"), df_conso_transactions_data("type_action"), df_conso_transactions_data("date_jour") )

      // insertion dans la datamart vision_dm_conso_transactions_data
      //df_conso_transactions_data_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_conso_transactions_data").save()
      df_conso_transactions_data_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_conso_transactions_data")
      //df_conso_transactions_data_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_conso_transactions_data")
    }



    else if (args(1) == "vision_dm_conso_credit_sos") {
      // 7 **************************************************** datamart conso_credit_sos **************************************************** credit sos et rcu
      // chargement des données de datalake teradata_sos_credit_data
      val df_conso_credit_sos = spark.sql (" SELECT SUBSTR(MSISDN,-10) AS MSISDN , SUM(MONTANT_CREDIT_INITIAL) AS MONTANT_CREDIT_INITIAL , SUM(MONTANT_REMBOURSE) AS MONTANT_REMBOURSE , SERVICE , CANAL, date_jour FROM vision_uc1_datalake.teradata_sos_credit_data WHERE date_jour = '"+args(0)+"' GROUP BY SUBSTR(MSISDN,-10), SERVICE , CANAL, date_jour ")

      // ajout du master_id
      val df_conso_credit_sos_results = df_conso_credit_sos.join(df_rcu, df_conso_credit_sos("MSISDN") === df_rcu("msisdn"),"left" )
        .select(df_rcu("master_id"), df_conso_credit_sos("MSISDN"), df_conso_credit_sos("montant_credit_initial"), df_conso_credit_sos("montant_rembourse"), df_conso_credit_sos("service"), df_conso_credit_sos("canal") , df_conso_credit_sos("date_jour") )

      // insertion dans la datamart vision_dm_conso_credit_sos
      //df_conso_credit_sos_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_conso_credit_sos").save()
      df_conso_credit_sos_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_conso_credit_sos")
      //df_conso_credit_sos_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_conso_credit_sos")
    }



    else if (args(1) == "vision_dm_conso_achats_sva") {
      // 8 **************************************************** datamart conso_achats_sva **************************************************** sva et rcu
      // chargement des données de datalake teradata_sva & dwocit_dim_premices_sva
      val df_conso_achats_sva = spark.sql ("SELECT CASE WHEN LENGTH(TRIM(SVA.compte_client)) = 13 AND SUBSTR(TRIM(SVA.compte_client),1,3) = '225' THEN SUBSTR(TRIM(SVA.compte_client),-10) WHEN LENGTH(TRIM(SVA.compte_client)) = 14 AND SUBSTR(TRIM(SVA.compte_client),1,3) = '225' AND SUBSTR(TRIM(SVA.compte_client),-1) = '.' THEN SUBSTR(TRIM(SVA.compte_client),4,10) ELSE TRIM(SVA.compte_client) END AS MSISDN  , SUM(SVA.MONTANT_SVA) AS MONTANT_SVA , TRIM(SVA.canal) as moyen_de_payement, TRIM(DPS.nom_sva) as nom_service, SVA.date_jour as date_jour FROM vision_uc1_datalake.teradata_sva SVA LEFT JOIN vision_uc1_datalake.dwocit_dim_premices_sva DPS ON TRIM(SVA.id_vas) = TRIM(DPS.vas_id) WHERE SVA.date_jour = '"+args(0)+"' AND DPS.date_jour = (SELECT MAX(date_jour) FROM vision_uc1_datalake.dwocit_dim_premices_sva WHERE date_jour <= '"+args(0)+"') GROUP BY CASE WHEN LENGTH(TRIM(SVA.compte_client)) = 13 AND SUBSTR(TRIM(SVA.compte_client),1,3) = '225' THEN SUBSTR(TRIM(SVA.compte_client),-10) WHEN LENGTH(TRIM(SVA.compte_client)) = 14 AND SUBSTR(TRIM(SVA.compte_client),1,3) = '225' AND SUBSTR(TRIM(SVA.compte_client),-1) = '.' THEN SUBSTR(TRIM(SVA.compte_client),4,10) ELSE TRIM(SVA.compte_client) END, TRIM(SVA.canal) , TRIM(DPS.nom_sva), SVA.date_jour ")

      // ajout du master_id
      val df_conso_achats_sva_results = df_conso_achats_sva.join(df_rcu, df_conso_achats_sva("MSISDN") === df_rcu("msisdn"),"left" )
        .select(df_rcu("master_id"), df_conso_achats_sva("MSISDN"), df_conso_achats_sva("montant_sva"), df_conso_achats_sva("moyen_de_payement"), df_conso_achats_sva("nom_service"), df_conso_achats_sva("date_jour") )

      // insertion dans la datamart vision_dm_conso_achats_sva
      //df_conso_achats_sva_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_conso_achats_sva").save()
      df_conso_achats_sva_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_conso_achats_sva")
      //df_conso_achats_sva_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_conso_achats_sva")
    }


    else if (args(1) == "vision_dm_conso_e_recharge") {
      // 9 **************************************************** datamart conso_erecharge **************************************************** e_recharge et rcu
      // chargement des données de datalake teradata_e_recharge
      val df_conso_teradata_e_recharge = spark.sql ("select SUBSTR(client_final,-10) as MSISDN , TYPE_TRANSACTION , SUM (TRANSFER_AMOUNT) AS ca_e_recharge , date_jour FROM vision_uc1_datalake.teradata_e_recharge WHERE date_jour= '"+args(0)+"' GROUP BY  SUBSTR(client_final,-10) , TYPE_TRANSACTION , date_jour  ")

      // ajout du master_id
      val df_conso_teradata_e_recharge_results = df_conso_teradata_e_recharge.join(df_rcu, df_conso_teradata_e_recharge("MSISDN") === df_rcu("msisdn"),"left" )
        .select(df_rcu("master_id"), df_conso_teradata_e_recharge("MSISDN"), df_conso_teradata_e_recharge("TYPE_TRANSACTION"), df_conso_teradata_e_recharge("ca_e_recharge"), df_conso_teradata_e_recharge("date_jour"))

      // insertion dans la datamart vision_dm_conso_achats_sva
      //df_conso_teradata_e_recharge_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_conso_e_recharge").save()
      df_conso_teradata_e_recharge_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_conso_e_recharge")
      //df_conso_teradata_e_recharge_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_conso_e_recharge")
    }


    else if (args(1) == "vision_dm_orange_money") {
      // 14 **************************************************** datamart Orange_money **************************************************** OM et rcu et idbase
      // chargement des données dwocit de orange money : msisdn, montant_transaction, chiffre_affaire, VOLUME_TRANSACTIONS, service_de_base, sous_service_de_base, date_transaction,service_a_valeur_ajoute,zone_commerciale,type_reseaux ,canal,date_jour
      val df_dwocit = spark.sql(" SELECT compte_client as msisdn,sum(montant_transaction) as montant_transaction, sum(chiffre_affaire) as chiffre_affaire ,sum(VOLUME_TRANSACTIONS) as VOLUME_TRANSACTIONS,service_de_base ,sous_service_de_base,date_transaction,service_a_valeur_ajoute,zone_commerciale,type_reseaux ,canal,date_jour FROM vision_uc1_datalake.dwocit_om WHERE date_jour = '"+args(0)+"'  group by  compte_client,service_de_base ,sous_service_de_base,date_transaction,service_a_valeur_ajoute,zone_commerciale,type_reseaux ,canal,date_jour ")
      // chargement des données IDBASE
      val df_idbase_om = spark.sql (" SELECT msisdn_idbase, STATU_OM_KYC FROM ( SELECT TRIM(numero) AS  msisdn_idbase   ,TRIM(STATU_OM_KYC) as STATU_OM_KYC,  ROW_NUMBER() OVER (PARTITION BY numero ORDER BY DATE_IDENTIFICATION DESC )  AS row_num FROM    vision_uc1_datalake.idbase where date_jour <= '"+args(0)+"' ) tt where row_num = 1 ")

      //val df_idbase_om = df_idbase.select("msisdn_idbase","STATU_OM_KYC")

      // ajout  masterID
      val df_orange_om=df_dwocit.join(df_rcu,df_dwocit("msisdn") === df_rcu("msisdn"),"left")
        .join(df_idbase_om,df_dwocit("msisdn") === df_idbase_om("msisdn_idbase"),"left")
        .select(df_rcu("master_id"),df_dwocit("msisdn"), df_dwocit("montant_transaction"),df_dwocit("chiffre_affaire"), df_dwocit("VOLUME_TRANSACTIONS"),df_dwocit("service_de_base"),df_dwocit("sous_service_de_base"),df_dwocit("date_transaction"),df_dwocit("service_a_valeur_ajoute"),df_dwocit("zone_commerciale"),df_dwocit("canal"),df_idbase_om("STATU_OM_KYC"),df_dwocit("date_jour"))

      // insertion dans la datamart vision_dm_orange_money
      //df_orange_om.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_orange_money").save()
      df_orange_om.write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_orange_money")
      //df_orange_om.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_orange_money")
    }




    spark.stop()
    spark.close()
  }






}
