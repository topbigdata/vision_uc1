import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, when}

object Vision_uc1_datamarts_part_one_rattrapage {

  def main(args: Array[String]): Unit = {


    // création de spark session
    val spark = SparkSession.builder().appName("Vision_uc1_datamarts_part_one_rattrapage").master("yarn").enableHiveSupport().getOrCreate()
    // création de hive warehouse connection session
    //val hive = com.hortonworks.hwc.HiveWarehouseSession.session(spark).build()

    // chargement des données de datalake RCU dans les deux dataframes df_rcu et df_rcu_socio et persist dans la RAM
    val df_rcu = spark.sql (" SELECT master_id, MSISDN FROM  (    SELECT master_id , MSISDN , ROW_NUMBER() OVER (PARTITION BY MSISDN ORDER BY date_creation_mid DESC) AS row_num  FROM   ( SELECT master_id , CASE WHEN LENGTH(TRIM(phone_num)) = 8 THEN CASE WHEN LIBELLE_TYPESERVICE = 'FIXE' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'WIMAX' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'MOBILE' THEN CASE WHEN SUBSTR(TRIM(phone_num),2,1) IN ('0','1','2','3') THEN CONCAT('01',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('4','5','6') THEN CONCAT('05',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('7','8','9') THEN CONCAT('07',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'AVISO' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'ORANGE MONEY' THEN CASE WHEN SUBSTR(TRIM(phone_num),2,1) IN ('0','1','2','3') THEN CONCAT('01',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('4','5','6') THEN CONCAT('05',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('7','8','9') THEN CONCAT('07',TRIM(phone_num)) ELSE TRIM(phone_num) END ELSE TRIM(phone_num) END ELSE TRIM(phone_num) END AS MSISDN  , date_creation_mid FROM vision_uc1_datalake.rcu where date_jour = (SELECT MAX(date_jour) FROM vision_uc1_datalake.rcu WHERE date_jour <= '"+args(0)+"' )  ) first_req   ) second_req  WHERE row_num = 1 " )
      .persist()



    // 1 **************************************************** datamart rechargement **************************************************** rechargement et rcu
    // chargement des données de datalake rechargement_in // tous les numéros à 13 chiffres
    val df_rechargement = spark.sql ("SELECT SUM (montant) AS MONTANT_TOTAL_RECHARGEMENT, COUNT(*) AS NB_TOTAL_RECHARGEMENT , CANAL AS TYPE_PAYEMENT , SUBSTR(MSISDN,-10) AS MSISDN , DATE_JOUR FROM vision_uc1_datalake.teradata_rechargement_in WHERE date_jour = '"+args(0)+"' GROUP BY CANAL  , SUBSTR(MSISDN,-10) , DATE_JOUR ")
    // ajout des master_id
    val df_rechargement_results = df_rechargement.join(df_rcu, df_rechargement("MSISDN") === df_rcu("msisdn"),"left" )
      .select(df_rcu("master_id"), df_rechargement("MSISDN"), df_rechargement("MONTANT_TOTAL_RECHARGEMENT"), df_rechargement("NB_TOTAL_RECHARGEMENT"), df_rechargement("TYPE_PAYEMENT"), df_rechargement("date_jour"))
    // insertion dans la datamart vision_dm_rechargement
    //df_rechargement_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_rechargement").save()
    df_rechargement_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_rechargement")
    //df_rechargement_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_rechargement")
    //  spark.sql("ANALYZE TABLE vision_uc1_datamart.vision_dm_rechargement COMPUTE STATISTICS")



    // 2 **************************************************** datamart call center **************************************************** call center et rcu
    // chargement des données de datalake genesys1_call_center
    val df_call_center = spark.sql ("SELECT CASE WHEN LENGTH(num_appelant) = 13 AND SUBSTR(num_appelant,1,3) = '225' THEN SUBSTR(num_appelant,-10) WHEN LENGTH(num_appelant) = 14 AND SUBSTR(num_appelant,1,4) = '+225' THEN SUBSTR(num_appelant,-10) ELSE num_appelant END AS MSISDN  , count(case issue_appel when 'Completed' then 1 else null end) AS nb_appels_aboutis , SUM (  UNIX_TIMESTAMP(  TO_DATE (  from_unixtime(unix_timestamp(date_fin_appel , 'dd/MM/yyyy'))  )  || ' ' || heure_fin_appel  )     -     UNIX_TIMESTAMP(  TO_DATE (  from_unixtime(unix_timestamp(date_debut_appel , 'dd/MM/yyyy'))  )  || ' ' || heure_debut_appel   )   )  AS temps_passe_ivr , issue_appel AS status_appel, date_jour FROM vision_uc1_datalake.genesys1_call_center WHERE date_jour= '"+args(0)+"' GROUP BY CASE WHEN LENGTH(num_appelant) = 13 AND SUBSTR(num_appelant,1,3) = '225' THEN SUBSTR(num_appelant,-10) WHEN LENGTH(num_appelant) = 14 AND SUBSTR(num_appelant,1,4) = '+225' THEN SUBSTR(num_appelant,-10) ELSE num_appelant END, issue_appel, date_jour ")

    // chargement des données de datalake genesys1_call_center
    val df_call_center_reiteration = spark.sql ("SELECT CASE WHEN LENGTH(num_appelant) = 13 AND SUBSTR(num_appelant,1,3) = '225' THEN SUBSTR(num_appelant,-10) WHEN LENGTH(num_appelant) = 14 AND SUBSTR(num_appelant,1,4) = '+225' THEN SUBSTR(num_appelant,-10) ELSE num_appelant END AS MSISDN , count(*) AS  nb_reiteration FROM vision_uc1_datalake.genesys1_call_center WHERE date_jour = '"+args(0)+"' OR date_jour = date_sub('"+args(0)+"', 1) OR date_jour = date_sub('"+args(0)+"', 2)  GROUP BY CASE WHEN LENGTH(num_appelant) = 13 AND SUBSTR(num_appelant,1,3) = '225' THEN SUBSTR(num_appelant,-10) WHEN LENGTH(num_appelant) = 14 AND SUBSTR(num_appelant,1,4) = '+225' THEN SUBSTR(num_appelant,-10) ELSE num_appelant END ")

    // ajouter la colonne reiteration à la dataframe genesys1_call_center
    val df_call_center_all = df_call_center.join(df_call_center_reiteration, df_call_center("MSISDN") === df_call_center_reiteration("MSISDN"),"left")
      .select(df_call_center("MSISDN"), df_call_center("nb_appels_aboutis"), df_call_center("temps_passe_ivr"), df_call_center_reiteration("nb_reiteration"), df_call_center("status_appel"), df_call_center("date_jour"))

    // ajout du master_id
    val df_call_center_results = df_call_center_all.join(df_rcu, df_call_center_all("MSISDN") === df_rcu("msisdn"),"left" )
      .select(df_rcu("master_id"), df_call_center_all("MSISDN"), df_call_center_all("nb_appels_aboutis"), df_call_center_all("temps_passe_ivr"), df_call_center_all("nb_reiteration"), df_call_center_all("status_appel") , df_call_center_all("date_jour"))

    // insertion dans la datamart vision_dm_call_center
    //df_call_center_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_exp_client_call_center").save()
    df_call_center_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_exp_client_call_center")
    //df_call_center_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_exp_client_call_center")
    // spark.sql("ANALYZE TABLE vision_uc1_datamart.vision_dm_exp_client_call_center COMPUTE STATISTICS")



    // 3 **************************************************** datamart reclamation jade **************************************************** reclamation et rcu
    // chargement des données de datalake jade_reclamations_fmio
    // à changer pour spark sql
    // val df_reclamation = spark.sql ("SELECT TRANSLATE(ligne_client,' ','') AS MSISDN  , count(ligne_client) AS nb_prise_contact, TRIM(type_interation) as canal , univers, motif , CASE WHEN AVG (  DAY( MODIFIER_LE -       CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END        )*86400  +  hour( MODIFIER_LE -       CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END       )*3600  +  minute( MODIFIER_LE -        CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END       )*60  +  second( MODIFIER_LE -       CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END        )   )  < 0  THEN AVG (  DAY( MODIFIER_LE - CREE_LE )*86400  +  hour( MODIFIER_LE - CREE_LE )*3600  +  minute( MODIFIER_LE - CREE_LE )*60  +  second( MODIFIER_LE - CREE_LE )   )  ELSE  AVG (  DAY( MODIFIER_LE -       CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END        )*86400  +  hour( MODIFIER_LE -       CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END       )*3600  +  minute( MODIFIER_LE -        CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END       )*60  +  second( MODIFIER_LE -       CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END        )   )  END AS DMT  , statut , date_jour FROM vision_uc1_datalake.jade_reclamations_fmio WHERE date_jour = date_sub(current_date,1) GROUP BY TRANSLATE(ligne_client,' ','') , type_interation, univers, motif, statut, date_jour ")
    val df_reclamation = spark.sql ("SELECT TRANSLATE(ligne_client,' ','') AS MSISDN  , count(*) AS nb_prise_contact, count(case when statut in ('Résolu','Actif') then 1 else null end) AS nb_prise_en_charge , TRIM(type_interation) as canal , univers, motif , CASE WHEN AVG ( unix_timestamp(MODIFIER_LE)       -    unix_timestamp(   CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END   )   )  < 0 THEN AVG (  unix_timestamp(MODIFIER_LE)   -   unix_timestamp(CREE_LE)   ) ELSE  AVG ( unix_timestamp(MODIFIER_LE)       -    unix_timestamp(   CASE WHEN DATE_FORMAT(CREE_LE, 'u') IN (6,7) THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') = 5 AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07)  THEN CAST ( next_day(CREE_LE,'MONDAY') || ' 08:00:00' as timestamp ) WHEN DATE_FORMAT(CREE_LE, 'u') IN (1,2,3,4) AND HOUR(CREE_LE) IN (17,18,19,20,21,22,23,00,01,02,03,04,05,06,07) THEN CAST ( DATE_ADD(CREE_LE,1) || ' 08:00:00' as timestamp ) ELSE CREE_LE END  )   )  END AS DMT  , statut , CASE WHEN SUBSTR(categorie,1,2) ='IN' THEN 'demandeInfo' WHEN SUBSTR(categorie,1,2) ='OP' THEN 'operation' WHEN SUBSTR(categorie,1,2) ='RE' THEN 'reclamation' ELSE 'autre' END AS categorie , date_jour FROM vision_uc1_datalake.jade_reclamations_fmio WHERE date_jour = '"+args(0)+"' GROUP BY TRANSLATE(ligne_client,' ','') , type_interation, univers, motif, statut,  CASE WHEN SUBSTR(categorie,1,2) ='IN' THEN 'demandeInfo' WHEN SUBSTR(categorie,1,2) ='OP' THEN 'operation' WHEN SUBSTR(categorie,1,2) ='RE' THEN 'reclamation' ELSE 'autre' END , date_jour ")
    // ajout du master_id
    val df_reclamation_results = df_reclamation.join(df_rcu, df_reclamation("MSISDN") === df_rcu("msisdn"),"left" )
      .select(df_rcu("master_id"), df_reclamation("MSISDN"), df_reclamation("nb_prise_contact"), df_reclamation("nb_prise_en_charge"), df_reclamation("canal"), df_reclamation("univers") , df_reclamation("motif") , df_reclamation("dmt") , df_reclamation("statut") , df_reclamation("categorie")  , df_reclamation("date_jour"))

    // insertion dans la datamart vision_dm_reclamation
    //df_reclamation_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_exp_client_reclamation").save()
    df_reclamation_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_exp_client_reclamation")
    //df_reclamation_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_exp_client_reclamation")



    // 4 **************************************************** datamart equipements **************************************************** equipement et rcu

    // chargement des données des datalakes teradata_equipement_1 et teradata_equipement_2
    // à changer pour spark sql
    val df_equipements = spark.sql ("SELECT  SUBSTR(E2.MSISDN,-10)   AS msisdn, E1.modele AS modele, E1.fabriquant AS fabriquant, E1.type_technologie AS type_technologie, E1.os AS os, E1.version_os AS version_os, E1.type_device AS type_device, E1.date_jour AS date_jour FROM vision_uc1_datalake.teradata_equipement_1 E1  LEFT JOIN vision_uc1_datalake.teradata_equipement_2 E2 ON E1.TAC=E2.TAC_ID WHERE E1.date_jour = '"+args(0)+"' AND E2.date_jour = '"+args(0)+"' ")
      .distinct()

    val df_usim_detection = spark.sql("SELECT SUBSTR(msisdn,-10)  AS msisdn_usim FROM vision_uc1_datalake.dwocit_usim_detection WHERE date_jour =  '"+args(0)+"'  ")
      .distinct()

    val df_equipements_usim = df_equipements
      .join(df_usim_detection,  df_equipements("msisdn") === df_usim_detection("msisdn_usim"),"LEFT")
      .withColumn("statut_usim", when(df_equipements("msisdn") === df_usim_detection("msisdn_usim"),"Y").otherwise("N"))
      .select(df_equipements("msisdn"), df_equipements("modele"), df_equipements("fabriquant"), df_equipements("type_technologie"), df_equipements("os"), df_equipements("version_os") , df_equipements("type_device") , col("statut_usim") , df_equipements("date_jour"))

    val df_equipement_results=df_equipements_usim
      .join(df_rcu, df_equipements_usim("msisdn")===df_rcu("msisdn"), "LEFT")
      .select(df_rcu("master_id"),df_equipements_usim("msisdn"),df_equipements_usim("modele"),df_equipements_usim("fabriquant"),df_equipements_usim("type_technologie"),df_equipements_usim("os"),df_equipements_usim("version_os"),df_equipements_usim("type_device"),df_equipements_usim("statut_usim"),df_equipements_usim("date_jour"))
    // insertion dans la datamart vision_datamart_equipements
    //df_equipements_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_equipements").save()
    df_equipement_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_equipements")
    //df_equipements_usim.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_equipements")



    // 5 **************************************************** datamart conso_achats_bundles **************************************************** bundles et rcu
    // chargement des données de datalake TERADATA_ACHAT_PASS & file_liste_channel_id
    val df_conso_achats_bundles = spark.sql ("SELECT SUBSTR(ACC_NBR,-10) AS MSISDN , SUM(CA_ACHAT_PASS) AS montant_achat_pass , type_de_pass , canal, date_jour as date_jour FROM vision_uc1_datalake.TERADATA_ACHAT_PASS WHERE date_jour = '"+args(0)+"' GROUP BY SUBSTR(ACC_NBR,-10), type_de_pass , canal, date_jour ")
    val df_file_categorisation_bundles = spark.sql("SELECT type_pass, service_id FROM vision_uc1_datalake.file_categorisation_bundles WHERE date_jour = (SELECT MAX(date_jour) FROM vision_uc1_datalake.file_categorisation_bundles WHERE date_jour <= '"+args(0)+"' ) ")
    val df_file_liste_channel_id = spark.sql("SELECT contact_channel_id, contact_channel_name, libelle_commercial FROM vision_uc1_datalake.file_liste_channel_id WHERE date_jour = (SELECT MAX(date_jour) FROM vision_uc1_datalake.file_liste_channel_id WHERE date_jour <= '"+args(0)+"' ) ")

    val df_conso_achats_bundles_type=df_conso_achats_bundles
      .join(df_file_categorisation_bundles,  df_conso_achats_bundles("type_de_pass") === df_file_categorisation_bundles("type_pass"),"LEFT" )
      .select(df_conso_achats_bundles("MSISDN"),df_conso_achats_bundles("montant_achat_pass"),df_file_categorisation_bundles("type_pass"),df_conso_achats_bundles("canal"),df_conso_achats_bundles("date_jour"))

    val df_conso_achats_bundles_type_canal=df_conso_achats_bundles_type
      .join(df_file_liste_channel_id, df_conso_achats_bundles_type("canal") === df_file_liste_channel_id("contact_channel_id"),"LEFT" )
      .select(df_conso_achats_bundles_type("MSISDN"),df_conso_achats_bundles_type("montant_achat_pass"),df_conso_achats_bundles_type("type_pass"),df_file_liste_channel_id("contact_channel_name"),df_file_liste_channel_id("libelle_commercial"),df_conso_achats_bundles_type("date_jour"))
      .withColumnRenamed("type_pass","type_de_pass")
      .withColumnRenamed("contact_channel_name","canal")
      .withColumnRenamed("libelle_commercial","moyen_de_payement")

    // ajout du master_id
    val df_conso_achats_bundles_results = df_conso_achats_bundles_type_canal.join(df_rcu, df_conso_achats_bundles_type_canal("MSISDN") === df_rcu("msisdn"),"left" )
      .select(df_rcu("master_id"), df_conso_achats_bundles_type_canal("MSISDN"), df_conso_achats_bundles_type_canal("montant_achat_pass"), df_conso_achats_bundles_type_canal("type_de_pass"), df_conso_achats_bundles_type_canal("canal"), df_conso_achats_bundles_type_canal("moyen_de_payement")  , df_conso_achats_bundles_type_canal("date_jour") )

    // insertion dans la datamart vision_dm_conso_achats_bundles
    //df_conso_achats_bundles_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_conso_achats_bundles").save()
    df_conso_achats_bundles_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_conso_achats_bundles")
    //df_conso_achats_bundles_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_conso_achats_bundles")



    // 6 **************************************************** datamart conso_transactions_data **************************************************** conso network data et rcu

    // chargement des données de datalake teradata_social_network_data
    val df_conso_transactions_data = spark.sql ("SELECT SUBSTR(MSISDN,-10) AS MSISDN , SUM(data_MB) AS conso_transactions_data , TYPE_TRANSACTION , TYPE_ACTION, date_jour FROM vision_uc1_datalake.teradata_social_network_data WHERE date_jour = '"+args(0)+"' AND TYPE_DONNEES like '%VOLUME%' GROUP BY SUBSTR(MSISDN,-10), TYPE_TRANSACTION , TYPE_ACTION, date_jour ")

    // ajout du master_id
    val df_conso_transactions_data_results = df_conso_transactions_data.join(df_rcu, df_conso_transactions_data("MSISDN") === df_rcu("msisdn"),"left" )
      .select(df_rcu("master_id"), df_conso_transactions_data("MSISDN"), df_conso_transactions_data("conso_transactions_data"), df_conso_transactions_data("type_transaction"), df_conso_transactions_data("type_action"), df_conso_transactions_data("date_jour") )

    // insertion dans la datamart vision_dm_conso_transactions_data
    //df_conso_transactions_data_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_conso_transactions_data").save()
    df_conso_transactions_data_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_conso_transactions_data")
    //df_conso_transactions_data_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_conso_transactions_data")



    // 7 **************************************************** datamart conso_credit_sos **************************************************** credit sos et rcu

    // chargement des données de datalake teradata_sos_credit_data
    val df_conso_credit_sos = spark.sql (" SELECT SUBSTR(MSISDN,-10) AS MSISDN , SUM(MONTANT_CREDIT_INITIAL) AS MONTANT_CREDIT_INITIAL , SUM(MONTANT_REMBOURSE) AS MONTANT_REMBOURSE , SERVICE , CANAL, date_jour FROM vision_uc1_datalake.teradata_sos_credit_data WHERE date_jour = '"+args(0)+"' GROUP BY SUBSTR(MSISDN,-10), SERVICE , CANAL, date_jour ")

    // ajout du master_id
    val df_conso_credit_sos_results = df_conso_credit_sos.join(df_rcu, df_conso_credit_sos("MSISDN") === df_rcu("msisdn"),"left" )
      .select(df_rcu("master_id"), df_conso_credit_sos("MSISDN"), df_conso_credit_sos("montant_credit_initial"), df_conso_credit_sos("montant_rembourse"), df_conso_credit_sos("service"), df_conso_credit_sos("canal") , df_conso_credit_sos("date_jour") )

    // insertion dans la datamart vision_dm_conso_credit_sos
    //df_conso_credit_sos_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_conso_credit_sos").save()
    df_conso_credit_sos_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_conso_credit_sos")
    //df_conso_credit_sos_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_conso_credit_sos")



    // 8 **************************************************** datamart conso_achats_sva **************************************************** sva et rcu
    // chargement des données de datalake teradata_sva & dwocit_dim_premices_sva
    val df_conso_achats_sva = spark.sql ("SELECT CASE WHEN LENGTH(TRIM(SVA.compte_client)) = 13 AND SUBSTR(TRIM(SVA.compte_client),1,3) = '225' THEN SUBSTR(TRIM(SVA.compte_client),-10) WHEN LENGTH(TRIM(SVA.compte_client)) = 14 AND SUBSTR(TRIM(SVA.compte_client),1,3) = '225' AND SUBSTR(TRIM(SVA.compte_client),-1) = '.' THEN SUBSTR(TRIM(SVA.compte_client),4,10) ELSE TRIM(SVA.compte_client) END AS MSISDN  , SUM(SVA.MONTANT_SVA) AS MONTANT_SVA , TRIM(SVA.canal) as moyen_de_payement, TRIM(DPS.nom_sva) as nom_service, SVA.date_jour as date_jour FROM vision_uc1_datalake.teradata_sva SVA LEFT JOIN vision_uc1_datalake.dwocit_dim_premices_sva DPS ON TRIM(SVA.id_vas) = TRIM(DPS.vas_id) WHERE SVA.date_jour = '"+args(0)+"' AND DPS.date_jour = (SELECT MAX(date_jour) FROM vision_uc1_datalake.dwocit_dim_premices_sva WHERE date_jour <= '"+args(0)+"')  GROUP BY CASE WHEN LENGTH(TRIM(SVA.compte_client)) = 13 AND SUBSTR(TRIM(SVA.compte_client),1,3) = '225' THEN SUBSTR(TRIM(SVA.compte_client),-10) WHEN LENGTH(TRIM(SVA.compte_client)) = 14 AND SUBSTR(TRIM(SVA.compte_client),1,3) = '225' AND SUBSTR(TRIM(SVA.compte_client),-1) = '.' THEN SUBSTR(TRIM(SVA.compte_client),4,10) ELSE TRIM(SVA.compte_client) END, TRIM(SVA.canal) , TRIM(DPS.nom_sva), SVA.date_jour ")

    // ajout du master_id
    val df_conso_achats_sva_results = df_conso_achats_sva.join(df_rcu, df_conso_achats_sva("MSISDN") === df_rcu("msisdn"),"left" )
      .select(df_rcu("master_id"), df_conso_achats_sva("MSISDN"), df_conso_achats_sva("montant_sva"), df_conso_achats_sva("moyen_de_payement"), df_conso_achats_sva("nom_service"), df_conso_achats_sva("date_jour") )

    // insertion dans la datamart vision_dm_conso_achats_sva
    //df_conso_achats_sva_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_conso_achats_sva").save()
    df_conso_achats_sva_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_conso_achats_sva")
    //df_conso_achats_sva_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_conso_achats_sva")



    // 9 **************************************************** datamart conso_erecharge **************************************************** e_recharge et rcu
    // chargement des données de datalake teradata_e_recharge
    val df_conso_teradata_e_recharge = spark.sql ("select SUBSTR(client_final,-10) as MSISDN , TYPE_TRANSACTION , SUM (TRANSFER_AMOUNT) AS ca_e_recharge , date_jour FROM vision_uc1_datalake.teradata_e_recharge WHERE date_jour= '"+args(0)+"' GROUP BY  SUBSTR(client_final,-10) , TYPE_TRANSACTION , date_jour  ")

    // ajout du master_id
    val df_conso_teradata_e_recharge_results = df_conso_teradata_e_recharge.join(df_rcu, df_conso_teradata_e_recharge("MSISDN") === df_rcu("msisdn"),"left" )
      .select(df_rcu("master_id"), df_conso_teradata_e_recharge("MSISDN"), df_conso_teradata_e_recharge("TYPE_TRANSACTION"), df_conso_teradata_e_recharge("ca_e_recharge"), df_conso_teradata_e_recharge("date_jour"))

    // insertion dans la datamart vision_dm_conso_achats_sva
    //df_conso_teradata_e_recharge_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_conso_e_recharge").save()
    df_conso_teradata_e_recharge_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_conso_e_recharge")
    //df_conso_teradata_e_recharge_results.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_conso_e_recharge")


    // 14 **************************************************** datamart Orange_money **************************************************** OM et rcu et idbase
    // chargement des données dwocit de orange money : msisdn, montant_transaction, chiffre_affaire, VOLUME_TRANSACTIONS, service_de_base, sous_service_de_base, date_transaction,service_a_valeur_ajoute,zone_commerciale,type_reseaux ,canal,date_jour
    val df_dwocit = spark.sql(" SELECT compte_client as msisdn,sum(montant_transaction) as montant_transaction, sum(chiffre_affaire) as chiffre_affaire ,sum(VOLUME_TRANSACTIONS) as VOLUME_TRANSACTIONS,service_de_base ,sous_service_de_base,date_transaction,service_a_valeur_ajoute,zone_commerciale,type_reseaux ,canal,date_jour FROM vision_uc1_datalake.dwocit_om WHERE date_jour = '"+args(0)+"'  group by  compte_client,service_de_base ,sous_service_de_base,date_transaction,service_a_valeur_ajoute,zone_commerciale,type_reseaux ,canal,date_jour ")
    // chargement des données IDBASE
    val df_idbase_om = spark.sql (" SELECT msisdn_idbase, STATU_OM_KYC FROM ( SELECT TRIM(numero) AS  msisdn_idbase   ,TRIM(STATU_OM_KYC) as STATU_OM_KYC,  ROW_NUMBER() OVER (PARTITION BY numero ORDER BY DATE_IDENTIFICATION DESC )  AS row_num FROM    vision_uc1_datalake.idbase where date_jour <= '"+args(0)+"' ) tt where row_num = 1 ")

    //val df_idbase_om = df_idbase.select("msisdn_idbase","STATU_OM_KYC")

    // ajout  masterID
    val df_orange_om=df_dwocit.join(df_rcu,df_dwocit("msisdn") === df_rcu("msisdn"),"left")
      .join(df_idbase_om,df_dwocit("msisdn") === df_idbase_om("msisdn_idbase"),"left")
      .select(df_rcu("master_id"),df_dwocit("msisdn"), df_dwocit("montant_transaction"),df_dwocit("chiffre_affaire"), df_dwocit("VOLUME_TRANSACTIONS"),df_dwocit("service_de_base"),df_dwocit("sous_service_de_base"),df_dwocit("date_transaction"),df_dwocit("service_a_valeur_ajoute"),df_dwocit("zone_commerciale"),df_dwocit("canal"),df_idbase_om("STATU_OM_KYC"),df_dwocit("date_jour"))

    // insertion dans la datamart vision_dm_orange_money
    //df_orange_om.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_orange_money").save()
    df_orange_om.write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_orange_money")
    //df_orange_om.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_orange_money")






    spark.stop()
    spark.close()
  }

}





