import org.apache.spark.sql.functions._
import org.apache.spark.sql._
import org.apache.spark.ml.feature.StringIndexer
import org.apache.spark.graphx.{Edge, Graph, PartitionStrategy, VertexId, VertexRDD}
import org.apache.spark.sql.catalyst.expressions.aggregate.ApproximatePercentile
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{LongType, StringType, StructType}

object Vision_uc1_datascience_sna {

  def main(args: Array[String]): Unit = {

    // création de spark session
    val spark = SparkSession.builder().appName("Vision_uc1_datascience_sna").master("yarn").enableHiveSupport().getOrCreate()

    // données d'usage teradata simple
    val df1 = spark.sql("SELECT CAST (src as Long) as src, CAST (dst as Long) as dst , relationship FROM ( SELECT CASE WHEN LENGTH(CALLING_NBR) = 13 AND SUBSTR(CALLING_NBR,1,3) = '225' THEN SUBSTR(CALLING_NBR,-10) WHEN LENGTH(CALLING_NBR) = 18 AND SUBSTR(CALLING_NBR,1,8) = 'tel:+225' THEN SUBSTR(CALLING_NBR,-10) ELSE CALLING_NBR END AS src, CASE WHEN LENGTH(CALLED_NBR) = 13 AND SUBSTR(CALLED_NBR,1,3) = '225' THEN SUBSTR(CALLED_NBR,-10) WHEN LENGTH(CALLED_NBR) = 18 AND SUBSTR(CALLED_NBR,1,8) = 'tel:+225' THEN SUBSTR(CALLED_NBR,-10) WHEN LENGTH(CALLED_NBR) = 12 THEN SUBSTR(CALLED_NBR,-10) ELSE CALLED_NBR END AS dst, CASE WHEN UPPER(category_evenement) like '%SMS%' THEN 'SMS' WHEN UPPER(category_evenement) like '%VOICE%' THEN 'VOIX' END AS relationship FROM vision_uc1_datalake.TERADATA_USAGE_SIMPLE WHERE (category_evenement like '%SMS%' OR category_evenement like '%VOICE%') AND date_jour          BETWEEN date_format ( date_sub(current_date,20),'yyyy-MM-01' ) AND last_day ( date_sub(current_date,20) )                 ) first_req WHERE LENGTH(src) = 10 AND LENGTH(dst) = 10")
    // données d'usage OM
    val df2 = spark.sql("SELECT CAST(compte_client as Long) AS src, CAST (compte_distributeur as Long) AS dst , 'OM' as relationship FROM vision_uc1_datalake.dwocit_om WHERE LENGTH(compte_client) = 10 AND LENGTH(compte_distributeur) = 10 AND date_jour      BETWEEN date_format ( date_sub(current_date,20),'yyyy-MM-01' ) AND last_day ( date_sub(current_date,20) )      AND service_de_base = 'P2P' ")

    // liste des numéros à exclure des numéros teradata usage simple
    val telco_gtnes_df = spark.sql("SELECT DISTINCT CAST( SUBSTR(gt,-10) as Long) as msisdn FROM vision_uc1_datalake.file_liste_gt")

    // liste des plages des numéros a exclure des numéros teradata usage simple
    val telco_msrn = spark.sql("SELECT DISTINCT type_msrn,start_msrn,end_msrn FROM vision_uc1_datalake.file_liste_msrn")
    // traitement des plages msrn // changement de start_msrn 0748350XXX ==> 0748350000 et end_msrn 0748353XXX ==> 0748353999
    val telco_msrn_range = telco_msrn.withColumn("start_msrn", regexp_replace(col("start_msrn"),"X","0")).withColumn("end_msrn", regexp_replace(col("end_msrn"),"X","9")).withColumn("start_msrn",col("start_msrn").cast("Long")).withColumn("end_msrn",col("end_msrn").cast("Long"))
    // fonction d'iteration de start_msrn ==> end_msrn
    val rangeSequence = udf{ (lower: Int, upper: Int) => Seq.iterate(lower, upper - lower + 1)(_ + 1) }
    // explode de dataframe des numéros msrn
    val telco_msrn_df = telco_msrn_range.withColumn("msrn", explode(rangeSequence(col("start_msrn"), col("end_msrn")))).select("msrn")

    // union et distinct des numéros à exclure des numéros teradata usage simple
    val telco_sys_df = telco_gtnes_df.union(telco_msrn_df).dropDuplicates()

    // liste des numéros a exclure des numéros OM
    val om_channel_df = spark.sql("SELECT DISTINCT CAST (msisdn as Long) as msisdn FROM vision_uc1_datalake.dwocit_om_compte_channel")

    // exclure les numéros sustème de la dataframe des usages simple
    val x1 = df1.join(telco_sys_df, df1("src") === telco_sys_df("msisdn") , "left_anti").join(telco_sys_df, df1("dst") === telco_sys_df("msisdn") , "left_anti")

    // exclure les numéros sustème channel de la dataframe OM
    val x2 = df2.join(om_channel_df, df2("src") === om_channel_df("msisdn"), "left_anti").join(om_channel_df, df2("dst") === om_channel_df("msisdn"), "left_anti")

    // union des usage simple et OM totale  : src ==> dst + relationship // suppression de tous les null
    val x3 = x1.union(x2).na.drop()

    // avoir la distribution des couple distinct src/dst qui ont fait moins de 3 operations
    val couple_non_actif=x3.groupBy(col("src").as("src2"),col("dst").as("dst2")).agg(count("*").alias("cnt")).filter(col("cnt")  < 4)

    // filtrer seulement qui ont fait plus de trois interaction durant un mois
    val x4=x3.join(couple_non_actif, x3("src")===couple_non_actif("src2") &&  x3("dst")===couple_non_actif("dst2"),"left_anti").select(x3("src"), x3("dst"),x3("relationship"))

    // avoir les MSISDN qui ont fait plus de 3000 opérations par mois
    val msisdn_actif = x4.groupBy(col("src").as("msisdn_actif")).agg(count("*").alias("cnt")).filter(col("cnt")  > 2000).union(x4.groupBy(col("dst").as("msisdn_actif")).agg(count("*").alias("cnt")).filter(col("cnt")  > 2000)).select("msisdn_actif").distinct()
    //val schema = new StructType().add("src",LongType,true).add("dst",LongType,true).add("relationship",StringType,true)
    //val df = spark.read.format("csv").option("header", "true").schema(schema).load("/user/ahmed.mnif/test_graphe_sna.csv")

    // filtrer seulement les MSISDN qui ont fait moins de 3000 opérations par mois
    val clean_df = x4.join(msisdn_actif, x4("src") === msisdn_actif("msisdn_actif"), "left_anti").join(msisdn_actif, x4("dst") === msisdn_actif("msisdn_actif"), "left_anti")

    // construction de la dataframe des nodes ==> les nodes sont tous les numéros possibles distinct dans les deux colonnes src et dst
    val df0 = clean_df.select("src").union(clean_df.select("dst")).withColumnRenamed("src","nodes").dropDuplicates()

    // transformer la dataframe des noeuds en rdd
    val vertices : RDD[(VertexId, Long)] = df0.rdd.map(row => (row.getAs[Long]("nodes") -> row.getAs[Long]("nodes")))
    // pour vérifier ==> vertices.collect().foreach(println)
    // transformer la dataframe des relations en rdd
    val edges : RDD[Edge[String]] = clean_df.rdd.map(row => Edge(row.getAs[Long]("src"), row.getAs[Long]("dst"), row.getAs[String]("relationship")))
    // pour vérifier ==> edges.collect().foreach(println)

    //creation du graph
    val graph = Graph(vertices, edges).cache()
    //println("[GRAPH CREATED]       ==>     " + graph.numVertices + " nodes  |  " + graph.numEdges + " edges")

    //identifier le nbr d operations totales pour chaque msisdn
    val degrees: VertexRDD[Int] = graph.degrees
    // pour vérifier ==> degrees.collect().foreach(println)
    val deg_cols = Seq("MSISDN1","Nb_total_operations")
    val degs_df = spark.createDataFrame(degrees).toDF(deg_cols:_*)
    // pour vérifier ==> degs_df.show()

    //identifier le nbr d operations entrantes pour chaque msisdn
    val inDegrees: VertexRDD[Int] = graph.inDegrees
    // pour vérifier ==> inDegrees.collect().foreach(println)
    val in_cols = Seq("MSISDN2","Operations_recues")
    val indegs_df = spark.createDataFrame(inDegrees).toDF(in_cols:_*)

    //identifier le nbr d operations sortantes pour chaque msisdn
    val outDegrees: VertexRDD[Int] = graph.outDegrees
    // pour vérifier ==> outDegrees.collect().foreach(println)
    val out_cols = Seq("MSISDN3","Operations_emises")
    val outdegs_df = spark.createDataFrame(outDegrees).toDF(out_cols:_*)

    //identifier les communautes en utilisant la fonction connected components
    val cc = graph.connectedComponents(3).vertices
    //val cc = graph.stronglyConnectedComponents(3).vertices
    //println("[COMMUNITIES CREATED] ==> " + cc.values.distinct().count() + " communities found")

    //creation du dataframe a partir du resultat obtenu
    val columns = Seq("MSISDN","name")
    val comm_df = spark.createDataFrame(cc).toDF(columns:_*)
    //attribuer un nom de communaute en se basant sur les index obtenus
    val indexer = new StringIndexer().setInputCol("name").setOutputCol("Community")
    val comms_df2 = indexer.fit(comm_df).transform(comm_df).withColumn("Community",  col("Community").cast("Int")).withColumn("Community", functions.concat(lit("comm"),col("Community"))).drop("name")

    //ajouter l information des communte a notre dataframe contenant les details
    val joined_df = comms_df2.join(degs_df, comms_df2("MSISDN") === degs_df("MSISDN1"), "left").join(indegs_df, comms_df2("MSISDN") === indegs_df("MSISDN2"), "left").join(outdegs_df, comms_df2("MSISDN") === outdegs_df("MSISDN3"), "left").select(comms_df2("MSISDN"),degs_df("nb_total_operations"),indegs_df("operations_recues"),outdegs_df("operations_emises"),comms_df2("Community")).na.fill(0)

    //classification des msisdn en se basant sur les deciles
    def percentile_approxx(col: Column, percentage: Column, accuracy: Column): Column = {
      val expr = new ApproximatePercentile(col.expr,  percentage.expr, accuracy.expr).toAggregateExpression
      new Column(expr)
    }

    val perc_df = joined_df.groupBy(col("Community").as("Community2")).agg(percentile_approxx(col("Nb_total_operations"), lit(0.8), lit(100)).as("QT"))
    //val sna_df_final = joined_df.join(perc_df, joined_df("Community") === perc_df("Community"), "left").withColumnRenamed("percentile_approx(Nb_total_operations, 0.8, 100)","QT").withColumn("Role", when(col("Nb_total_operations") > col("QT"), lit("Leader")).otherwise(lit("Follower"))).withColumn("Date_jour", lit(datee)).drop(col("QT")).drop(perc_df("Community"))

    val final_df = joined_df
      .join(perc_df, joined_df("Community") === perc_df("Community2"), "left")
      .withColumn("Role", when(col("Nb_total_operations") >= col("QT"), lit("Leader")).otherwise(lit("Follower")))
      .withColumn("Date_jour", lit( date_format(date_sub(current_date(),20),"yyyy-MM-01") )  )
      .withColumn("MSISDN",col("MSISDN").cast(StringType))
      .withColumn("MSISDN", when(length(col("MSISDN")) === 9 , concat(lit("0"), col("MSISDN"))).otherwise(col("MSISDN") ) )
      .select("MSISDN","nb_total_operations","operations_recues","operations_emises","Community",  "Role", "Date_jour")

    final_df.write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_sna")

    }

}
