import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, when}

object Vision_uc1_datamart_equipements {


  def main(args: Array[String]): Unit = {

    // création de spark session
    val spark = SparkSession.builder().appName("Vision_uc1_datamart_equipements").master("yarn").enableHiveSupport().getOrCreate()
    // création de hive warehouse connection session
    //val hive = com.hortonworks.hwc.HiveWarehouseSession.session(spark).build()

    // chargement des données de datalake RCU dans les deux dataframes df_rcu et df_rcu_socio et persist dans la RAM
    val df_rcu = spark.sql(" SELECT master_id, MSISDN FROM  (    SELECT master_id , MSISDN , ROW_NUMBER() OVER (PARTITION BY MSISDN ORDER BY date_creation_mid DESC) AS row_num  FROM   ( SELECT master_id , CASE WHEN LENGTH(TRIM(phone_num)) = 8 THEN CASE WHEN LIBELLE_TYPESERVICE = 'FIXE' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'WIMAX' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'MOBILE' THEN CASE WHEN SUBSTR(TRIM(phone_num),2,1) IN ('0','1','2','3') THEN CONCAT('01',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('4','5','6') THEN CONCAT('05',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('7','8','9') THEN CONCAT('07',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'AVISO' THEN CASE WHEN SUBSTR(TRIM(phone_num),1,2) IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39') THEN CONCAT('27',TRIM(phone_num)) ELSE TRIM(phone_num) END WHEN LIBELLE_TYPESERVICE = 'ORANGE MONEY' THEN CASE WHEN SUBSTR(TRIM(phone_num),2,1) IN ('0','1','2','3') THEN CONCAT('01',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('4','5','6') THEN CONCAT('05',TRIM(phone_num)) WHEN SUBSTR(TRIM(phone_num),2,1) IN ('7','8','9') THEN CONCAT('07',TRIM(phone_num)) ELSE TRIM(phone_num) END ELSE TRIM(phone_num) END ELSE TRIM(phone_num) END AS MSISDN  , date_creation_mid FROM vision_uc1_datalake.rcu where date_jour = date_sub(current_date,1)  ) first_req   ) second_req  WHERE row_num = 1 ")


    // 4 **************************************************** datamart equipements **************************************************** equipement et rcu

    // chargement des données des datalakes teradata_equipement_1 et teradata_equipement_2
    // à changer pour spark sql
    val df_equipements = spark.sql ("SELECT  SUBSTR(E2.MSISDN,-10)   AS msisdn, E1.modele AS modele, E1.fabriquant AS fabriquant, E1.type_technologie AS type_technologie, E1.os AS os, E1.version_os AS version_os, E1.type_device AS type_device, E1.date_jour AS date_jour FROM vision_uc1_datalake.teradata_equipement_1 E1  LEFT JOIN vision_uc1_datalake.teradata_equipement_2 E2 ON E1.TAC=E2.TAC_ID WHERE E1.date_jour = date_sub(current_date,1) AND E2.date_jour = date_sub(current_date,1) ")
      .distinct()

    val df_usim_detection = spark.sql("SELECT SUBSTR(msisdn,-10)  AS msisdn_usim FROM vision_uc1_datalake.dwocit_usim_detection WHERE date_jour = date_sub(current_date,1)")
      .distinct()

    val df_equipements_usim = df_equipements
      .join(df_usim_detection,  df_equipements("msisdn") === df_usim_detection("msisdn_usim"),"LEFT")
      .withColumn("statut_usim", when(df_equipements("msisdn") === df_usim_detection("msisdn_usim"),"Y").otherwise("N"))
      .select(df_equipements("msisdn"), df_equipements("modele"), df_equipements("fabriquant"), df_equipements("type_technologie"), df_equipements("os"), df_equipements("version_os") , df_equipements("type_device") , col("statut_usim") , df_equipements("date_jour"))

    val df_equipement_results=df_equipements_usim
      .join(df_rcu, df_equipements_usim("msisdn")===df_rcu("msisdn"), "LEFT")
      .select(df_rcu("master_id"),df_equipements_usim("msisdn"),df_equipements_usim("modele"),df_equipements_usim("fabriquant"),df_equipements_usim("type_technologie"),df_equipements_usim("os"),df_equipements_usim("version_os"),df_equipements_usim("type_device"),df_equipements_usim("statut_usim"),df_equipements_usim("date_jour"))
    // insertion dans la datamart vision_datamart_equipements
    //df_equipements_results.write.format("com.hortonworks.spark.sql.hive.llap.HiveWarehouseConnector").mode("overwrite").option("table", "vision_uc1_datamart.vision_dm_equipements").save()
    df_equipement_results.coalesce(10).write.format("orc").mode("overwrite").insertInto("vision_uc1_datamart.vision_dm_equipements")
    //df_equipements_usim.coalesce(1).write.format("orc").mode("Append").partitionBy("date_jour").saveAsTable("vision_uc1_datamart.vision_dm_equipements")


    spark.stop()
    spark.close()



  }

  }
