name := "vision_uc1"

version := "0.1"

scalaVersion := "2.11.12"

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.3.2"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.3.2"
libraryDependencies += "org.apache.spark" %% "spark-hive" % "2.3.2"
libraryDependencies += "org.mongodb.spark" %% "mongo-spark-connector" % "2.3.2"
libraryDependencies += "org.mongodb" % "bson" % "4.4.0"
libraryDependencies += "org.mongodb" % "mongo-java-driver" % "3.12.10"
libraryDependencies += "org.apache.spark" %% "spark-graphx" % "2.3.2"
libraryDependencies += "org.apache.spark" %% "spark-mllib" % "2.3.2"



mappings in (Compile,packageSrc) := (managedSources in Compile).value map (s => (s,s.getName))
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
